var databaseRef = firebase.database().ref();
// var DispatchRef = databaseRef.child("Dispatch Slip")
var SIRef = databaseRef.child("Sales Invoice");
var SmallLedgerRef = databaseRef.child("Small Ledger");
var BigLedgerRef = databaseRef.child("Big Ledger");
var CustCylindersRef = databaseRef.child("Customer Cylinder");
// var productsRef = databaseRef.child("Inventory");
// var newInventory = databaseRef.child("Cylinders");
// var icrRef = databaseRef.child("Incoming Cylinder Report");

// //display loading screen
document.querySelector(".loading").style.display = "block";

var products = [];
var productrecordsid = [];
// var DispatchList = [];
var SmallLedgerList = [];
// var BigLedgerList = [];
// var ICRData = [];
// var ICRList = [];

// var cylarrtransition = [];
// var cyltypes = [];
var specsicylarr = [];

// const getProducts = new Promise((resolve, reject) => {
//   productsRef.once("value", (data) => {
//     data.forEach((data2) => {
//       products.push(data2.key + "---" + data2.val().cylinderstatus);
//       productrecordsid.push(data2.key);
//       resolve(products);
//     });
//   })
// });

const getSpecificSIItems = new Promise((resolve, reject) => {
  SIRef.child('JRRMMC-2').child('items').once("value", (data) => {
    data.forEach((data2) => {
      specsicylarr.push(data2.key);
      resolve(specsicylarr);
    })
  })
})

// const getCustomerCyls = new Promise((resolve, reject) => {
//   CustCylindersRef.child('-M9FjzsaMZZsnAKzJ-FM').once("value", (data) => {
//     data.forEach((data2) => {
//       specsicylarr.push(data2.key);
//       resolve(specsicylarr);
//     })
//   })
// })

// const getInventoryData = new Promise((resolve, reject) => {
//   productsRef.once("value", (data) => {
//     data.forEach((data2) => {
//       cylarrtransition.push(data2.key + "---" + data2.val().cylindertype);
//       if (!cyltypes.includes(data2.val().cylindertype)) {
//         cyltypes.push(data2.val().cylindertype);
//       }
//       resolve(cylarrtransition);
//     });
//   })
// });

// const getICRData = new Promise((resolve, reject) => {
//   icrRef.once("value", (data) => {
//     data.forEach((data2) => {
//       ICRData.push(data2.val());
//     });
//     resolve(ICRData);
//   });

// });

// // const getDispatchItems = new Promise((resolve, reject) => {
// //   var ctr = 0;
// //   var code = '';
// //   DispatchRef.child("DISP-000039").child("Cylinders").orderByChild("sorterkey").once("value", (data) => {
// //     data.forEach((data2) => {
// //       DispatchList.push(data2.key);
// //     });
// //     ctr++;
// //     resolve(DispatchList);
// //   });
// // });


// const getSmallLedger = new Promise((resolve, reject) => {
//   SmallLedgerRef.orderByChild("customer").equalTo("PNP GENERAL HOSPITAL").once("value", (data) => {
//     data.forEach((data2) => {
//       console.log(data2.key, data2.val().lastOut);
//       products.push([data2.key, data.val().lastOut]);
//     });
//     resolve(SmallLedgerList);
//   });
// });

// // const getBigLedger = new Promise((resolve, reject) => {
// //   BigLedgerRef.child("-M9G95BxkhvjBLbSBppZ").once("value", (data) => {
// //     data.forEach((data2) => {
// //       data2.forEach((data3) => {
// //         data3.forEach((data4) => {
// //           data4.forEach((data5) => {
// //             data5.forEach((data6) => {
// //               let monthhldr = "02/28/2020 12:00 AM";
// //               if (new Date(monthhldr) < new Date(data6.val().out)) {
// //                 if (data.val().in == 'ACTIVE') {
// //                   BigLedgerList.push([data4.key, data5.key, data6.key, "-M9G95BxkhvjBLbSBppZ", data6.val().in]);
// //                 }
// //               }
// //             })
// //           });
// //         });
// //       });
// //     });
// //     resolve(BigLedgerList);
// //   });
// // });

async function retrieveDatabase() {
  // const inventoryList = await getProducts;
  // const dispatchList = await getDispatchItems;
  // const smallLedgerList = await getSmallLedger;
  // const bigLedgerList = await getBigLedger;
  // const cylinderList = await getInventoryData;
//   const icrList = await getICRData;
  // const specSILIst = await getSpecificSIItems;
  // console.log(smallLedgerList);
  // const custCylList = await getCustomerCyls;
  // console.log(smallLedgerList);
  // console.log(bigLedgerList);
  // console.log(ICRData);
  document.querySelector(".loading").style.display = "none";
}
retrieveDatabase();

// function adjustBigLedger(){
//   specsicylarr.forEach((data) => {
//     console.log(data);
//     BigLedgerRef.child('-M9FjzsaMZZsnAKzJ-FM').child('items').child(data).child('Cylinder Record').child('JRRMMC-2').child('04-27-2021 7:44').set({
//       cylinderType: "MED 02",
//       directInvoice: true,
//       drNum: "",
//       in: "ACTIVE",
//       invNum: "JRRMMC-2/27/21",
//       out: "04/27/2021 7:44 AM"
//     }).then(() => {
//       BigLedgerRef.child('-M9FjzsaMZZsnAKzJ-FM').child('items').child(data).child('Cylinder Record').child('JRRMMC-2').child('27').remove()
//       .then(() => {
//         console.log('success');
//       })
//     })
//   })
// }

function addICRReference(){
  SmallLedgerRef.orderByChild("customer").equalTo("PHILIPPINE CHILDRENS MEDICAL CENTER").once("value", (data) => {
    data.forEach((data2) => {
      if(data2.val().status == "Active"){
        CustCylindersRef.child("-MMSR16akWKDpq9a-j0s").child(data2.key).set({
          cylinderType: data2.val().cylinderType,
          lastOut: data2.val().lastOut,
        }).then(() => {
          console.log(data2.key);
        })
      }
    });
  });
}

// // function addCylsToInvoice() {
// //   DispatchList.forEach((data) => {
// //     SIRef.child("0310915").child("items").child(data).set({
// //         CylinderID: data,
// //         CylinderType: "MED 02",
// //         CylinderPrice: "364.39",
// //         sorterkey: databaseRef.push().getKey()
// //       })
// //       .then(() => {
// //         console.log('success');
// //       });
// //   });
// // }

// // function clearLedgerData() {
// //   DispatchList.forEach((data) => {
// //     console.log(data);

// //     for (var y = 0; y < BigLedgerList.length; y++) {
// //       if (data == BigLedgerList[y][0]) {
// //         BigLedgerRef.child('-M9FjzsaMZZsnAKzJ-FM').child('items').child(data).child('Cylinder Record').child('0310990').child('03-04-2020 10:46').remove()
// //           .then(() => {
// //             console.log(data)
// //           })
// //       }
// //     }

// //     for (var x = 0; x < SmallLedgerList.length; x++) {
// //       if (data == SmallLedgerList[x][0]) {
// //         SmallLedgerRef.child(data).child('CylinderRecord').child('03-04-2020 10:46').remove()
// //           .then(() => {
// //             console.log(data)
// //           });
// //       }
// //     }
// //   });
// // }

// function clearCylinderRecordsInventory() {
//   for (var x = 0; x < productrecordsid.length; x++) {
//     productsRef.child(productrecordsid[x]).child('CylinderRecord').remove()
//       .then(() => {
//         console.log(productrecordsid[x]);
//       });
//   }
// }

// function updateICRDate() {
//   let ctr = 0;
//     Object.keys(ICRData[0].Cylinders).forEach(function (key) {
//       for (var x = 0; x < SmallLedgerList.length; x++) {
//         if (SmallLedgerList[x][0] == key) {
//           var refnumber = SmallLedgerList[x][3];
//           var refdate = SmallLedgerList[x][1];
//           BigLedgerRef.child("-M9G95BxkhvjBLbSBppZ").child('items').child(key).child('Cylinder Record').child(refnumber).child(refdate).once('value', function(snapshot) {
//             var exists = (snapshot.val() !== null);
//             if(exists){
//               BigLedgerRef.child("-M9G95BxkhvjBLbSBppZ").child('items').child(key).child('Cylinder Record').child(refnumber).child(refdate).update({
//                 in: ICRData[0].date
//               }).then(() => {
//                 ctr++;
//                 console.log(ctr);
//               }); 
//             }
//           });
//         }
//       }
//     });
// }