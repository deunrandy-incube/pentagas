var userrole = localStorage['userRole'] || 'defaultValue';
var email = document.getElementById("email");
var pass = document.getElementById("pass");

document.getElementById("loginBtn").addEventListener("click", function(event){
  event.preventDefault();
  document.querySelector(".loading").style.display = "block";
  firebase.auth().signInWithEmailAndPassword(email.value, pass.value)
  .then(() => {

    })
  .catch(function(e){
    var errorCode = e.code;
    var errorMessage = e.message;
    alert(errorCode + " " + errorMessage);
    document.querySelector(".loading").style.display = "none";
  });
});

firebase.auth().onAuthStateChanged(function(user) {
  if (user) {
    var uid = user.uid;
    var CheckRef = firebase.database().ref().child("Users").child(uid);
    CheckRef.once("value", function(snapshot){
          userstatus = snapshot.val().Role;
          if(userstatus == "sales"){
            localStorage['userRole'] = 'sales';
            document.querySelector(".loading").style.display = "none";
            window.location = "index.html";
          }
          else if(userstatus == "superadmin"){
            localStorage['userRole'] = 'admin';
            document.querySelector(".loading").style.display = "none";
            window.location = "index.html";
          }
          else if(userstatus = "ccs"){
            localStorage['userRole'] = 'cylcontrol';
            document.querySelector(".loading").style.display = "none";
            window.location = "smallLedger.html";
          }

      });
  } else {
    document.querySelector(".loading").style.display = "none";
  }
});
