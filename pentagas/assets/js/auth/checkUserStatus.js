var userrole = localStorage['userRole'] || 'defaultValue';
var dispnav = localStorage['dispatcherNav'] || 'defaultValue';

var menutabs = document.getElementById("menutabs");
var navigationcheck = '';

if(userrole == 'sales'){
  menutabs.children[1].style.display = "none";
}
else if(userrole == 'cylcontrol'){
  menutabs.children[0].style.display = "none";
}
else if(userrole == 'admin'){
  //nothing
}

function checkDispatcherNav(navigationhldr) {
  localStorage['dispatcherNav'] = navigationhldr;
}