//cylinder balance variables
var items = [];
var vardatelist = [];
var customernames = [];
var cylindervalues = [];
var tempcylinderhldr = [];
var splittedvalues = [];
var datestemphldr = [];
var valueshldr = [];
var finalcustvalues = [];
var finalcylvalues = [];
var nameloopctr = 0;
var cylinderctr = 0;

//PDF variables
var pdfitems = [];

//database references
var databaseRef = firebase.database().ref();
var inventoryRef = databaseRef.child("Inventory");
var deliveryRef = databaseRef.child("Delivery Receipt");
var invoiceRef = databaseRef.child("Sales Invoice");

var drdatelist = [];
var sidatelist = [];
var recordvalues = [];
var drvalues = [];
var sivalues = [];

var dateInput = document.getElementById("dateInput");
var mainDispArr = [];
var dateFilter = '';

var table = '';
//loading of data from DB

function retrieveDRDateData(){
  document.querySelector(".loading").style.display = "block";
  return new Promise((resolve, reject) => {
    datestemphldr = [];
    drdatelist = [];
    deliveryRef.once("value", (data) => {
      data.forEach((data2) => {
        let datesplitter = (data2.val().Date).split(" ");
        if(datesplitter[0] == dateFilter){
          if(datestemphldr.includes(datesplitter[0]) == false){
            datestemphldr.push(datesplitter[0].toString());
            drdatelist.push([datesplitter[0]]);
          }
        }
      })
      resolve(drdatelist);
    })
  })  
}

function retrieveSIDateData(){
  return new Promise((resolve, reject) => {
    sidatelist = [];
    invoiceRef.once("value", (data) => {
      data.forEach((data2) => {
        let datesplitter = (data2.val().Date).split(" ");
        if(datesplitter[0] == dateFilter){
          if(datestemphldr.includes(datesplitter[0].toString()) == false){
            datestemphldr.push(datesplitter[0].toString());
            sidatelist.push([datesplitter[0]]);
          }
        }
      })
      resolve(sidatelist);
    })
  });
}

Date.prototype.isValid = function () {
  // An invalid date object returns NaN for getTime() and NaN is the only
  // object not strictly equal to itself.
  return this.getTime() === this.getTime();
};  

async function refreshCylBalTable(){
  if(dateInput.value.length != 0){
    var datetemp = moment(dateInput.value, 'YYYY/MM/DD');
    dateFilter = datetemp.format('MM/DD/YYYY');
    drdatelist = await retrieveDRDateData();  
    getSIData();  
  }else{
    window.alert("Please select a date to display data");
  }
}

async function getSIData(){
  sidatelist = await retrieveSIDateData();
  vardatelist=[];
  vardatelist = drdatelist.concat(sidatelist);
  vardatelist.sort(function(a, b) {
    var dateA = moment(a);
    var dateB = moment(b);
    return dateA.diff(dateB);
  });
  stateTable(vardatelist);
}

function stateTable(tablearr){
  if(table){
    table.destroy();
  }
  if(tablearr.length == 0){
    window.alert("No data found in this date.");
  }
  table = $('#cylinderbalanceTable').DataTable( {
    data: tablearr,
    select: true,
    columns: [
      { title: "Date" }
    ],
    language: {
      "emptyTable": "Input a date to load data."
    }
  } );
  document.querySelector(".loading").style.display = "none";
}

var date = '';
$('#cylinderbalanceTable tbody').on('click', 'td', (e) => {
  document.querySelector(".loading2").style.display = "block";
  var selectedRow = e.target.parentNode;
  date = selectedRow.children[0].innerText;
  
  recordvalues = [];
  drvalues = [];
  sivalues = [];
  customernames = [];
  getDRItems = new Promise((resolve, reject) => {
    deliveryRef.once("value",(data) => {
      data.forEach((data2) => {
        let datesplitter = data2.val().Date.split(" ");
        if(date == datesplitter[0]){
          if(customernames.indexOf(data2.val().custName) == -1){
            customernames.push(data2.val().custName);
          }

          recordvalues.push({
            "key": data2.key,
            "customer": data2.val().custName,
            "date": datesplitter[0],
            "unit": getMapSize(data2.val().items),
            "cylinderType": data2.val().cylinderType,
          })
        }
      })
      resolve(recordvalues);
    })
  });

  getSIItems = new Promise((resolve, reject) => {
    invoiceRef.once("value",(data) => {
      data.forEach((data2) => {
        if(data2.val().directInvoice == true){
          let datesplitter = data2.val().Date.split(" ");
          let cyltype = '';
          let med02ctrtemp = 0;
          let cairctrtemp = 0;
          let n20ctrtemp = 0;
          let tech02ctrtemp = 0;
          let c2h2ctrtemp = 0;
          let c02ctrtemp = 0;
          let n2ctrtemp = 0;
          let argonctrtemp = 0;

          if(date == datesplitter[0]){
            if(customernames.indexOf(data2.val().CustomerName) == -1){
              customernames.push(data2.val().CustomerName);
            }

            data2.forEach((data3) => {
              if(data3.key === 'items'){
                data3.forEach((data4) => {
                  cyltype = data4.val().CylinderType;
                  if(cyltype == "MED 02"){
                    med02ctrtemp++;
                  }
                  else if(cyltype == "COMP AIR"){
                    cairctrtemp++;
                  }
                  else if(cyltype == "NITROUS DIOXIDE"){
                    n20ctrtemp++;
                  }
                  else if(cyltype == "TECH 02"){
                    tech02ctrtemp++;
                  }
                  else if(cyltype == "ACETYLENE"){
                    c2h2ctrtemp++;
                  }
                  else if(cyltype == "CARBON DIOXIDE"){
                    c02ctrtemp++;
                  }
                  else if(cyltype == "NITROGEN"){
                    n2ctrtemp++;
                  }
                  else if(cyltype == "ARGON"){
                    argonctrtemp++;
                  }
                });
              }
            });
              
            if(med02ctrtemp!=0){
              recordvalues.push({
                "key": data2.key,
                "customer": data2.val().CustomerName,
                "date": datesplitter[0],
                "unit": med02ctrtemp,
                "cylinderType": "MED 02",
              })
            }
            if(cairctrtemp!=0){
              recordvalues.push({
                "key": data2.key,
                "customer": data2.val().CustomerName,
                "date": datesplitter[0],
                "unit": cairctrtemp,
                "cylinderType": "COMP AIR",
              })
            }
            if(n20ctrtemp!=0){
              recordvalues.push({
                "key": data2.key,
                "customer": data2.val().CustomerName,
                "date": datesplitter[0],
                "unit": n20ctrtemp,
                "cylinderType": "NITROUS DIOXIDE",
              })
            }
            if(tech02ctrtemp!=0){
              recordvalues.push({
                "key": data2.key,
                "customer": data2.val().CustomerName,
                "date": datesplitter[0],
                "unit": tech02ctrtemp,
                "cylinderType": "TECH 02",
              })
            }
            if(c2h2ctrtemp!=0){
              recordvalues.push({
                "key": data2.key,
                "customer": data2.val().CustomerName,
                "date": datesplitter[0],
                "unit": c2h2ctrtemp,
                "cylinderType": "ACETYLENE",
              })
            }
            if(c02ctrtemp!=0){
              recordvalues.push({
                "key": data2.key,
                "customer": data2.val().CustomerName,
                "date": datesplitter[0],
                "unit": c02ctrtemp,
                "cylinderType": "CARBON DIOXIDE",
              })
            }
            if(n2ctrtemp!=0){
              recordvalues.push({
                "key": data2.key,
                "customer": data2.val().CustomerName,
                "date": datesplitter[0],
                "unit": n2ctrtemp,
                "cylinderType": "NITROGEN",
              })
            }
            if(argonctrtemp!=0){
              recordvalues.push({
                "key": data2.key,
                "customer": data2.val().CustomerName,
                "date": datesplitter[0],
                "unit": argonctrtemp,
                "cylinderType": "ARGON",
              })
            }

          }
        }
      })
      resolve(recordvalues);
    })
  })

  retrieveCylTotalDRDataAW();
});

function displayDetails(itemsarr){
  table2 = $('#ledgerItemsTable').DataTable( {
      data: itemsarr,
      select: true,
      columns: [
        { title: "Customer Name" },
        { title: "Med 02" },
        { title: "C-Air" },
        { title: "N20" },
        { title: "TECH 02" },
        { title: "C2H2" },
        { title: "C02" },
        { title: "N2" },
        { title: "Argon" }
      ]
  } );
  document.querySelector(".loading2").style.display = "none";
  $('#myModal').modal({
    backdrop: 'static',
    keyboard: false
  });
  $('#myModal').modal("show");
}


async function retrieveCylTotalDRDataAW() {
  document.querySelector(".modal-title").textContent = date;
  const drvalues = await getDRItems;
  retrieveCylTotalSIDataAW();
}

async function retrieveCylTotalSIDataAW(){
  const sivalues = await getSIItems;
  recordvalues = drvalues.concat(sivalues);
  getCylinderTally();
}

function getCylinderTally(){
  //Initialize Table Variables
  var items = [];
  var med02ctr = 0;
  var cairctr = 0;
  var n20ctr = 0;
  var tech02ctr = 0;
  var c2h2ctr = 0;
  var c02ctr = 0;
  var n2ctr = 0;
  var argonctr = 0;

  finalcustvalues = [];
  finalcylvalues = [];
  valueshldr = [];

  for(var namectr=0;namectr<customernames.length;namectr++){
    for(var recordctr=0;recordctr<recordvalues.length;recordctr++){
      if(recordvalues[recordctr].customer == customernames[namectr]){       //IF name exists. Set values to temp array
        valueshldr.push(recordvalues[recordctr]);
      }
    }
    finalcustvalues.push(customernames[namectr]);
    finalcylvalues.push(valueshldr);
    valueshldr = [];
  }

  for(var custctr=0;custctr<finalcustvalues.length;custctr++){
    for(balctr=0;balctr<finalcylvalues.length;balctr++){
      let custtemp = '';
      for(recctr=0;recctr<finalcylvalues[balctr].length;recctr++){
        let cyltype = finalcylvalues[balctr][recctr].cylinderType;
        let unitnumber = finalcylvalues[balctr][recctr].unit;
        custtemp = finalcylvalues[balctr][recctr].customer;
        if(cyltype == "MED 02"){
          med02ctr+=parseInt(unitnumber);
        }
        else if(cyltype == "COMP AIR"){
          cairctr+=parseInt(unitnumber);
        }
        else if(cyltype == "NITROUS DIOXIDE"){
          n20ctr+=parseInt(unitnumber);
        }
        else if(cyltype == "TECH 02"){
          tech02ctr+=parseInt(unitnumber);
        }
        else if(cyltype == "ACETYLENE"){
          c2h2ctr+=parseInt(unitnumber);
        }
        else if(cyltype == "CARBON DIOXIDE"){
          c02ctr+=parseInt(unitnumber);
        }
        else if(cyltype == "NITROGEN"){
          n2ctr+=parseInt(unitnumber);
        }
        else if(cyltype == "ARGON"){
          argonctr+=parseInt(unitnumber);
        }
      }
      if(custtemp == finalcustvalues[custctr]){
        items.push([finalcustvalues[custctr], med02ctr, cairctr, n20ctr, tech02ctr, c2h2ctr, c02ctr, n2ctr, argonctr]);
        pdfitems.push(custtemp+","+med02ctr+","+cairctr+","+n20ctr+","+tech02ctr+","+c2h2ctr+","+c02ctr+","+n2ctr+","+argonctr);
      }
      med02ctr = 0;
      cairctr = 0;
      n20ctr = 0;
      tech02ctr = 0;
      c2h2ctr = 0;
      c02ctr = 0;
      n2ctr = 0;
      argonctr = 0;
    }
  }
  displayDetails(items);
}

function getMapSize(x) {
  var len = 0;
  for (var count in x) {
          len++;
  }
  return len;
}

function clearTables() {
  table2.destroy();
}




// getLedgerItems = new Promise((resolve, reject) => {
//   inventoryRef.once("value", (data) => {
//     data.forEach((data2) => {
//       data2.forEach(function(data3){
//         data3.forEach(function(data4){
//             if(date == data4.val().recorddate){
//               if(customernames.indexOf(data4.val().recordcust) == -1){  //Check if Data already exists in array
//                 customernames.push(data4.val().recordcust);
//               }
//               if(tempcylinderhldr.indexOf(data2.key) == -1){
//                 tempcylinderhldr.push(data2.key);
//                 cylindervalues.push(data2.key+","+data2.val().cylindertype+","+data4.val().recordcust+","+data4.val().custCode);
//               }
//             }
//           });
//       });
//     });

//     for(var namectr=0;namectr<customernames.length;namectr++){
//         for(var cylctr=0;cylctr<cylindervalues.length;cylctr++){
//           splittedvalues = cylindervalues[cylctr].split(",");    //Split Values
//           if(splittedvalues[2] == customernames[namectr]){       //IF name exists. Set values to temp array
//             valueshldr.push(cylindervalues[cylctr]);
//           }
//       }
//       finalcustvalues.push(customernames[namectr]);
//       finalcylvalues.push(valueshldr);
//       valueshldr = [];
//     }

//     for(var cylctr=0;cylctr<finalcustvalues.length;cylctr++){
//       cylinderhldr = finalcylvalues[cylctr];
//       for(balctr=0;balctr<cylinderhldr.length;balctr++){
//         splittedvalues = cylinderhldr[balctr].split(",");    //Split Values
//         if(splittedvalues[1] == "MED 02"){
//           med02ctr++;
//         }
//         else if(splittedvalues[1] == "COMP AIR"){
//           cairctr++;
//         }
//         else if(splittedvalues[1] == "NITROUS DIOXIDE"){
//           n20ctr++;
//         }
//         else if(splittedvalues[1] == "TECH 02"){
//           tech02ctr++;
//         }
//         else if(splittedvalues[1] == "ACETYLENE"){
//           c2h2ctr++;
//         }
//         else if(splittedvalues[1] == "CARBON DIOXIDE"){
//           c02ctr++;
//         }
//         else if(splittedvalues[1] == "NITROGEN"){
//           n2ctr++;
//         }
//         else if(splittedvalues[1] == "ARGON"){
//           argonctr++;
//         }
//       }
      
//       items.push([finalcustvalues[cylctr], med02ctr, cairctr, n20ctr, tech02ctr, c2h2ctr, c02ctr, n2ctr, argonctr]);
//       pdfitems.push(splittedvalues[3]+","+med02ctr+","+cairctr+","+n20ctr+","+tech02ctr+","+c2h2ctr+","+c02ctr+","+n2ctr+","+argonctr);
//       med02ctr = 0;
//       cairctr = 0;
//       n20ctr = 0;
//       tech02ctr = 0;
//       c2h2ctr = 0;
//       c02ctr = 0;
//       n2ctr = 0;
//       argonctr = 0;
//     }
//     resolve(items);
//   });
// });