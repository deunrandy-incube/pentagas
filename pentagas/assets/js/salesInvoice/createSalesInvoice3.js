//for PDF valids
var forViewing = "false";
var PDFDispatchList = [];
var pdfunitprice = '';
var pdftotalamtdue = '';
var pdfvatamt = '';
var pdffinaltotal = '';

var selectedDispatch = JSON.parse(localStorage.getItem("selectedDispatch"));
var CustomerName = localStorage.getItem("CustomerName");

document.querySelector(".loading").style.display = "block";
//database references
var databaseRef = firebase.database().ref();
var DispatchRef = databaseRef.child("Dispatch Slip")
var SIRef = databaseRef.child("Sales Invoice");
var SORef = databaseRef.child("Sales Order");
var customerRef = databaseRef.child("Customer");
var bigledgerRef = databaseRef.child("Big Ledger");
var smallledgerRef = databaseRef.child("Small Ledger")
var custCylindersRef = databaseRef.child("Customer Cylinder");

//input fields
var invNum = document.getElementById("InvoiceNum");
var customername = document.getElementById("CustomerName");
var custaddress = document.getElementById("Address");
var tinNum = document.getElementById("TinBuyer");
var busType = document.getElementById("BusinessStyle");
var date = document.getElementById("Date");
var PONum = document.getElementById("PONum");
var PGCSONo = document.getElementById("PGCSONo");
var terms = document.getElementById("Terms");
var PRNo = document.getElementById("PRNum");
var SODate = document.getElementById("SODate");
var Salesman = document.getElementById("Salesman");
var RefDRNum = document.getElementById("RefDRNum");
var RefICRNum = document.getElementById("RefICRNum");
var PreparedBy = document.getElementById("PreparedBy");
var ReceivedBy = document.getElementById("ReceivedBy");
var DeliveredBy = document.getElementById("DeliveredBy");
var DriversLicense = document.getElementById("DriversLicense");
var TruckPlate = document.getElementById("TruckPlate");
var invoiceFormat = document.getElementById("invoiceFormat");


//variables
var table = '';
var DRCustAddress = '';
var DRTinNum = '';
var DRdate = '';
var DRPONum = '';
var DRterms = '';
var DRPRNo = '';
var DRSalesman = '';
var DRRefDRNum = '';
var DRPreparedBy = '';
var DRReceivedBy = '';
var DRDeliveredBy = '';
var DRDriversLicense = '';
var DRTruckPlate = '';
var DRSONum = '';
var DRSODate = '';
var unitPrice = '';

//get date today
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth() + 1;
var yyyy = today.getFullYear();
var hr = today.getHours();
var min = today.getMinutes();
var sec = today.getSeconds();

if (dd < 10) {
  dd = '0' + dd;
}

if (mm < 10) {
  mm = '0' + mm;
}
var datetoday = mm + '/' + dd + '/' + yyyy + " " + hr + ":" + min + ":" + sec + " AM";

const getDispatchList = new Promise((resolve, reject) => {
  var DispatchList = [];
  selectedDispatch.forEach(value => {
    DispatchRef.child(value).once("value", (data2) => {
      DispatchList.push(data2.key);
      DRCustAddress = data2.val().address;
      DRdate = datetoday;
      DRSONum = data2.val().SONumber;
      resolve(DispatchList);
    })
  })
});

const getSOInfo = soNum => {
  return new Promise((resolve, reject) => {
    var SOInfo = {};
    SORef.child(soNum).once("value", (data) => {
      SOInfo.PRNum = data.val().PRNum;
      SOInfo.Terms = data.val().Terms;
      SOInfo.custTINNum = data.val().custTINNum;
      SOInfo.PONum = data.val().PONum;
      SOInfo.createDate = data.val().createDate;
      SOInfo.customerID = data.val().customerID;
      SOInfo.custCode = data.val().custCode;
      resolve(SOInfo);
    })
  })
}

const getDispatchItems = dispatchIDS => {
  return new Promise((resolve, reject) => {
    var DispatchList = [];
    var ctr = 0;
    var code = '';
    dispatchIDS.forEach(ids => {
      DispatchRef.child(ids).child("Cylinders").orderByChild("sorterkey").once("value", (data) => {
        data.forEach((data2) => {
          switch (data2.val().type) {
            case "MED 02":
              code = 0;
              break;
            case "MED 02 FLASK":
              code = 1;
              break;
            case "MED 02 BANTAM":
              code = 2;
              break;
            case "MED 02 MEDIUM":
              code = 3;
              break;
            case "TECH 02":
              code = 4;
              break;
            case "ARGON":
              code = 5;
              break;
            case "COMP AIR":
              code = 6;
              break;
            case "ACETYLENE":
              code = 7;
              break;
            case "NITROGEN":
              code = 8;
              break;
            case "NITROUS DIOXIDE":
              code = 9;
              break;
            case "CARBON DIOXIDE":
              code = 10;
              break;
          }
          DispatchList.push([data2.key, data2.val().type, code]);
          PDFDispatchList.push([data2.key, data2.val().type, code]);
        });
        ctr++;
        if (ctr == dispatchIDS.length) {
          resolve(DispatchList);
        }
      });
    });

  });
}

const getItemPrices = customerID => {
  return new Promise((resolve, reject) => {
    var ItemPrice = [];
    customerRef.child(customerID).child("Item Prices").once("value", (data) => {
      data.forEach((data2) => {
        ItemPrice.push([data2.val().code, data2.val().Price]);
      });
      resolve(ItemPrice);
    });
  })
}


var DRList = [];
var DispatchList = [];
var DispatchItems = [];
var DispatchItemList = []
var SIList = [];
var ItemPrices = [];
var SOInfo = {};
async function retrieveDataAW() {
  DispatchList = await getDispatchList;
  DispatchItems = await getDispatchItems(DispatchList);
  SOInfo = await getSOInfo(DRSONum);
  ItemPrices = await getItemPrices(SOInfo.customerID);
  $("#InvoiceNum").focus();
  table = $('#DRTable').DataTable({
    data: DispatchItems,
    ordering: false,
    select: true,
    columns: [{
        title: "Cylinder ID."
      },
      {
        title: "Cylinder Type"
      }
    ]
  });

  customername.value = CustomerName;
  custaddress.value = DRCustAddress;
  tinNum.value = SOInfo.custTINNum;
  date.value = DRdate;
  PONum.value = SOInfo.PONum;
  terms.value = SOInfo.Terms;
  PRNo.value = SOInfo.PRNum;
  PGCSONo.value = DRSONum;
  SODate.value = SOInfo.createDate;
  document.querySelector(".loading").style.display = "none";
}
retrieveDataAW();

var format = invoiceFormat.options[invoiceFormat.selectedIndex].value;
invoiceFormat.addEventListener("click", (event) => {
  format = invoiceFormat.options[invoiceFormat.selectedIndex].value;
});
document.getElementById("createSalesInvoiceBtn").addEventListener("click", (event) => {
  event.preventDefault();
  SIRef.child(invNum.value).once("value", (data) => {
    if(data.exists()){
      window.alert(invNum.value+" is existing, cannot create an Invoice with the same ID.");
    }else{
      if (confirm("You are about to create a Sales Invoice. Would you like to continue?")){
        document.querySelector(".loading2").style.display = "block";
        validate();
      } else {
        // do something.
      }
    }
  });
});

//cancel button
document.getElementById("cancelSalesInvoiceBtn").addEventListener("click", (event) => {
  event.preventDefault();
  if (confirm("You are about to discard this session. Would you like to continue?")){
    window.location = "createSalesInvoice.html";
  } else {
    // do something.
  }
});

function validate() {
  var inputs = document.getElementsByTagName("input");
  for (var i = 0; i < 20; i++) {
    if (i === 14) {
      continue;
    } else {
      if (inputs[i].value == "") {
        if (i == 0) {
          alert("Invoice number cannot be empty");
          break;
        }
        else {
          inputs[i].value = "--";
        }

      }
    }
    if (i === 19) {
      createSI();
    }
  }
  document.querySelector(".loading").style.display = "none";
}

var itemDesc = document.getElementById("ItemDesc");
var truckPlate = document.getElementById("TruckPlate");

async function createSI() {
  document.querySelector(".loading").style.display = "block";
  var checker = [false, false, false, false, false, false, false];
  var itemsProcessed = [0, 0, 0, 0, 0];
  SIRef.child(invNum.value).set({
      CustomerID: SOInfo.customerID,
      CustomerName: customername.value,
      CustCode: SOInfo.custCode,
      Address: custaddress.value,
      TinBuyer: tinNum.value,
      BusinessStyle: busType.value,
      Date: date.value,
      PONum: PONum.value,
      PGCSONo: PGCSONo.value,
      Terms: terms.value,
      PRNum: PRNo.value,
      SODate: SODate.value,
      Salesman: Salesman.value,
      RefDRNum: RefDRNum.value,
      RefICRNum: RefICRNum.value,
      PreparedBy: PreparedBy.value,
      ReceivedBy: ReceivedBy.value,
      DeliveredBy: DeliveredBy.value,
      DriversLicense: DriversLicense.value,
      TruckPlate: truckPlate.value,
      itemDesc: itemDesc.options[itemDesc.selectedIndex].value,
      Format: format,
      directInvoice: true,
      SONum: DRSONum
    })
    .then(() => {
      checker[0] = true;
      checkerFunc(checker);
    });

  let itemDeliveredQty = await new Promise((resolve, reject) => {
    SORef.child(DRSONum).child("items").child(DispatchItems[0][1]).once("value", (data) => {
      resolve(data.val().itemDelivered);
    })
  });

  var cylQuantityVal = await new Promise((resolve, reject) => {
    SORef.child(DRSONum).child("items").child(DispatchItems[0][1]).once("value", (data) => {
      resolve(data.val().itemQty);
    })
  });

  let SOStatus = 'Partially Invoiced';
  
  if (Number(itemDeliveredQty) + Number(DispatchItems.length) == Number(cylQuantityVal)){
    // SOStatus = 'Invoiced'
  }
  SORef.child(DRSONum).update({
    status: SOStatus
  })
  .then(() => {
    SORef.child(DRSONum).child("items").child(DispatchItems[0][1]).update({
      itemDelivered: Number(itemDeliveredQty) + Number(DispatchItems.length)
    })
    .then(() => {
      checker[1] = true;
      checkerFunc(checker);
    })
  });
  DispatchItems.forEach((data) => {
    var price = '';
    ItemPrices.forEach((itemData) => {
      if (itemData[0] == data[2]) {
        price = itemData[1];
      }
    })
    
    let smdatesplitter = (date.value).split(" ");
    let newdatesplit;
      if(smdatesplitter[0].indexOf("/") != -1){
        newdatesplit = smdatesplitter[0].split("/");
      }else if(smdatesplitter[0].indexOf("-") != -1){
        newdatesplit = smdatesplitter[0].split("-");
      }
    let newdate = newdatesplit[0]+"-"+newdatesplit[1]+"-"+newdatesplit[2];
    SIRef.child(invNum.value).child("items").child(data[0]).set({
        CylinderID: data[0],
        CylinderType: data[1],
        CylinderPrice: price,
        sorterkey: databaseRef.push().getKey()
      })
      .then(() => {
        itemsProcessed[0]++;        
        if (itemsProcessed[0] === DispatchItems.length) {
          checker[2] = true;
          checkerFunc(checker);
        }
      });
    bigledgerRef.child(SOInfo.customerID).update({
      customerName: customername.value,
      lastUpdate: datetoday
    })
    bigledgerRef.child(SOInfo.customerID).child("items").child(data[0]).child("Cylinder Record").child(invNum.value).child(newdate+" "+smdatesplitter[1]).set({
      drNum: '',
      invNum: invNum.value,
      directInvoice: true,
      cylinderType: data[1],
      out: date.value,
      in: "ACTIVE"
    })
    .then(() => {
      itemsProcessed[2]++;        
        if (itemsProcessed[2] === DispatchItems.length) {
          checker[4] = true;
          checkerFunc(checker);
        }
    })
    
    smallledgerRef.child(data[0]).update({
      customer: CustomerName,
      lastUpdate: datetoday,
      lastOut: date.value,
      cylinderType: data[1],
      status: "Active",
      directInvoice: true
    })
    .then((e) => {
      smallledgerRef.child(data[0]).child("CylinderRecord").child(newdate+" "+smdatesplitter[1]).set({
        out: date.value,
        customer: CustomerName,
        customerID: SOInfo.customerID,
        custCode: SOInfo.custCode,
        in: "ACTIVE",
        invoiceNum: invNum.value
      })
      .then((e) => {
        itemsProcessed[3]++;
        if (itemsProcessed[3] === DispatchItems.length){
          checker[5] = true;
          checkerFunc(checker);
        }

      })
    })

    custCylindersRef.child(SOInfo.customerID).child(data[0]).set({
      cylinderType: data[1],
      lastOut: date.value
    }).then((e) => {
      itemsProcessed[4]++;
      if (itemsProcessed[4] === DispatchItems.length){
        checker[6] = true;
        checkerFunc(checker);
      }
    })
  });
  DispatchList.forEach((data) => {
    DispatchRef.child(data).update({
        status: "Invoiced"
      })
      .then(() => {
        itemsProcessed[1]++;
        if (itemsProcessed[1] === DispatchList.length) {
          checker[3] = true;
          checkerFunc(checker);
        }
      });
  })
  
}


function checkerFunc(checker) {
  console.log(checker);
  if (checker[0] && checker[1] && checker[2] && checker[3] && checker[4] && checker[5] && checker[6]) {
    window.location = "createSalesInvoice.html";
  }
}