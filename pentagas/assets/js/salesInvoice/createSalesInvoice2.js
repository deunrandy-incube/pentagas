//for PDF valids
var forViewing = "false";

var selectedDR = JSON.parse(localStorage.getItem("selectedDR"));
var selectedDispatch = JSON.parse(localStorage.getItem("selectedDispatch"));
var CustomerName = localStorage.getItem("CustomerName");

document.querySelector(".loading").style.display = "block";
//database references
var databaseRef = firebase.database().ref();
var DRRef = databaseRef.child("Delivery Receipt");
var DispatchRef = databaseRef.child("Dispatch Slip")
var SIRef = databaseRef.child("Sales Invoice");
var SORef = databaseRef.child("Sales Order");
var bigledgerRef = databaseRef.child("Big Ledger");

//input fields
var invNum = document.getElementById("InvoiceNum");
var customername = document.getElementById("CustomerName");
var custaddress = document.getElementById("Address");
var tinNum = document.getElementById("TinBuyer");
var busType = document.getElementById("BusinessStyle");
var date = document.getElementById("Date");
var PONum = document.getElementById("PONum");
var PGCSONo = document.getElementById("PGCSONo");
var terms = document.getElementById("Terms");
var PRNo = document.getElementById("PRNum");
var SODate = document.getElementById("SODate");
var Salesman = document.getElementById("Salesman");
var RefDRNum = document.getElementById("RefDRNum");
var RefICRNum = document.getElementById("RefICRNum");
var PreparedBy = document.getElementById("PreparedBy");
var ReceivedBy = document.getElementById("ReceivedBy");
var DeliveredBy = document.getElementById("DeliveredBy");
var DriversLicense = document.getElementById("DriversLicense");
var TruckPlate = document.getElementById("TruckPlate");
var invoiceFormat = document.getElementById("invoiceFormat");

//variables
var table = '';
var DRNumber = '';
var DRCustCode = '';
var DRCustAddress = '';
var DRTinNum = '';
var DRdate = '';
var DRPONum = '';
var DRterms = '';
var DRPRNo = '';
var DRSalesman = '';
var DRRefDRNum = '';
var DRPreparedBy = '';
var DRReceivedBy = '';
var DRDeliveredBy = '';
var DRDriversLicense = '';
var DRTruckPlate = '';
var DRCustId = '';
var DRCustNum = '';
var DRSODate = '';
var unitPrice = '';

//get date today
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1;
var yyyy = today.getFullYear();
var hr = today.getHours();
var min = today.getMinutes();
var sec = today.getSeconds();
var SOInfo;

if(dd<10)
{
  dd='0'+dd;
}

if(mm<10)
{
  mm='0'+mm;
}
var datetoday = mm + '/' + dd + '/' + yyyy + " " + hr + ":" + min + ":" + sec + " AM";

const getSOInfo = soNum => {
  return new Promise((resolve, reject) => {
    var SOInfo = {};
    if(CustomerName == "SAN JUAN MEDICAL CENTER"){
      let soArrTemp = soNum.split(", ");
      let SoInfoArr = [];
      for(var x=0; x<soArrTemp.length; x++){
        SORef.child(soArrTemp[x]).once("value", (data) => {
          SOInfo.items = data.val().items;
          SoInfoArr.push(SOInfo);
        })  
        resolve(SoInfoArr);
      }
    }else{
      SORef.child(soNum).once("value", (data) => {
        SOInfo.items = data.val().items;
        resolve(SOInfo);
      })
    }
    
  })
}

const getDRList = new Promise((resolve, reject) => {
  var DRList = [];
  var SJSOList = [];
  let soctr = 0;
  let sonumtemp;
  DRRef.once("value", (data) => {
    data.forEach((data2) => {
      if (selectedDR.includes(data2.key)){
        if(CustomerName == "SAN JUAN MEDICAL CENTER"){
          if(soctr == 0){
            DRSONum = data2.val().SONum;
            sonumtemp = data2.val().SONum;
          }else{
            DRSONum = sonumtemp.concat(', ', data2.val().SONum);
            sonumtemp = data2.val().SONum;
          }
          soctr++;
          SJSOList.push(data2.val().SONum);
        }else{
          DRSONum = data2.val().SONum;
        }

        DRNumber = data2.key;
        DRCustAddress = data2.val().address;
        DRTinNum = data2.val().custTINNum;
        DRdate = datetoday;
        DRPONum = data2.val().PONum;
        DRterms = data2.val().Terms;
        DRPRNo = data2.val().PRNum;
        DRSalesman = data2.val().Salesman;
        DRRefDRNum = data2.val().RefDRNum;
        DRPreparedBy = data2.val().PreparedBy;
        DRReceivedBy = data2.val().ReceivedBy;
        DRDeliveredBy = data2.val().DeliveredBy;
        DRDriversLicense = data2.val().DriversLicense;
        DRTruckPlate = data2.val().TruckPlate;
        DRCustId = data2.val().custID;
        DRCustCode = data2.val().custCode;
        DRSODate = data2.val().SODate;
        unitPrice = data2.val().unitPrice;
        var datesplitter = data2.val().Date.split(" ");   //Split Date and Time
        DRList.push([data2.key, datesplitter[0], data2.val().itemQty, data2.val().unitPrice, data2.val().items, data2.val().Date]);
      }
    })
    resolve(DRList);
  })
});

var DRList = [];
var DispatchList = [];
var DispatchItemList = []
async function retrieveDataAW(){
  DRList = await getDRList;
  SOInfo = await getSOInfo(DRSONum);
  console.log(SOInfo);
  $("#InvoiceNum").focus();
  if (DRList.length != 0){
    table = $('#DRTable').DataTable( {
        data: DRList,
        select: true,
        columns: [
          { title: "DR No." },
          { title: "Date" },
          { title: "Quantity"},
          { title: "Unit Price"}
        ]
    } );
  }

  customername.value = CustomerName;
  custaddress.value = DRCustAddress;
  tinNum.value = DRTinNum;
  date.value = DRdate;
  PONum.value = DRPONum;
  terms.value = DRterms;
  PRNo.value = DRPRNo;
  Salesman.value = DRSalesman;
  RefDRNum.value = DRRefDRNum;
  PreparedBy.value = DRPreparedBy;
  ReceivedBy.value = DRReceivedBy;
  DeliveredBy.value = DRDeliveredBy;
  DriversLicense.value = DRDriversLicense;
  TruckPlate.value = DRTruckPlate;
  PGCSONo.value = DRSONum;
  SODate.value = DRSODate;
  document.querySelector(".loading").style.display = "none";
}
retrieveDataAW();

var format = invoiceFormat.options[invoiceFormat.selectedIndex].value;
invoiceFormat.addEventListener("click", (event) => {
  format = invoiceFormat.options[invoiceFormat.selectedIndex].value;
});
document.getElementById("createSalesInvoiceBtn").addEventListener("click", (event) => {
  event.preventDefault();
  SIRef.child(invNum.value).once('value', function(snapshot) {
    var exists = (snapshot.val() !== null);
    if(!exists){
      if (confirm("You are about to create a Sales Invoice. Would you like to continue?")){
        document.querySelector(".loading2").style.display = "block";
        validate();
      } else {
        // do something.
      }
    }else{
      window.alert(invNum.value+" is existing, cannot create an Invoice with the same ID.");
    }
  });
});

document.getElementById("cancelSalesInvoiceBtn").addEventListener("click", (event) => {
  event.preventDefault();
  if (confirm("You are about to discard this session. Would you like to continue?")){
    window.location = "createSalesInvoice.html";
  } else {
    // do something.
  }
});

function validate(){
  var inputs = document.getElementsByTagName("input");
  for (var i = 0; i < 20; i++){
    if (i === 14){
      continue;
    } else {
      if (inputs[i].value == ""){
        if (i == 0){
          alert("Invoice number cannot be empty");
          break;
        }
        else{
          inputs[i].value = "--";
        }

      }
    }
    if (i === 19) {
      createSI();
    }
  }
  document.querySelector(".loading2").style.display = "none";
}

var itemDesc = document.getElementById("ItemDesc");
var truckPlate = document.getElementById("TruckPlate");

function createSI(){
  var checker = [false, false, false, false, false];
  var itemsProcessed = [0,0];
  SIRef.child(invNum.value).set({
    CustomerID: DRCustId,
    CustomerName: customername.value,
    CustCode: DRCustCode,
    Address: custaddress.value,
    TinBuyer: tinNum.value,
    BusinessStyle: busType.value,
    Date: date.value,
    PONum: PONum.value,
    PGCSONo: PGCSONo.value,
    Terms: terms.value,
    PRNum: PRNo.value,
    SODate: SODate.value,
    Salesman: Salesman.value,
    RefDRNum: RefDRNum.value,
    RefICRNum: RefICRNum.value,
    PreparedBy: PreparedBy.value,
    ReceivedBy: ReceivedBy.value,
    DeliveredBy: DeliveredBy.value,
    DriversLicense: DriversLicense.value,
    TruckPlate: truckPlate.value,
    itemDesc: itemDesc.options[itemDesc.selectedIndex].value,
    Format: invoiceFormat.value,
    unitPrice: unitPrice,
    directInvoice: false,
    SONum: DRSONum
  })
  .then(() => {
    checker[0] = true;
    checkerFunc(checker);
  });
  let SOStatus = "Partially Invoiced"
  if(CustomerName == "SAN JUAN MEDICAL CENTER"){
    let soArrTemp = DRSONum.split(", ");
    for(var x=0; x<soArrTemp.length; x++){
      SORef.child(soArrTemp[x]).update({
        status: SOStatus
      })
      .then(() => {
        checker[1] = true;
        checkerFunc(checker);
      });
    }
  }else{
    SORef.child(DRSONum).update({
      status: SOStatus
    })
    .then(() => {
      checker[1] = true;
      checkerFunc(checker);
    });
  }

  DRList.forEach((data) => {
    SIRef.child(invNum.value).child("items").child(data[0]).set({
      DRNum: data[0],
      DRDate: data[1],
      DRQty: data[2],
      sorterkey: databaseRef.push().getKey()
    })
    .then(() => {
      itemsProcessed[0]++;
      if (itemsProcessed[0] === DRList.length){
        checker[2] = true;
        checkerFunc(checker);
      }
    });
    DRRef.child(data[0]).update({
      status: "Invoiced"
    })
    .then(() => {
      itemsProcessed[1]++;
      if (itemsProcessed[1] === DRList.length){
        checker[3] = true;
        checkerFunc(checker);
      }
    });
    bigledgerRef.child(DRCustId).update({
      customerName: CustomerName,
      lastUpdate: datetoday
    })
    let index = 0
    for (let item in data[4]){
      let smdatesplitter = data[5].split(" ");
      let newdatesplit;
        
      if(smdatesplitter[0].indexOf("/") != -1){
        newdatesplit = smdatesplitter[0].split("/");
      }else if(smdatesplitter[0].indexOf("-") != -1){
        newdatesplit = smdatesplitter[0].split("-");
      }

      let newdate = newdatesplit[0]+"-"+newdatesplit[1]+"-"+newdatesplit[2];
      bigledgerRef.child(DRCustId).child("items").child(item).child("Cylinder Record").child(data[0]).child(newdate+" "+smdatesplitter[1]).update({
        invNum: invNum.value,
      })
      .then(() => {
        if (index == (Object.keys(data[4]).length)){
          checker[4] = true;
          checkerFunc(checker);
        }
      })
      index++;
    }
  });
}


function checkerFunc(checker){
  if (checker[0] && checker[1] && checker[2] && checker[3] && checker[4]){
    let SOStatus = "Invoiced"
    if(CustomerName == "SAN JUAN MEDICAL CENTER"){
      let soArrTemp = DRSONum.split(", ");
      for(var x=0; x<soArrTemp.length; x++){
        Object.values(SOInfo[x].items).forEach(item => {
          if (!item.isFullyInvoiced){
            SOStatus = 'Partially Invoiced'
          }
        })
        SORef.child(soArrTemp[x]).update({
          status: SOStatus
        })
        .then(() => {
          window.location = "createSalesInvoice.html";
        })  
      }
    }else{
      Object.values(SOInfo.items).forEach(item => {
        if (!item.isFullyInvoiced){
          SOStatus = 'Partially Invoiced'
        }
      })
      SORef.child(DRSONum).update({
        status: SOStatus
      })
      .then(() => {
        window.location = "createSalesInvoice.html";
      })
    }
    
  }
}
