//for PDF valids
var forViewing = "ownersfalse";
var PDFDispatchList = [];
var pdfunitprice = '';
var pdftotalamtdue = '';
var pdfvatamt = '';
var pdffinaltotal = '';

var ICRNum = localStorage.getItem("ICRNum");


document.querySelector(".loading").style.display = "block";
//database references
var databaseRef = firebase.database().ref();
var DRRef = databaseRef.child("Delivery Receipt");
var DispatchRef = databaseRef.child("Dispatch Slip")
var SIRef = databaseRef.child("Sales Invoice");
var SORef = databaseRef.child("Sales Order");
var customerRef = databaseRef.child("Customer");
var ICRRef = databaseRef.child("Incoming Cylinder Receipt");

//input fields
var invNum = document.getElementById("InvoiceNum");
var customername = document.getElementById("CustomerName");
var custaddress = document.getElementById("Address");
var tinNum = document.getElementById("TinBuyer");
var busType = document.getElementById("BusinessStyle");
var date = document.getElementById("Date");
var PONum = document.getElementById("PONum");
var PGCSONo = document.getElementById("PGCSONo");
var terms = document.getElementById("Terms");
var PRNo = document.getElementById("PRNum");
var SODate = document.getElementById("SODate");
var Salesman = document.getElementById("Salesman");
var RefDRNum = document.getElementById("RefDRNum");
var RefICRNum = document.getElementById("RefICRNum");
var itemDesc = document.getElementById("ItemDesc");
var PreparedBy = document.getElementById("PreparedBy");
var ReceivedBy = document.getElementById("ReceivedBy");
var DeliveredBy = document.getElementById("DeliveredBy");
var DriversLicense = document.getElementById("DriversLicense");
var truckPlate = document.getElementById("TruckPlate");
var invoiceFormat = document.getElementById("invoiceFormat");

//variables
var table = '';
var DRCustAddress = '';
var DRTinNum = '';
var DRdate = '';
var DRPONum = '';
var DRterms = '';
var DRPRNo = '';
var DRSalesman = '';
var DRRefDRNum = '';
var DRPreparedBy = '';
var DRReceivedBy = '';
var DRDeliveredBy = '';
var DRDriversLicense = '';
var DRTruckPlate = '';
var DRSONum = '';
var DRSODate = '';
var unitPrice = '';

//get date today
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1;
var yyyy = today.getFullYear();
var hr = today.getHours();
var min = today.getMinutes();
var sec = today.getSeconds();

if(dd<10)
{
  dd='0'+dd;
}

if(mm<10)
{
  mm='0'+mm;
}
var datetoday = mm+'-'+dd+'-'+yyyy+" "+hr+":"+min+":"+sec;

const getICRInfo = new Promise((resolve, reject) => {
  var ICRInfo = {};
  ICRRef.once("value", (data) => {
    data.forEach((data2) => {
      if (data2.key == ICRNum){
        ICRInfo.ID = data2.key;
        ICRInfo.custName = data2.val().custName;
        ICRInfo.custCode = data2.val().custCode;
        ICRInfo.date = data2.val().date;
        ICRInfo.custAddress = data2.val().custAddress;
      }
    });
    resolve(ICRInfo);
  })
});

const getICRItems = new Promise((resolve, reject) => {
  var ICRItems = [];
  ICRRef.child(ICRNum).child("Cylinders").child("Others").once("value", data => {
    data.forEach(data2 => {
      ICRItems.push([data2.key, data2.val().itemPrice]);
      PDFDispatchList.push([data2.key, data2.val().itemPrice]);
    });
    resolve(ICRItems);
  })
})

var ICRInfo = {};
var ICRItems = [];
async function retrieveDataAW(){
  ICRInfo = await getICRInfo;
  ICRItems = await getICRItems;
  $("#InvoiceNum").focus();
  table = $('#DRTable').DataTable( {
      data: ICRItems,
      select: true,
      columns: [
        { title: "Cylinder ID." },
        { title: "Price"}
      ]
  } );
  customername.value = ICRInfo.custName;
  custaddress.value = ICRInfo.custAddress;
  date.value = ICRInfo.date;
  document.querySelector(".loading").style.display = "none";
}
retrieveDataAW();

var format = invoiceFormat.options[invoiceFormat.selectedIndex].value;
invoiceFormat.addEventListener("click", (event) => {
  format = invoiceFormat.options[invoiceFormat.selectedIndex].value;
});
document.getElementById("createSalesInvoiceBtn").addEventListener("click", (event) => {
  event.preventDefault();
  if (confirm("You are about to create a Sales Invoice. Would you like to continue?")){
    document.querySelector(".loading2").style.display = "block";
    validate();
  } else {
    // do something.
  }
});

//cancel button
document.getElementById("cancelSalesInvoiceBtn").addEventListener("click", (event) => {
  event.preventDefault();
  if (confirm("You are about to discard this session. Would you like to continue?")){
    window.location = "createSalesInvoice.html";
  } else {
    // do something.
  }
});

function validate(){
  var inputs = document.getElementsByTagName("input");
  for (var i = 0; i < 20; i++){
    if (i === 14){
      continue;
    } else {
      if (inputs[i].value == ""){
        if (i == 0){
          alert("Invoice number cannot be empty");
          break;
        }
        else{
          inputs[i].value = "--";
        }

      }
    }
    if (i === 19) {
      createSI();
    }
  }
  document.querySelector(".loading2").style.display = "none";
}



function createSI(){
  document.querySelector(".loading2").style.display = "block";
  var checker = [false, false];
  var itemsProcessed = [0,0];
  
  SIRef.child(invNum.value).set({
    CustomerName: customername.value,
    Address: custaddress.value,
    TinBuyer: tinNum.value,
    BusinessStyle: busType.value,
    Date: date.value,
    PONum: PONum.value,
    PGCSONo: PGCSONo.value,
    Terms: terms.value,
    PRNum: PRNo.value,
    SODate: SODate.value,
    Salesman: Salesman.value,
    RefDRNum: RefDRNum.value,
    RefICRNum: RefICRNum.value,
    PreparedBy: PreparedBy.value,
    ReceivedBy: ReceivedBy.value,
    DeliveredBy: DeliveredBy.value,
    DriversLicense: DriversLicense.value,
    TruckPlate: truckPlate.value,
    itemDesc: itemDesc.options[itemDesc.selectedIndex].value,
    Format: format,
  })
  .then(() => {
    checker[0] = true;
    checkerFunc(checker);
  });
  ICRItems.forEach((data) => {
    SIRef.child(invNum.value).child("items").child(data[0]).set({
      CylinderID: data[0],
      CylinderPrice: data[1]
    })
    .then(() => {
      itemsProcessed[0]++;
      if (itemsProcessed[0] === ICRItems.length){
        checker[1] = true;
        checkerFunc(checker);
      }
    });
  });
}


function checkerFunc(checker){
  if (checker){
    window.location = "createSalesInvoice.html";
  }
}
