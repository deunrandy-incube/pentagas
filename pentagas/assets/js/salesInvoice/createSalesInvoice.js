//for PDF valids
var SIList = [];
var forViewing = "true";

var table = '';
var table2 = '';
var table3 = '';
var table4 = '';
var selectedDRIndex = [];
var selectedDR = [];
var selectedDispatch = [];
var selectedSO = [];
var DRCustName = '';
var DispatchList = [];
var DirectInvList = [];
var BigLedgerList = [];
var SmallLedgerList = [];
var itemsLedger = [];
var siDataPDF = [];
mainSIArr = [];
//references
var databaseRef = firebase.database().ref();
var DRRef = databaseRef.child("Delivery Receipt");
var SIRef = databaseRef.child("Sales Invoice");
var DispatchRef = databaseRef.child("Dispatch Slip");
var BigLedgerRef = databaseRef.child("Big Ledger");
var SmallLedgerRef = databaseRef.child("Small Ledger");
var productRef = databaseRef.child("Inventory");

var pageLoad = 20;

var siSearchInput = document.getElementById("siSearchInput");
siSearchInput.addEventListener('input', function (evt) {
  if (this.value == '') {
      stateTable(mainSIArr);
  }
});

//get date today
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1;
var yyyy = today.getFullYear();
var hr = today.getHours();
var min = today.getMinutes();
var sec = today.getSeconds();
var SOInfo;

if(dd<10)
{
  dd='0'+dd;
}

if(mm<10)
{
  mm='0'+mm;
}
var datetoday = mm + '/' + dd + '/' + yyyy + " " + hr + ":" + min + ":" + sec + " AM";


function getSmallLedger() {
  return new Promise((resolve, reject) => {
  for(var x =0; x< itemsLedger.length; x++){
    let cylinderLedgerId = itemsLedger[x][0];
    SmallLedgerRef.child(cylinderLedgerId).once("value", (data) => {
      data.forEach((data2) => {
        data2.forEach((data3) => {
          if(data3.val().invoiceNum == SalesInvoiceID){
            SmallLedgerList.push([cylinderLedgerId, SalesInvoiceID, data3.val(), data2.val()]);
          }
        });
      });
    });
  }
  resolve(SmallLedgerList);
});
}

function getBigLedger() {
  var itemsProcessedLedger = 0;
  return new Promise((resolve, reject) => {
    for(var x =0; x< itemsLedger.length; x++){
      let cylinderLedgerId = itemsLedger[x][0];
      BigLedgerRef.child(custId).child('items').child(cylinderLedgerId).child('Cylinder Record').child(SalesInvoiceID).once("value", (data) => {
        data.forEach((data2) => {
          BigLedgerList.push([cylinderLedgerId, SalesInvoiceID, data2.key, data2.val()]);
          itemsProcessedLedger++;
          if (itemsProcessedLedger === itemsList.length) {
            resolve(BigLedgerList);
          }
        });
      });
    }
  });
} 

function getDRList(){
  return new Promise((resolve, reject) => {
    var DRList = [];
    DRRef.orderByChild("status").equalTo("Delivered").once("value", (data) => {
      data.forEach((data2) => {
        let drDateSplitter = data2.val().Date.split(" ");
        DRList.push([data2.key, data2.val().custName, drDateSplitter[0], data2.val().SONum]);
      });
      resolve(DRList);
    });
  });
} 
  
function getDispatchList(){
  return new Promise((resolve, reject) => {
    DispatchRef.orderByChild("status").equalTo("Dispatched").once("value", (data) => {
      data.forEach((data2) => {
        DispatchList.push([data2.key, data2.val().custName, data2.val().cylinderType, data2.val().SONumber]);
      });
      resolve(DispatchList);
    })
  })
} 

function getSIList() {
  return new Promise((resolve, reject) => {
    SIList = [];
    SIRef.limitToLast(pageLoad).once("value", (data) => {
      data.forEach((data2) => {
        let siDateSplitter = data2.val().Date.split(" ");
        SIList.push([data2.key, data2.val().CustomerName, siDateSplitter[0], data2.val().PONum]);
      });
      resolve(SIList);
    });
  });
} 

//table assignment
async function retrieveAllSIData(){
  document.querySelector(".loading").style.display = "block";
  const SIList = await getSIList(); 
  mainSIArr = SIList;
  stateTable(SIList)
}

function retrieveSpecificSI() {
  if (siSearchInput.value) {
    document.querySelector(".loading").style.display = "block";
    pageLoad = 20;
    SIRef.child(siSearchInput.value).once('value', function (data2) {
      var exists = (data2.val() !== null);
      if (exists) {
        let specSIArr = [];
        let siDateSplitter = data2.val().Date.split(" ");
        specSIArr.push([data2.key, data2.val().CustomerName, siDateSplitter[0], data2.val().PONum]);
        stateTable(specSIArr);
      } else {
        document.querySelector(".loading").style.display = "none";
        window.alert("Sales Invoice Not Found.");
      }
    });
  } else {
    window.alert("Input is empty, please try again.");
  }
}

function stateTable(tablearr){
  var content = '';
  var paginate = false;
  paginate = tablearr.length > 1 ? true : false;

  if(table){
    table.destroy();
  }

  tablearr.forEach((value) => {
    content +='<tr id="' + "++" + value[0] + '">';
    content += '<td>' + value[0] + '</td>';
    content += '<td>' + value[1] + '</td>';
    content += '<td>' + value[2] + '</td>';
    content += '<td>' + value[3] + '</td>';
    content += '</tr>';
  });
  $('#INVTable').append(content);

  table = $('#INVTable').DataTable({
    "aaSorting": [],
    "bPaginate": paginate,
    drawCallback: function(){
      $('.paginate_button.next', this.api().table().container())          
         .on('click', async function(){
            document.querySelector(".loading").style.display = "block";
            pageLoad += 20;
            table.clear();
            SIList = await getSIList();
            mainSIArr = SIList;
            stateTable(SIList)
      });       
   }
  });
  document.querySelector(".loading").style.display = "none";
}

var SalesInvoiceID = '';
$('#INVTable tbody').on('click', 'td', (e) => {
  var selectedRow = e.target.parentNode;
  var selectedRow = e.target.parentNode;
  SalesInvoiceID = selectedRow.children[0].innerText
  if (SalesInvoiceID != "No data available in table"){
    document.querySelector(".loading2").style.display = "block";
    getInvoiceData = new Promise((resolve, reject) => {
      invoiceData = [];
      SIRef.child(SalesInvoiceID).once("value", (data2) => {
        let siDateSplitter = data2.val().Date.split(" ");
        let soDateSplitter = data2.val().SODate.split(" ");
        
        invoiceData.push(data2.key, data2.val().CustomerName, siDateSplitter[0], data2.val().PONum,
                   data2.val().Address, data2.val().TinBuyer, data2.val().BusinessStyle,
                   data2.val().PGCSONo, data2.val().Terms, data2.val().PRNum,
                   soDateSplitter[0], data2.val().Salesman, data2.val().RefDRNum, data2.val().RefICRNum,
                   data2.val().PreparedBy, data2.val().ReceivedBy, data2.val().DeliveredBy,
                   data2.val().DriversLicense, data2.val().TruckPlate, data2.val().Format, data2.val().itemDesc,
                   data2.val().unitPrice, data2.val().Date, data2.val().CustomerID, data2.val().directInvoice, data2.val().SONum);
        resolve(invoiceData);
      });
    });

    getItemList = new Promise((resolve, reject) => {
      itemsList = [];
      SIRef.child(SalesInvoiceID).child("items").orderByChild("sorterkey").once("value", (data) => {
        data.forEach((data2) => {
          if(data2.val().CylinderID){
            itemsList.push([data2.key, data2.val().CylinderType, data2.val().CylinderPrice]);
          }
        });
        resolve(itemsList);
      });
    });

    getListDR = new Promise((resolve, reject) => {
      drList = [];
      let unitPriceDr = 0;
      SIRef.child(SalesInvoiceID).once("value", (data2) => {
        unitPriceDr = data2.val().unitPrice;
        SIRef.child(SalesInvoiceID).child("items").orderByChild("sorterkey").once("value", (data) => {
          data.forEach((data2) => {
            if(data2.val().DRNum){
              drList.push([data2.val().DRNum, data2.val().DRDate, data2.val().DRQty, unitPriceDr]);
            }
          });
          resolve(drList);
        });
      });
      
    });
     retrieveSIInformation();
   }
});


var custName = '';
var oldDate = '';
var custId = '';
var directInvoiceStat;
var salesOrderId = '';

async function editDate(){
  var checker = [false, false];
  var itemsProcessed = [0, 0];

  const SmallLedgerList = await getSmallLedger();
  const BigLedgerList = await getBigLedger();

  document.querySelector(".loading2").style.display = "block";
  SIRef.child(SalesInvoiceID).update({
    Date: document.getElementById("modalDelDate").value,
  }).then((e) => {
    if(directInvoiceStat == true){
      if(document.getElementById("modalDelDate").value != oldDate){
        let smdatesplitter = (document.getElementById("modalDelDate").value).split(" ");
        let newdatesplit;
          if(smdatesplitter[0].indexOf("/") != -1){
            newdatesplit = smdatesplitter[0].split("/");
          }else if(smdatesplitter[0].indexOf("-") != -1){
            newdatesplit = smdatesplitter[0].split("-");
          }
        let newdate = newdatesplit[0]+"-"+newdatesplit[1]+"-"+newdatesplit[2];
  
        let oldsmdatesplitter = (oldDate).split(" ");
        let olddatesplit;
          if(oldsmdatesplitter[0].indexOf("/") != -1){
            olddatesplit = oldsmdatesplitter[0].split("/");
          }else if(oldsmdatesplitter[0].indexOf("-") != -1){
            olddatesplit = oldsmdatesplitter[0].split("-");
          }
        let olddateupdated = olddatesplit[0]+"-"+olddatesplit[1]+"-"+olddatesplit[2];
  
        for(var w=0; w < itemsList.length; w++){
          for(var x=0; x< SmallLedgerList.length; x++){
            if(itemsList[w][0] == SmallLedgerList[x][0]){
              let outsmdatesplitter = (SmallLedgerList[x][2].out).split(" ");
              let outdatesplit;
                if(outsmdatesplitter[0].indexOf("/") != -1){
                  outdatesplit = outsmdatesplitter[0].split("/");
                }else if(outsmdatesplitter[0].indexOf("-") != -1){
                  outdatesplit = outsmdatesplitter[0].split("-");
                }
              let outdateupdated = outdatesplit[0]+"-"+outdatesplit[1]+"-"+outdatesplit[2];
              
              if(olddateupdated+" "+oldsmdatesplitter[1] == outdateupdated+" "+outsmdatesplitter[1]){
                SmallLedgerRef.child(SmallLedgerList[x][0]).update({
                  lastUpdate: datetoday,
                  lastOut: document.getElementById("modalDelDate").value,
                })
                SmallLedgerRef.child(SmallLedgerList[x][0]).child("CylinderRecord").child(newdate+" "+smdatesplitter[1]).set({
                  out: document.getElementById("modalDelDate").value,
                  customer: SmallLedgerList[x][2].customer,
                  customerID: SmallLedgerList[x][2].customerID,
                  custCode: SmallLedgerList[x][2].custCode,
                  in: SmallLedgerList[x][2].in,
                  invoiceNum: SmallLedgerList[x][2].invoiceNum,
                  drNumber: SmallLedgerList[x][2].drNumber == undefined ? "" :  SmallLedgerList[x][2].drNumber 
                })  
                SmallLedgerRef.child(SmallLedgerList[x][0]).child('CylinderRecord').child(olddateupdated+" "+oldsmdatesplitter[1]).remove()
                .then(() => {
                  itemsProcessed[0]++;       
                    if (itemsProcessed[0] === itemsList.length) {
                      checker[0] = true;
                      checkerFunc(checker, "edit");
                    }
                })
              }
            }
          }
  
          for(var y=0; y < BigLedgerList.length; y++){
            if(itemsList[w][0] == BigLedgerList[y][0]){
  
              let outsmdatesplitter = (BigLedgerList[y][3].out).split(" ");
              let outdatesplit;
                if(outsmdatesplitter[0].indexOf("/") != -1){
                  outdatesplit = outsmdatesplitter[0].split("/");
                }else if(outsmdatesplitter[0].indexOf("-") != -1){
                  outdatesplit = outsmdatesplitter[0].split("-");
                }
              let outdateupdated = outdatesplit[0]+"-"+outdatesplit[1]+"-"+outdatesplit[2];
              
              if(olddateupdated+" "+oldsmdatesplitter[1] == outdateupdated+" "+outsmdatesplitter[1]){
                BigLedgerRef.child(custId).update({
                  customerName: custName,
                  lastUpdate: datetoday
                })
                BigLedgerRef.child(custId).child("items").child(BigLedgerList[y][0]).child("Cylinder Record").child(SalesInvoiceID).child(newdate+" "+smdatesplitter[1]).set({
                  drNum: BigLedgerList[y][3].drNum == undefined ? "" :  BigLedgerList[y][3].drNum ,
                  invNum: SalesInvoiceID,
                  directInvoice: BigLedgerList[y][3].directInvoice,
                  cylinderType: BigLedgerList[y][3].cylinderType,
                  out: document.getElementById("modalDelDate").value,
                  in: BigLedgerList[y][3].in
                })
                BigLedgerRef.child(custId).child('items').child(BigLedgerList[y][0]).child('Cylinder Record').child(SalesInvoiceID).child(olddateupdated+" "+oldsmdatesplitter[1]).remove()
                .then(() => {
                  itemsProcessed[1]++;        
                    if (itemsProcessed[1] === itemsList.length) {
                      checker[1] = true;
                      checkerFunc(checker, "edit");
                    }
                })
              }
            }
          }
        }
      }else{
        document.querySelector(".loading2").style.display = "none";
      }
    }else{
      window.location = "createSalesInvoice.html";
    }
  });
}

async function deleteSI(){
  if (confirm("You are about to delete this Sales Invoice. Would you like to continue?")) {
    var deletechecker = [false, false];
    var deleteitemsProcessed = [0, 0];
    
    if(directInvoiceStat == true){
      if(document.getElementById("modalDelDate").value == oldDate){
  
        const SmallLedgerList = await getSmallLedger();
        const BigLedgerList = await getBigLedger();
        
        document.querySelector(".loading2").style.display = "block";
        let smdatesplitter = (document.getElementById("modalDelDate").value).split(" ");
        let newdatesplit;
          if(smdatesplitter[0].indexOf("/") != -1){
            newdatesplit = smdatesplitter[0].split("/");
          }else if(smdatesplitter[0].indexOf("-") != -1){
            newdatesplit = smdatesplitter[0].split("-");
          }
        let newdate = newdatesplit[0]+"-"+newdatesplit[1]+"-"+newdatesplit[2];
  
        let oldsmdatesplitter = (oldDate).split(" ");
        let olddatesplit;
          if(oldsmdatesplitter[0].indexOf("/") != -1){
            olddatesplit = oldsmdatesplitter[0].split("/");
          }else if(oldsmdatesplitter[0].indexOf("-") != -1){
            olddatesplit = oldsmdatesplitter[0].split("-");
          }
        let olddateupdated = olddatesplit[0]+"-"+olddatesplit[1]+"-"+olddatesplit[2];
  
        for(var w=0; w < itemsList.length; w++){
          for(var x=0; x< SmallLedgerList.length; x++){
            if(itemsList[w][0] == SmallLedgerList[x][0]){
              let outsmdatesplitter = (SmallLedgerList[x][2].out).split(" ");
              let outdatesplit;
                if(outsmdatesplitter[0].indexOf("/") != -1){
                  outdatesplit = outsmdatesplitter[0].split("/");
                }else if(outsmdatesplitter[0].indexOf("-") != -1){
                  outdatesplit = outsmdatesplitter[0].split("-");
                }
              let outdateupdated = outdatesplit[0]+"-"+outdatesplit[1]+"-"+outdatesplit[2];
              
              if(olddateupdated+" "+oldsmdatesplitter[1] == outdateupdated+" "+outsmdatesplitter[1]){
                if(Object.keys(SmallLedgerList[x][3]).length==1){
                  SmallLedgerRef.child(SmallLedgerList[x][0]).remove().then(() =>{
                    deleteitemsProcessed[0]++;
                    if (deleteitemsProcessed[0] === itemsList.length) {
                      deletechecker[0] = true;
                      checkerFunc(deletechecker, "delete");
                    }
                  })
                }else{
                  SmallLedgerRef.child(SmallLedgerList[x][0]).update({
                    lastOut: "",
                    status: "Production"
                  })
                  SmallLedgerRef.child(SmallLedgerList[x][0]).child('CylinderRecord').child(olddateupdated + " " + oldsmdatesplitter[1]).remove()
                    .then(() => {
                      deleteitemsProcessed[0]++;
                      if (deleteitemsProcessed[0] === itemsList.length) {
                        deletechecker[0] = true;
                        checkerFunc(deletechecker, "delete");
                      }
                    })
                }
              }
            }
          }
  
          for(var y=0; y < BigLedgerList.length; y++){
            if(itemsList[w][0] == BigLedgerList[y][0]){
  
              let outsmdatesplitter = (BigLedgerList[y][3].out).split(" ");
              let outdatesplit;
                if(outsmdatesplitter[0].indexOf("/") != -1){
                  outdatesplit = outsmdatesplitter[0].split("/");
                }else if(outsmdatesplitter[0].indexOf("-") != -1){
                  outdatesplit = outsmdatesplitter[0].split("-");
                }
              let outdateupdated = outdatesplit[0]+"-"+outdatesplit[1]+"-"+outdatesplit[2];
              
              if(olddateupdated+" "+oldsmdatesplitter[1] == outdateupdated+" "+outsmdatesplitter[1]){
                BigLedgerRef.child(custId).child('items').child(BigLedgerList[y][0]).child('Cylinder Record').child(SalesInvoiceID).remove()
                .then(() => {
                  deleteitemsProcessed[1]++;
                  if (deleteitemsProcessed[1] === itemsList.length) {
                    deletechecker[1] = true;
                    checkerFunc(deletechecker, "delete");
                  }
                })
              }
            }
          }
        }
      }else{
        window.alert("You cannot delete this Invoice if you adjusted the Date. Please re-open Invoice window and use default date instead.")
      }
    }else{
      document.querySelector(".loading2").style.display = "block";
      var deliveryprocess = [0, 0];
      var deliverychecker = [false, false];
      drList.forEach((drdata) => {
        console.log(drdata[0])
        DRRef.child(drdata[0]).update({
          status: "Delivered"
        }).then(()=> {
          deliveryprocess[0]++;
          if (deliveryprocess[0] == drList.length){
            SIRef.child(SalesInvoiceID).remove().then(() => {
              deliverychecker[0] = true;
              checkerFunc(deliverychecker, "fromdr")
            })
          }
        })
        DRRef.child(drdata[0]).once("value", (data) => {
          var itemarray = Object.keys(data.val().items);
          for (const key of itemarray) {
            let oldsmdatesplitter = (data.val().Date).split(" ");
            let olddatesplit;
              if(oldsmdatesplitter[0].indexOf("/") != -1){
                olddatesplit = oldsmdatesplitter[0].split("/");
              }else if(oldsmdatesplitter[0].indexOf("-") != -1){
                olddatesplit = oldsmdatesplitter[0].split("-");
              }
            let olddateupdated = olddatesplit[0]+"-"+olddatesplit[1]+"-"+olddatesplit[2];
            console.log(olddateupdated+" "+oldsmdatesplitter[1]);
            BigLedgerRef.child(custId).child("items").child(key).child("Cylinder Record").child(drdata[0]).child(olddateupdated+" "+oldsmdatesplitter[1]).child("invNum").remove()
            .then(() => {
              deliveryprocess[1]++;
              if(deliveryprocess[1] == drList.length){
                deliverychecker[1] = true;
                checkerFunc(deliverychecker, "fromdr")
              }
            });      
          }
        });
      });
    }
  }
}

function checkerFunc(checker,from) {
  if (checker[0] && checker[1]) {
    if(from==="edit"){
      window.location = "createSalesInvoice.html";
    }else if(from==="delete"){
      removeSIfromDB();
    }else if(from==="fromdr"){
      window.alert("Invoice deleted. Please notify Web Admin of the SO ID ("+salesOrderId+"), the Invoice ID ("+ SalesInvoiceID+")");
      window.location = "createSalesInvoice.html";
    }
  }
}

function removeSIfromDB(){
  var inventoryprocess = 0;
  var inventorychecker = false;
  SIRef.child(SalesInvoiceID).remove().then(() => {
    itemsList.forEach((data2) => {
      productRef.child(data2[0]).update({
        cylinderstatus : "Production"
      })
      .then((e) => {
        inventoryprocess++;
        if (inventoryprocess === itemsList.length){
          inventorychecker = true;
          inventoryCheckerFunc(inventorychecker);
        }
      });
    });
  });
}

function inventoryCheckerFunc(checker){
  if(checker){
    BigLedgerRef.child(custId).child('items').once("value").then(function(snapshot){
      if(snapshot.exists()){
        window.alert("Invoice deleted. Please notify Web Admin of the SO ID ("+salesOrderId+"), Invoice cylinder count ("+itemsList.length+"), the Invoice ID ("+ SalesInvoiceID +") and Dispatch ID to update status of Dispatcher Slip back to 'Dispatched' and to update delivery count in the Sales Order")
        window.location = "createSalesInvoice.html";
      }else{
        BigLedgerRef.child(custId).remove().then(() => {
          window.alert("Invoice deleted. Please notify Web Admin of the SO ID ("+salesOrderId+"), Invoice cylinder count ("+itemsList.length+"), the Invoice ID ("+ SalesInvoiceID +") and Dispatch ID to update status of Dispatcher Slip back to 'Dispatched' and to update delivery count in the Sales Order")
          window.location = "createSalesInvoice.html";
        })
      }
    });
  }
}

async function retrieveSIInformation(){
  document.querySelector(".modal-title").textContent = SalesInvoiceID;
  const invoiceData = await getInvoiceData;
  const itemsList = await getItemList;
  const drList = await getListDR;
  siDataPDF = invoiceData;
  itemsLedger = itemsList;

  custName = invoiceData[1];
  oldDate = invoiceData[22];
  custId = invoiceData[23];
  directInvoiceStat = invoiceData[24];
  salesOrderId = invoiceData[25];

  document.getElementById("modalCustName").textContent = invoiceData[1];
  document.getElementById("modalDelDate").value = invoiceData[22];
  document.getElementById("modalPONumber").textContent = invoiceData[3];
  document.getElementById("modalCustAddress").textContent = invoiceData[4];
  document.getElementById("modalTin").textContent = invoiceData[5];
  document.getElementById("modalBuisnessStyle").textContent = invoiceData[6];
  document.getElementById("modalPGCSONumber").textContent = invoiceData[7];
  document.getElementById("modalTerms").textContent = invoiceData[8];
  document.getElementById("modalPRNumber").textContent = invoiceData[9];
  document.getElementById("modalSODate").textContent = invoiceData[10];
  document.getElementById("modalSalesman").textContent = invoiceData[11];
  document.getElementById("modalRefDRNumber").textContent = invoiceData[12];
  document.getElementById("modalRefICRNumber").textContent = invoiceData[13];
  document.getElementById("modalPreparedBy").textContent = invoiceData[14];
  document.getElementById("modalRecievedBy").textContent = invoiceData[15];
  document.getElementById("modalDeliveredBy").textContent = invoiceData[16];
  document.getElementById("modalDriversLicense").textContent = invoiceData[17];
  document.getElementById("modalTruckPlate").textContent = invoiceData[18];
  document.getElementById("modalFormat").textContent = invoiceData[19];

  table1 = $('#DRTable2').DataTable( {
    data: drList,
    select: true,
    columns: [
      { title: "D.R. No." },
      { title: "D.R. Date" },
      { title: "DR Qty"}
    ]
  } );
  table4 = $('#itemsTable').DataTable( {
      "ordering": false,
      data: itemsList,
      select: true,
      columns: [
        { title: "Cylinder ID" },
        { title: "Quantity Type" }
      ]
  } );
  document.querySelector(".loading2").style.display = "none";
  $('#DispModal').modal({
    backdrop: 'static',
    keyboard: false
  });
  $('#DispModal').modal("show");
}

async function openModal(){
  document.querySelector(".loading2").style.display = "block";
  const DRList = await getDRList();
  const DispatchList = await getDispatchList();

  if(table2){
    table2.destroy();
  }
  table2 = $('#DRTable').DataTable( {
    data: DRList,
    select: true,
    columns: [
      { title: "D.R. No." },
      { title: "Customer Name" },
      { title: "Delivery Date"},
      { title: "S.O. No."}
    ]
  } );
  if(table3){
    table3.destroy();
  }
  table3 = $('#DispatchTable').DataTable( {
    data: DispatchList,
    select: true,
    columns: [
      { title: "Dispatch ID" },
      { title: "Customer Name" },
      { title: "Cylinder Type"},
      { title: "S.O. No." }
    ]
  } );
  document.querySelector(".loading2").style.display = "none";
  $('#myModal').modal({
    backdrop: 'static',
    keyboard: false
  });
  $('#myModal').modal("show");
}

//for multiple row selection
$('#DRTable tbody').on( 'click', 'tr', function () {
    $(this).toggleClass('selected');
} );
$('#DispatchTable tbody').on( 'click', 'tr', function () {
    $(this).toggleClass('selected');
} );

function CreateSI(){
  let custtemparr = [];
  $('#DRTable tbody')[0].childNodes.forEach((data) => {
    if(data.classList.contains("selected")){
      selectedDR.push(data.childNodes[0].innerText);
      selectedSO.push(data.childNodes[3].innerText);
      DRCustName = data.childNodes[1].innerText;
      custtemparr.push(DRCustName);
    }
  });
  if (selectedSO.every(function(item){
    return item === selectedSO[0];
  })){
    localStorage.setItem("selectedDR", JSON.stringify(selectedDR));
    localStorage.setItem("selectedDispatch", JSON.stringify(selectedDispatch));
    localStorage.setItem("CustomerName", DRCustName);
    window.location = "createSalesInvoice2.html";
  } else {
    let isSanJuan = true;
    for(var x=0; x<custtemparr.length; x++){
      if(custtemparr[x] != "SAN JUAN MEDICAL CENTER"){
        isSanJuan = false;
      }
    }

    if(isSanJuan){
      localStorage.setItem("selectedDR", JSON.stringify(selectedDR));
      localStorage.setItem("selectedDispatch", JSON.stringify(selectedDispatch));
      localStorage.setItem("CustomerName", DRCustName);
      window.location = "createSalesInvoice2.html";
    }else{
      selectedDR = [];
      selectedSO = [];
      alert("Please select DR with same SO. Different SO Invoice is only applicable to SAN JUAN MEDICAL CENTER");
    }
    
  }
}

function CreateSI2(){
  $('#DispatchTable tbody')[0].childNodes.forEach((data) => {
    if(data.classList.contains("selected")){
      selectedDispatch.push(data.childNodes[0].innerText);
      selectedSO.push(data.childNodes[3].innerText);
      DRCustName = data.childNodes[1].innerText;

    }
  });
  if (selectedSO.every(function(item){
    return item === selectedSO[0];
  })){
    localStorage.setItem("selectedDispatch", JSON.stringify(selectedDispatch));
    localStorage.setItem("selectedDR", JSON.stringify(selectedDR));
    localStorage.setItem("CustomerName", DRCustName);
    window.location = "createSalesInvoice3.html";
  } else {
    selectedDR = [];
    selectedSO = [];
    alert("Please select DR with same SO");
  }
}

function clearTables() {
  table1.destroy();
  table4.destroy();
}