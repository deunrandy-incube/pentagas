var databaseRef = firebase.database().ref();
var salesOrderRef = databaseRef.child("Sales Order");
var dispatchRef = databaseRef.child("Dispatch Slip");
var icrRef = databaseRef.child("Incoming Cylinder Report");
var invRef = databaseRef.child("Inventory");

//variables
var table = '';
var table2 = '';
var table3 = '';

//pdf inventory Variables
    //med o2 inv variables
    var medfullstocks = 0;
    var meddefstocks = 0;
    var medemptystocks = 0;
    var medaddlempty = 0;
    var medaddlfull = 0;
    //comp air inv variables
    var cairfullstocks = 0;
    var cairdefstocks = 0;
    var cairemptystocks = 0;
    var cairaddlempty = 0;
    var cairaddlfull = 0;
    //n20 inv variables
    var n20fullstocks = 0;
    var n20defstocks = 0;
    var n20emptystocks = 0;
    var n20addlempty = 0;
    var n20addlfull = 0;
    //t02 inv variables
    var t02fullstocks = 0;
    var t02defstocks = 0;
    var t02emptystocks = 0;
    var t02addlempty = 0;
    var t02addlfull = 0;
    //acetylene inv variables
    var acetfullstocks = 0;
    var acetdefstocks = 0;
    var acetemptystocks = 0;
    var acetaddlempty = 0;
    var acetaddlfull = 0;
    //carbon dioxide inv variables
    var c02fullstocks = 0;
    var c02defstocks = 0;
    var c02emptystocks = 0;
    var c02addlempty = 0;
    var c02addlfull = 0;
    //nitrogen inv variables
    var n2fullstocks = 0;
    var n2defstocks = 0;
    var n2emptystocks = 0;
    var n2addlempty = 0;
    var n2addlfull = 0;
    //argon inv variables
    var argfullstocks = 0;
    var argdefstocks = 0;
    var argemptystocks = 0;
    var argaddlempty = 0;
    var argaddlfull = 0;

//PDF Delivery & Pick Up Variables
    var ordmed02 = 0;
    var ordca = 0;
    var ordn20 = 0;
    var ordt02 = 0;
    var ordc2h2 = 0;
    var ordn2 = 0;
    var ordarg = 0;
    var ordc02 = 0;

    var delmed02 = 0;
    var delca = 0;
    var deln20 = 0;
    var delt02 = 0;
    var delc2h2 = 0;
    var deln2 = 0;
    var delarg = 0;
    var delc02 = 0;

    var orddelbal = 0;

var sampdata = "tofollow";
var stockstable = [];
var delpickuptable = [];
var delpickuptableicr = [];
var dealerstable = [];

document.querySelector(".loading").style.display = "block";

const getInventory = new Promise((resolve, reject) => {
invRef.once("value", (data) => {
  data.forEach((data2) => {
      //med o2 stocks
      if(data2.val().cylinderstatus == "Production" && data2.val().cylindertype == "MED 02"){
        medfullstocks++;
      }
      else if(data2.val().cylinderstatus == "Defective" && data2.val().cylindertype == "MED 02"){
        meddefstocks++;
      }
      else if(data2.val().cylinderstatus == "Empty" && data2.val().cylindertype == "MED 02"){
        medemptystocks++;
      }

      //comp air stocks
      if(data2.val().cylinderstatus == "Production" && data2.val().cylindertype == "COMP AIR"){
        cairfullstocks++;
      }
      else if(data2.val().cylinderstatus == "Defective" && data2.val().cylindertype == "COMP AIR"){
        cairdefstocks++;
      }
      else if(data2.val().cylinderstatus == "Empty" && data2.val().cylindertype == "COMP AIR"){
        cairemptystocks++;
      }

      //nitrous oxide stocks
      if(data2.val().cylinderstatus == "Production" && data2.val().cylindertype == "NITROUS DIOXIDE"){
        n20fullstocks++;
      }
      else if(data2.val().cylinderstatus == "Defective" && data2.val().cylindertype == "NITROUS DIOXIDE"){
        n20defstocks++;
      }
      else if(data2.val().cylinderstatus == "Empty" && data2.val().cylindertype == "NITROUS DIOXIDE"){
        n20emptystocks++;
      }

      //tech 02 stocks
      if(data2.val().cylinderstatus == "Production" && data2.val().cylindertype == "TECH 02"){
        t02fullstocks++;
      }
      else if(data2.val().cylinderstatus == "Defective" && data2.val().cylindertype == "TECH 02"){
        t02defstocks++;
      }
      else if(data2.val().cylinderstatus == "Empty" && data2.val().cylindertype == "TECH 02"){
        t02emptystocks++;
      }

      //acetylene stocks
      if(data2.val().cylinderstatus == "Production" && data2.val().cylindertype == "ACETYLENE"){
        acetfullstocks++;
      }
      else if(data2.val().cylinderstatus == "Defective" && data2.val().cylindertype == "ACETYLENE"){
        acetdefstocks++;
      }
      else if(data2.val().cylinderstatus == "Empty" && data2.val().cylindertype == "ACETYLENE"){
        acetemptystocks++;
      }

      //carbon dioxide stocks
      if(data2.val().cylinderstatus == "Production" && data2.val().cylindertype == "CARBON DIOXIDE"){
        c02fullstocks++;
      }
      else if(data2.val().cylinderstatus == "Defective" && data2.val().cylindertype == "CARBON DIOXIDE"){
        c02defstocks++;
      }
      else if(data2.val().cylinderstatus == "Empty" && data2.val().cylindertype == "CARBON DIOXIDE"){
        c02emptystocks++;
      }

      //nitrogen stocks
      if(data2.val().cylinderstatus == "Production" && data2.val().cylindertype == "NITROGEN"){
        n2fullstocks++;
      }
      else if(data2.val().cylinderstatus == "Defective" && data2.val().cylindertype == "NITROGEN"){
        n2defstocks++;
      }
      else if(data2.val().cylinderstatus == "Empty" && data2.val().cylindertype == "NITROGEN"){
        n2emptystocks++;
      }

      //argon stocks
      if(data2.val().cylinderstatus == "Production" && data2.val().cylindertype == "ARGON"){
        argfullstocks++;
      }
      else if(data2.val().cylinderstatus == "Defective" && data2.val().cylindertype == "ARGON"){
        argdefstocks++;
      }
      else if(data2.val().cylinderstatus == "Empty" && data2.val().cylindertype == "ARGON"){
        argemptystocks++;
      }
    });
    resolve(argemptystocks);
});
})

const getSO = new Promise((resolve, reject) => {
  var SalesOrders = [];
  var today = new Date();
  var dd = today.getDate();
  var mm = today.getMonth()+1;
  var yyyy = today.getFullYear();
  if(dd<10){
    dd='0'+dd;
  }

  if(mm<10){
    mm='0'+mm;
  }
  today = mm+'/'+dd+'/'+yyyy;

  salesOrderRef.once("value", (data) => {
    data.forEach((data2) => {
      var deldate = data2.val().delDate.split(" ");
      SalesOrders.push([data2.key, data2.val().customerName, data2.val().createDate, data2.val().PONum, data2.val().status]);
    });
    resolve(SalesOrders);
  });
})
async function retrieveCustAW(){
  //for customer name
  const SalesOrders = await getSO;
  table = $('#DelandPickup').DataTable( {
      data: SalesOrders,
      select: true,
      columns: [
        { title: "Sales Order No." },
        { title: "Customer Name" },
        { title: "Date" },
        { title: "Purchase Order No." },
        { title: "Status" }
      ]
  } );
  //to hide loading screen
  document.querySelector(".loading").style.display = "none ";
}
//async function call
retrieveCustAW();

var SOID = '';
var DateofCreation = '';
$('#DelandPickup tbody').on('click', 'td', (e) => {
  var selectedRow = e.target.parentNode;
  SOID = selectedRow.children[0].innerText;
  if (SOID != "No data available in table"){
    document.querySelector(".loading2").style.display = "block";
    DateofCreation = selectedRow.children[2].innerText;
    
    getSOItems = new Promise((resolve, reject) => {
      var SalesOrders = [];
      var items = [];

      salesOrderRef.child(SOID).child("items").once("value", (data) => {
        data.forEach((data2) => {
          items.push([data2.key, data2.val().itemPrice, data2.val().itemQty]);
        });
        resolve(items);
      });
    })

    getDeliveryList = new Promise((resolve, reject) => {
      var deliveries = [];
      salesOrderRef.child(SOID).child("items").once("value", (data) => {
        data.forEach((data2) => {
          deliveries.push([data2.key, data2.val().itemDelivered, data2.val().latestDeliveryDate]);
        });
        resolve(deliveries);
      });
    });

      getSOItemsforDate = new Promise((resolve, reject) => {
        salesOrderRef.once("value", (data) => {
          data.forEach((data2) => {
            data2.forEach((data3) => {
              data3.forEach((data4) => {
                if(data2.val().createDate == DateofCreation){
                  ordmed02 = 0;
                  ordca = 0;
                  ordn20 = 0;
                  ordt02 = 0;
                  ordc2h2 = 0;
                  ordn2 = 0;
                  ordarg = 0;
                  ordc02 = 0;

                  delmed02 = 0;
                  delca = 0;
                  deln20 = 0;
                  delt02 = 0;
                  delc2h2 = 0;
                  deln2 = 0;
                  delarg = 0;
                  delc02 = 0;
                  orddelbal = 0;

                  if(data4.key == "MED 02"){
                    ordmed02 = data4.val().itemQty;
                    delmed02 = data4.val().itemDelivered;
                    orddelbal += Number(data4.val().itemQty) - Number(data4.val().itemDelivered);
                  }
                  else if(data4.key == "COMP AIR"){
                    ordca = data4.val().itemQty;
                    delca = data4.val().itemDelivered;
                    orddelbal += Number(data4.val().itemQty) - Number(data4.val().itemDelivered);
                  }
                  else if(data4.key == "NITROUS DIOXIDE"){
                    ordn20 = data4.val().itemQty;
                    deln20 = data4.val().itemDelivered;
                    orddelbal += Number(data4.val().itemQty) - Number(data4.val().itemDelivered);
                  }
                  else if(data4.key == "TECH 02"){
                    ordt02 = data4.val().itemQty;
                    delt02 = data4.val().itemDelivered;
                    orddelbal += Number(data4.val().itemQty) - Number(data4.val().itemDelivered);
                  }
                  else if(data4.key == "ACETYLENE"){
                    ordc2h2 = data4.val().itemQty;
                    delc2h2 = data4.val().itemDelivered;
                    orddelbal += Number(data4.val().itemQty) - Number(data4.val().itemDelivered);
                  }
                  else if(data4.key == "CARBON DIOXIDE"){
                    ordc02 = data4.val().itemQty;
                    delc02 = data4.val().itemDelivered;
                    orddelbal += Number(data4.val().itemQty) - Number(data4.val().itemDelivered);
                  }
                  else if(data4.key == "NITROGEN"){
                    ordn2 = data4.val().itemQty;
                    deln2 = data4.val().itemDelivered;
                    orddelbal += Number(data4.val().itemQty) - Number(data4.val().itemDelivered);
                  }
                  else if(data4.key == "ARGON"){
                    ordarg = data4.val().itemQty;
                    delarg = data4.val().itemDelivered;
                    orddelbal += Number(data4.val().itemQty) - Number(data4.val().itemDelivered);
                  }
                  delpickuptable.push(data2.val().custCode+","+data2.val().SOType+","+data2.key
                                      +","+ordmed02+","+ordca+","+ordn20
                                      +","+ordt02+","+ordc2h2+","+ordc02
                                      +","+ordn2+","+ordarg
                                      +","+delmed02+","+delca+","+deln20
                                      +","+delt02+","+delc2h2+","+delc02
                                      +","+deln2+","+delarg+","+orddelbal.toString()
                                      +","+data2.val().PONum+","+"TypeSO");
                }
              });
            });
          });
          resolve(delpickuptable);
        });
      });

    var ICRnohldr = '';
    var ICRloopctr = 0;
    var ICRchildcthldr = 0;
    var tempICRarrhldr = [];

    ordmed02 = 0;
    ordca = 0;
    ordn20 = 0;
    ordt02 = 0;
    ordc2h2 = 0;
    ordn2 = 0;
    ordarg = 0;
    ordc02 = 0;

    delmed02 = 0;
    delca = 0;
    deln20 = 0;
    delt02 = 0;
    delc2h2 = 0;
    deln2 = 0;
    delarg = 0;
    delc02 = 0;
    orddelbal = 0;

    getICRItemsforDate  = new Promise((resolve, reject) => {
      icrRef.once("value", (data) => {
        data.forEach((data2) => {
          data2.forEach((data3) => {
            data3.forEach((data4) => {
              if(data2.val().date == DateofCreation){
                if(ICRloopctr == 0){
                  ICRnohldr = data2.key;
                  ICRchildcthldr = data3.numChildren();
                }
                else if(ICRnohldr == data2.key && ICRloopctr == ICRchildcthldr-1){
                                        //CustCode    //SOType   //PONUM
                    delpickuptable.push(sampdata+","+sampdata+","+data2.key
                                          +","+ordmed02+","+ordca+","+ordn20
                                          +","+ordt02+","+ordc2h2+","+ordc02
                                          +","+ordn2+","+ordarg
                                          +","+delmed02+","+delca+","+deln20
                                          +","+deln2+","+delc2h2+","+delc02
                                          +","+deln2+","+delarg+","+sampdata
                                          +","+"TypeICR");
                    delmed02 = 0;
                    delca = 0;
                    deln20 = 0;
                    deln2 = 0;
                    delc2h2 = 0;
                    delc02 = 0;
                    deln2 = 0;
                    delarg = 0;
                    ICRloopctr = 0;
                }
                else if(ICRnohldr != data2.key && ICRloopctr !=0){
                                      //CustCode    //SOType   //PONUM
                    delpickuptable.push(sampdata+","+sampdata+","+data2.key
                    +","+ordmed02+","+ordca+","+ordn20
                    +","+ordt02+","+ordc2h2+","+ordc02
                    +","+ordn2+","+ordarg
                    +","+delmed02+","+delca+","+deln20
                    +","+deln2+","+delc2h2+","+delc02
                    +","+deln2+","+delarg+","+sampdata
                    +","+"TypeICR");

                  delmed02 = 0;
                  delca = 0;
                  deln20 = 0;
                  deln2 = 0;
                  delc2h2 = 0;
                  delc02 = 0;
                  deln2 = 0;
                  delarg = 0;
                  ICRloopctr = 0;
                  ICRnohldr = data2.key;
                  ICRchildcthldr = data3.numChildren();
                }

                if(data4.val().type == "MED 02" && ICRnohldr == data2.key){
                  delmed02++;
                }
                else if(data4.val().type == "COMP AIR" && ICRnohldr == data2.key){
                  delca++;
                }
                else if(data4.val().type == "NITROUS DIOXIDE" && ICRnohldr == data2.key){
                  deln20++;
                }
                else if(data4.val().type == "TECH 02" && ICRnohldr == data2.key){
                  delt02++;
                }
                else if(data4.val().type == "ACETYLENE" && ICRnohldr == data2.key){
                delc2h2++;
                }
                else if(data4.val().type == "CARBON DIOXIDE" && ICRnohldr == data2.key){
                  delc02++;
                }
                else if(data4.val().type == "NITROGEN" && ICRnohldr == data2.key){
                  deln2++;
                }
                else if(data4.val().type == "ARGON" && ICRnohldr == data2.key){
                  delarg++;
                }
                ICRloopctr++;
              }
            });
          });
        });
        resolve(delpickuptable);
      });
    });
    retrieveDeliveryDataAW();
  }
});

async function retrieveDeliveryDataAW() {
  const deliveries = await getDeliveryList;
  const SOItems = await getSOItems;
  const SOItemsforDate = await getSOItemsforDate;
  const ICRItemsforDate = await getICRItemsforDate;
  
  document.querySelector(".modal-title").textContent = SOID;

  table2 = $('#itemsOrderedTable').DataTable( {
      data: SOItems,
      select: true,
      columns: [
        { title: "Cylinder Type" },
        { title: "Price" },
        { title: "Quantity" }
      ]
  } );

  table3 = $('#itemsDeliveredTable').DataTable( {
      data: deliveries,
      select: true,
      columns: [
        { title: "Cylinder Type" },
        { title: "Quantity" },
        { title: "Delivery Date" }
      ]
  } );
  document.querySelector(".loading2").style.display = "none";
  $('#myModal').modal({
    backdrop: 'static',
    keyboard: false
  });
  $('#myModal').modal("show");
}

function clearTables() {
  table2.destroy();
  table3.destroy();
}
