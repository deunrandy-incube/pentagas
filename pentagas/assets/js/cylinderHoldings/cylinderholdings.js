//cylinder balance variables
var items = [];
var customerlist = [];
var daterecords = [];
var cylindervalues = [];
var tempcylinderhldr = [];
var splittedvalues = [];
var datestemphldr = [];
var valueshldr = [];
var finaldatevalues = [];
var finalcylvalues = [];
var nameloopctr = 0;
var cylinderctr = 0;
var itemstemp = [];

//PDF variables
var pdfitems = [];

//database references
var databaseRef = firebase.database().ref();
var inventoryRef = databaseRef.child("Inventory");
var customerRef = databaseRef.child("Customer");
var bigLedgerRef = databaseRef.child("Big Ledger");
var deliveryRef = databaseRef.child("Delivery Receipt");
var invoiceRef = databaseRef.child("Sales Invoice");
var icrRef = databaseRef.child("Incoming Cylinder Report");

//pdfvariables

document.querySelector(".loading").style.display = "block";

var table = '';
//loading of data from DB
const getCustomers = new Promise((resolve, reject) => {
  customerRef.once("value", (data) => {
    data.forEach((data2) => {
        customerlist.push([data2.val().Name, data2.val().Address]);
    });
    resolve(customerlist);
  });
});

//table assignment
async function retrieveSOListAW(){
  const customerlist = await getCustomers;
  document.querySelector(".loading").style.display = "none";
  table = $('#cylinderholdingstable').DataTable( {
      data: customerlist,
      select: true,
      columns: [
          { title: "Customer Name" },
          { title: "Customer Address" },
      ]
  } );
}
retrieveSOListAW();

var custname = '';
var custaddress = '';
$('#cylinderholdingstable tbody').on('click', 'td', (e) => {
  document.querySelector(".loading2").style.display = "block";
  var selectedRow = e.target.parentNode;
  custname = selectedRow.children[0].innerText;
  custaddress = selectedRow.children[1].innerText;
  
  getICRItems = new Promise((resolve, reject) => {
    var icritems = [];
    icrRef.orderByChild("custName").equalTo(custname).once("value", (data) => {
      data.forEach((data2) => {
        let datetemp = moment(data2.val().date, 'MM/DD/YYYY HH:mm A');
        let newicrdate = datetemp.format('MM/DD/YYYY, h:mm a');
        icritems.push({
            "key": data2.key,
            "date": newicrdate,
            // "unit": data2.val().oRideCylCtr,
            "unit": getMapSize(data2.val().Cylinders),
            "status": "IN",
            "priority": 1,
          })
      })
      resolve(icritems);
    });
  })

  getDRItems = new Promise((resolve, reject) => {
    var dritems = [];
    deliveryRef.orderByChild("custName").equalTo(custname).once("value", (data) => {
      data.forEach((data2) => {
        let datetemp = moment(data2.val().Date, 'MM/DD/YYYY HH:mm:ss A');
        let newdrdate = datetemp.format('MM/DD/YYYY, h:mm a');
        dritems.push({
          "key": data2.key,
          "date": newdrdate,
          "unit": getMapSize(data2.val().items),
          "status": "OUT",
          "priority": 0,
        })
      })
      resolve(dritems);
    })
  });

  getSIItems = new Promise((resolve,reject) => {
    var siitems = [];
    invoiceRef.orderByChild("CustomerName").equalTo(custname).once("value", (data) => {
      data.forEach((data2) => {
        let datetemp = moment(data2.val().Date, 'MM/DD/YYYY HH:mm:ss A');
        let newsidate = datetemp.format('MM/DD/YYYY, h:mm a');
        
        siitems.push({
            "key": data2.key,
            "date": newsidate,
            "unit": getMapSize(data2.val().items),
            "status": "OUT",
            "priority": 0,
          })
      })
      resolve(siitems);
    })
  })

  retrieveLedgerDataAW();
});

async function retrieveLedgerDataAW() {
  const dritems = await getDRItems;
  const icritems = await getICRItems;
  const siitems = await getSIItems;

  items = [];
  pdfitems = [];
  itemstemp = [];
  let outitemstemp = [];
  outitemstemp = dritems.concat(siitems);
  itemstemp = outitemstemp.concat(icritems);
  itemstemp.sort(function(a, b) {
    return moment(a.date).diff(moment(b.date)) || a.priority - b.priority;
  });

  getCylinderHoldings();

  
  document.querySelector(".modal-title").textContent = custname;
  table2 = $('#ledgerItemsTable').DataTable( {
      data: items,
      select: true,
      bSort : false,
      columns: [
        { title: "Date" },
        { title: "Reference" },
        { title: "Debit" },
        { title: "Credit" },
        { title: "Balance" },
      ]
  } );
  document.querySelector(".loading2").style.display = "none";
  $('#myModal').modal({
    backdrop: 'static',
    keyboard: false
  });
  $('#myModal').modal("show");
}

function getCylinderHoldings(){
  var datetimesplitter = '';
  var timehldr = '';
  var credit = 0;
  var debit = 0;
  var balance = 0;
  
    for(var balctr=0; balctr<itemstemp.length; balctr++){
      let datetemp = moment(itemstemp[balctr].date, 'MM/DD/YYYY, h:mm:ss a');
      datetimesplitter = datetemp.format('MM/DD/YYYY h:mm a');

      if(itemstemp[balctr].status == "OUT"){
        debit+=parseInt(itemstemp[balctr].unit);
        balance+=parseInt(itemstemp[balctr].unit);
      }else if(itemstemp[balctr].status =="IN"){
        credit+=parseInt(itemstemp[balctr].unit);
        balance-=parseInt(itemstemp[balctr].unit);
      }

      items.push([datetimesplitter, itemstemp[balctr].key, debit, credit, balance]);
      pdfitems.push(datetimesplitter+","+itemstemp[balctr].key+","+debit+","+credit+","+balance);

      debit = 0;
      credit = 0;

      // if(balctr == 0){
      //   tempdatehldr = itemstemp[balctr].date;
      //   tempcylnohldr = itemstemp[balctr].key;
      // }else if(balctr == itemstemp.length-1 && balctr !=0){
      //   datetimesplitter = (itemstemp[balctr].date).split(" ");
      //   items.push([datetimesplitter[0], itemstemp[balctr].key, debit, credit, balance]);
      //   pdfitems.push(datetimesplitter[0]+","+itemstemp[balctr].key+","+debit+","+credit+","+balance);
      // }else if(tempdatehldr != itemstemp[balctr].date && balctr != 0){
      //     datetimesplitter = (itemstemp[balctr].date).split(" ");
      //     items.push([datetimesplitter[0], tempcylnohldr, debit, credit, balance]);
      //     pdfitems.push(datetimesplitter[0]+","+tempcylnohldr+","+debit+","+credit+","+balance);
      //     tempdatehldr = itemstemp[balctr].date;
      //     tempcylnohldr = itemstemp[balctr].key;
      //     debit = 0;
      //     credit = 0;
      // }
  }
}

function clearTables() {
  table2.destroy();
}


function getMapSize(x) {
  var len = 0;
  for (var count in x) {
          len++;
  }
  return len;
}

function sortDate( a, b ) {
  if ( a.date < b.date ){
    return -1;
  }
  if ( a.date > b.date ){
    return 1;
  }
  return 0;
}

    // inventoryRef.once("value", (data) => {
    //   data.forEach((data2) => {
    //     data2.forEach(function(data3){
    //       data3.forEach(function(data4){
    //           if(custname == data2.val().cylindercust){
    //               if(tempcylinderhldr.indexOf(data2.key) == -1){
    //                 tempcylinderhldr.push(data2.key);
    //                 cylindervalues.push(data4.val().referenceno+","+data2.val().cylinderstatus+","+data4.val().recordcust+","+data4.val().recordstatus+","+data4.val().recorddate);
    //               }
    //             }
    //           });
    //         });
    //       });

    //       var datetimesplitter = '';
    //       var timehldr = '';

    //       for(var balctr=0; balctr<cylindervalues.length; balctr++){
    //         splittedvalues = cylindervalues[balctr].split(",");
    //         if(balctr == 0){
    //           tempdatehldr = splittedvalues[4];
    //           tempcylnohldr = splittedvalues[0];
    //         }else if(balctr == cylindervalues.length-1 && balctr !=0){
    //           datetimesplitter = splittedvalues[4].split(" ");
    //           items.push([datetimesplitter[0], splittedvalues[0], debit, credit, balance]);
    //           pdfitems.push(datetimesplitter[0]+","+splittedvalues[0]+","+debit+","+credit+","+balance);
    //         }else if(tempdatehldr != splittedvalues[4] && balctr != 0){
    //             datetimesplitter = splittedvalues[4].split(" ");
    //             items.push([datetimesplitter[0], tempcylnohldr, debit, credit, balance]);
    //             pdfitems.push(datetimesplitter[0]+","+tempcylnohldr+","+debit+","+credit+","+balance);
    //             tempdatehldr = splittedvalues[4];
    //             tempcylnohldr = splittedvalues[0];
    //             debit = 0;
    //             credit = 0;
    //         }
    //         if(splittedvalues[3] == "Out"){
    //           debit++;
    //           balance++;
    //         }else if(splittedvalues[3] =="In"){
    //           credit++;
    //           balance--;
    //         }
    //     }
    //   resolve(items);
    // });