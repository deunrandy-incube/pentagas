//for PDF valid
var forViewing = "false";
var dispnav = localStorage['dispatcherNav'] || 'defaultValue';

//database references
var databaseRef = firebase.database().ref();
var productRef = databaseRef.child("Inventory");
var SORef = databaseRef.child("Sales Order");
var dispatchRef = databaseRef.child("Dispatch Slip");
var bigledgerRef = databaseRef.child("Big Ledger");
var smallledgerRef = databaseRef.child("Small Ledger");
var salesOrderRef = databaseRef.child("Sales Order");
//input fields
var dispatchidhldr = document.getElementById("DispId");
var customername = document.getElementById("CustomerName");
var dispatchdate = document.getElementById("DispatchDate");
var cylindertype = document.getElementById("CylinderType");
var preparedby = document.getElementById("PrepBy");
var cylinderlimit = document.getElementById("CylLimit");
var tableSearch = '';
var cylinderquan = 0;

//variables
var table = '';
var table2 = '';
var table3 = '';
var row = '';
var cylinderID = '';
var cylinder = '';

//get date today
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1;
var yyyy = today.getFullYear();
var hr = today.getHours();
var min = today.getMinutes();
var sec = today.getSeconds();

if(dd<10)
{
  dd='0'+dd;
}

if(mm<10)
{
  mm='0'+mm;
}
var datetoday = mm+'-'+dd+'-'+yyyy+" "+hr+":"+min+":"+sec;

//checker
var lastidindb = 0;
var itemIDs = [];
var itemTypes = [];

//loading screen
document.querySelector(".loading").style.display = "block";

//the SO selected from prev page
var selectedRow = localStorage.getItem("selectedRow");
var SOList = [];

//loading of data from DB
const getSO = new Promise((resolve, reject) => {
  SORef.child(selectedRow).once("value", (data2) => {
    SOList.push(data2.key, data2.val().customerName, data2.val().custCode, data2.val().delAddress);
    resolve(SOList);
  });
});

const getSOItems = new Promise((resolve, reject) => {
  var SalesOrders = [];
  var items = [];
  salesOrderRef.child(selectedRow).child("items").once("value", (data) => {
    data.forEach((data2) => {
      if (data2.key != "Others"){
        items.push([data2.key, data2.val().itemQty, data2.val().itemDelivered, data2.val().itemDispatched, data2.val().itemPrice]);
      }
    });
  })
  .then(() => {
    salesOrderRef.child(selectedRow).child("items").child("Others").once("value", (data) => {
      data.forEach((data2) => {
        items.push([data2.key, data2.val().itemQty, data2.val().itemDelivered, data2.val().itemDispatched, data2.val().itemPrice]);
      });
      resolve(items);
    })
  });
})

function getProducts(CylinderType) {
  document.querySelector(".loading2").style.display = "block";
  return new Promise((resolve, reject) => {
    var products = [];
    productRef.orderByChild("cylindertype").equalTo(CylinderType).once("value", (data) => {
      data.forEach((data2) => {
        if(data2.val().cylinderstatus == "Production" || data2.val().cylinderstatus == "Empty"){
          products.push([data2.key, data2.val().cylindertype]);
        }
      });
      resolve(products);
    });
  });
}

var productArr = [];
var SOItems = [];
//table assignment
async function retrieveSOListAW(){
  SOList = await getSO;
  SOItems = await getSOItems;

  var SOItemName = [];
  SOItems.forEach((data) => {
    SOItemName.push(data[0]);
  })
  var options = '';
  for(var i = 0; i < SOItemName.length; i++)
    options += '<option value="'+SOItemName[i]+'" />';
  document.getElementById('Products').innerHTML = options;
  $( "#CylinderType" ).focus();
  customername.value = SOList[1];
  dispatchdate.value = datetoday;
  
  table = $('#productsTable').DataTable( {
      select: true,
      columns: [
          { title: "Cylinder ID" },
          { title: "Cylinder" },
          { title: "Action" , "defaultContent": "<a class='btn btn-info btn-round btn-sm' style='color:white;'>ADD</a>"}
      ]
  } );
  //for order table
  table2 = $('#orderTable').DataTable( {
      "ordering": false,
      select: true,
      columns: [
          { title: "Cylinder ID" },
          { title: "Cylinder" },
          { title: "Action" }
      ]
  } );
  //for SO items table
  table3 = $('#SOItemsTable').DataTable( {
      data: SOItems,
      select: true,
      columns: [
          { title: "Cylinder Type" },
          { title: "Quantity" },
          { title: "Delivered" },
          { title: "Dispatched" },
          { title: "Price" }
      ]
  } );
  tableSearch = document.getElementsByClassName("form-control-sm");
  tableSearch[1].classList.add("text-danger");
  tableSearch[3].classList.add("text-danger");
  tableSearch[5].classList.add("text-danger");
  tableSearch[3].addEventListener("focus", () => {
    tableSearch[3].addEventListener("keypress", (e) => {
      if (e.key === "Enter"){
        
      }
    });
  })
  document.querySelector(".loading").style.display = "none";
}
retrieveSOListAW();
var selectedCylinderHldr = '';
//input event for customer name
$('#CylinderType').bind("input", () => {
  var val = document.getElementById("CylinderType").value;
  var options = document.getElementById("Products").childNodes;
  for (var i = 0; i < options.length; i++){
    if (options[i].value === val){
      selectedCylinderHldr = val;
      fillData(val);
    }
  }
});

async function fillData(selectedCylinder){
  table.destroy();
  table2.destroy();
  var tableArray = [];
  
  const productArr = await getProducts(selectedCylinder);
  tableArray = productArr;
  table = $('#productsTable').DataTable( {
      data: tableArray,
      select: true,
      columns: [
          { title: "Cylinder ID" },
          { title: "Cylinder" },
          { title: "Action" , "defaultContent": "<a class='btn btn-info btn-round btn-sm' style='color:white;'>ADD</a>"}
      ]
  } );
  //for order table
  table2 = $('#orderTable').DataTable( {
      "ordering": false,
      data : [],
      select: true,
      columns: [
          { title: "Cylinder ID" },
          { title: "Cylinder" },
          { title: "Action" }
      ]
  } );
  cylinderquan = 0;
  tableSearch[3].addEventListener("keypress", (e) => {
    if (e.key === "Enter"){
      row = table.row({search: 'applied'}).data();
      cylinderID = row[0];
      cylinder = row[1];
      table.row({search: 'applied'}).remove().draw(false);
      table2.row.add([
        cylinderID,
        cylinder,
        "<a class='btn btn-info btn-round btn-sm' style='color:white;'>Remove</a>"
      ]).draw(false);
      cylinderquan += 1;
      tableSearch[3].value = "";
    }
  });
  tableSearch[5].addEventListener("keypress", (e) => {
    if (e.key === "Enter"){
      row = table2.row({search: 'applied'}).data();
      cylinderID = row[0];
      cylinder = row[1];
      table2.row({search: 'applied'}).remove().draw(false);
      table.row.add([
        cylinderID,
        cylinder,
        "<a class='btn btn-info btn-round btn-sm' style='color:white;'>Add</a>"
      ]).draw(false);
      cylinderquan -= 1;
      tableSearch[5].value = "";
    }
  });
  document.querySelector(".loading2").style.display = "none";
}

document.getElementById("createDispatcherBtn").addEventListener("click", (event) => {
  event.preventDefault();
  if(dispatchidhldr){
    dispatchRef.child(dispatchidhldr.value).once('value', function(snapshot) {
      var exists = (snapshot.val() !== null);
      if(!exists){
        if (confirm("You are about to set these Cylinders for Deployment. Would you like to continue?")){
          validate();
        } else {
          // do something.
        }
      }else{
        window.alert(dispatchidhldr.value+" is existing, cannot create a Dispatcher Slip with the same ID.");
      }
    });
  }else{
    window.alert("Please input Dispatch ID");
  }
});

document.getElementById("cancelDispatcherBtn").addEventListener("click", (event) => {
  event.preventDefault();
  if (confirm("You are about to discard this session. Would you like to continue?")){
    window.location = "createDispatcher.html";
  } else {
    // do something.
  }
});

function validate(){
  SOItems.forEach((data) => {
    var data2 = table2.rows().data();
    if (data[0] === selectedCylinderHldr){
      if ((parseInt(data[3]) + parseInt(cylinderquan)) > data[1]){
        alert("Error. You cannot dispatch items more than the SO quantity");
      }
      else if(parseInt(cylinderquan) < cylinderlimit.value){
        alert("Error. You have NOT YET REACHED inputted Cylinder Limit. You've chosen "+cylinderquan.toString()+" out of "+cylinderlimit.value);
      }
      else if(parseInt(cylinderquan) > cylinderlimit.value){
        alert("Error. You have REACHED inputted Cylinder Limit. You've chosen "+cylinderquan.toString()+" out of "+cylinderlimit.value);
      }
      else if(parseInt(cylinderquan) <= 0){
        alert("Quantity cannot be equal or less than to 0");
      }
      else if(data2.length == 0){
        alert("Please select cylinders");
      }
      else {
        AWDispatchCylinders();
      }
    }
  })
}

var productsSelected = [];
var getConf1 = '';
async function AWDispatchCylinders(){
  document.querySelector(".loading2").style.display = "block";
  //retrieve data from table
  var data = table2.rows().data();
  var ctr = 1;
   data.each(function (value, index) {
       productsSelected.push([value[0], value[1], ctr]);
       ctr++;
   });

   dispatchRef.child(dispatchidhldr.value).set({
     custName: customername.value,
     SONumber: selectedRow,
     date: dispatchdate.value,
     cylinderType: cylindertype.value,
     prepby: preparedby.value,
     custCode: SOList[2],
     address: SOList[3],
     type: "Regular",
     status: "Dispatched"
   })
   .then(() => {

   });

   var productSet = false;
   var dispatchSet = false;
   var SOSet = false;
   var cylinderRecordID = '';
   var itemsprocessed = [0,0,0,0];
   var prevDelVal = await new Promise((resolve, reject) => {
     SORef.child(SOList[0]).child("items").child(cylindertype.value).once("value", (data) => {
       resolve(data.val().itemDispatched);
     })
   });
   var cylQuantityVal = await new Promise((resolve, reject) => {
     SORef.child(SOList[0]).child("items").child(cylindertype.value).once("value", (data) => {
       resolve(data.val().itemQty);
     })
   });
   let SOStatus = 'Partially Dispatched';
   let dispatchStatus = false;
   if ((Number(prevDelVal) + Number(cylinderquan)) == Number(cylQuantityVal)){
    // SOStatus = "Dispatched"
    dispatchStatus = true;
   }
   SORef.child(SOList[0]).update({
     status: SOStatus
   })
   .then((e) => {
     SORef.child(SOList[0]).child("items").child(cylindertype.value).update({
       itemDispatched: Number(prevDelVal) + Number(cylinderquan),
       isFullyDispatched: dispatchStatus
     })
     .then((e) => {
       SOSet = true;
       productsSelected.forEach((data2) => {
         cylinderRecordID = productRef.push().getKey();
         productRef.child(data2[0]).update({
           cylinderstatus : "Dispatched",
           cylindercust: customername.value
         })
         .then((e) => {
           itemsprocessed[0]++;
           if (itemsprocessed[0] === productsSelected.length){
             productSet = true;
             confirmSet(productSet, dispatchSet);
           }
         })
         .catch((e) => {
           alert(e);
         });

         dispatchRef.child(dispatchidhldr.value).child("Cylinders").child(data2[0]).set({
           type: data2[1],
           order: data2[2],
           sorterkey: databaseRef.push().getKey()
         })
         .then((e) => {
           itemsprocessed[1]++;
           if (itemsprocessed[1] === productsSelected.length){
             dispatchSet = true;
             confirmSet(productSet, dispatchSet);
           }

         })
         .catch((e) => {
           alert(e)
         });
       });
     });
   });
}

function confirmSet(productSet, dispatchSet){
  if (productSet && dispatchSet){
    validate2();
    window.location = "createDispatcher.html";
  }
}

async function validate2(){
  var items = [];
  var checker = [];
  var SOItems2 = new Promise((resolve, reject) => {
    var SalesOrders = [];
    salesOrderRef.child(selectedRow).child("items").once("value", (data) => {
      data.forEach((data2) => {
        if (data2.key != "Others"){
          items.push([data2.key, data2.val().itemQty, data2.val().itemDelivered, data2.val().itemDispatched, data2.val().itemPrice]);
        }
      });
    })
    .then(() => {
      salesOrderRef.child(selectedRow).child("items").child("Others").once("value", (data) => {
        data.forEach((data2) => {
          items.push([data2.key, data2.val().itemQty, data2.val().itemDelivered, data2.val().itemDispatched, data2.val().itemPrice]);
        })
        resolve(items);
      })
    });
  });

  SOItems2.then(() => {
    items.forEach((data) => {
      if ((parseInt(data[2]) + parseInt(data[3])) == data[1]){
        checker.push(true);
      } else {
        checker.push(false);
      }
    });
    document.querySelector(".loading2").style.display = "none";
  })
}

function checkStatus(stat){
  return stat;
}


$('#productsTable tbody').on('click', 'a', (e) => {
  var selectedRow = e.target.parentNode.parentNode;
  var cylinderID = selectedRow.children[0].innerText;
  var cylinder = selectedRow.children[1].innerText
  e.target.parentNode.parentNode.classList.toggle("selected");
  table.row(".selected").remove().draw(false);
  table2.row.add([
    cylinderID,
    cylinder,
    "<a class='btn btn-info btn-round btn-sm' style='color:white;'>Remove</a>"
  ]).draw(false);
  cylinderquan += 1;
});

$('#orderTable tbody').on('click', 'a', (e) => {
  var selectedRow = e.target.parentNode.parentNode;
  var cylinderID = selectedRow.children[0].innerText;
  var cylinder = selectedRow.children[1].innerText
  e.target.parentNode.parentNode.classList.toggle("selected");
  table2.row(".selected").remove().draw(false);
  table.row.add([
    cylinderID,
    cylinder,
    "<a class='btn btn-info btn-round btn-sm' style='color:white;'>Add</a>"
  ]).draw(false);
  cylinderquan -= 1;
});
