// tables
var table = '';

// references
var databaseRef = firebase.database().ref();
var productsRef = databaseRef.child("Inventory");
var dispatchRef = databaseRef.child("Dispatch Slip");
var customerRef = databaseRef.child("Customer");

var forViewing = "false";
var dispatchTableBool = "false";

// variables
var selectedCylinders = JSON.parse(localStorage.getItem("selectedCylinders"));
var selectedCylinderType = localStorage.getItem("selectedCylinderType");
var selectedCylinderStatus = localStorage.getItem("dispatchItemStatus");

// display loading screen
document.querySelector(".loading").style.display = "block";

// input fields
var custName = document.getElementById("CustomerName");
var dispatchDate = document.getElementById("DispatchDate")
var address = document.getElementById("Address");

var itemIDs = [];

const getCustomers = new Promise((resolve, reject) => {
  var customers = [];
  customerRef.once("value", (data) => {
    data.forEach((data2) => {
      customers.push(data2.val().Name);
    });
    resolve(customers);
  });
});

const getItemIDs = new Promise((resolve, reject) => {
  dispatchRef.once("value", (data) => {
    data.forEach((data2) => {
      var keyhldr = data2.key.split("-");
      itemIDs.push(parseInt(keyhldr[1]));
    });
    resolve(itemIDs);
  });
});

async function selectedCylinderAW() {
  IDList = await getItemIDs;
  const customers = await getCustomers;
  var options = '';
  for(var i = 0; i < customers.length; i++)
    options += '<option value="'+customers[i]+'" />';
  document.getElementById('Customers').innerHTML = options;

  table = $('#DispatchTable').DataTable({
    data: selectedCylinders,
    select: true,
    columns: [
      { title: "Cylinder No." },
      { title: "Cylinder Type" },
      { title: "Status", "defaultContent" : selectedCylinderStatus }
    ]
  });
  document.querySelector(".loading").style.display = "none";
}
selectedCylinderAW();

document.getElementById("createDispatcherBtn").addEventListener("click", (event) => {
  event.preventDefault();
  if(validate()){
    if (confirm("You are about to set these Cylinders for Deployment. Would you like to continue?")){
      document.querySelector(".loading2").style.display = "block";
      console.log("success");
      dispatchCylinders();
    } else {
      // do something.
    }
  }
})

function validate(){
  if (document.getElementById("CustomerName").value.length == 0 || document.getElementById("DispatchDate").value.length == 0 || document.getElementById("Address").value.length == 0){
    alert("Please fill up all the details needed");
    return false;
  } else {
    return true;
  }
}

$("#CustomerName").bind("input", () => {
  var val = document.getElementById("CustomerName").value;
  var options = document.getElementById("Customers").childNodes;
  for (var i = 0; i < options.length; i++){
    if (options[i].value == val){
      fillData(val);
    } else {
      document.getElementById("Address").value = "";
      customers.id = "";
      customers.Name = "";
      customers.Address = "";
      customers.OrderedBy = "";
      customers.DelAddress = "";
    }
  }
});

var getCustData = '';
var customers = {};
//function for autofill
function fillData(custName){
  document.querySelector(".loading2").style.display = "block";
  getCustData = new Promise((resolve, reject) => {
    customerRef.once("value", (data) => {
      data.forEach((data2) => {
        if (data2.val().Name === custName){
          customers.id = data2.key;
          customers.Name = data2.val().Name;
          customers.Address = data2.val().Address;
          customers.OrderedBy = data2.val().OrderedBy;
          customers.DelAddress = data2.val().DelAddress;
          customers.CustCode = data2.val().CustCode;
          customers.CustType = data2.val().CustType;
        }
      });
      resolve(customers);
    });
  });
  retrieveCustDataAW();
}

async function retrieveCustDataAW(){
  customer = await getCustData;
  document.getElementById("Address").value = customer.Address;
  document.querySelector(".loading2").style.display = "none";
}

function dispatchCylinders() {
  var checker = [false, false, false];
  var dispatchID;
  if(itemIDs.length == 0){
    dispatchID = "DISP-00001";
  }else{
    lastidindb = Math.max.apply(Math,itemIDs);
    dispatchID = "DISP-0000"+(lastidindb+1);
  }
  dispatchRef.child(dispatchID).set({
    custName: custName.value,
    SONumber: selectedCylinderStatus,
    date: dispatchDate.value,
    cylinderType: selectedCylinderType,
    address: address.value,
    type: selectedCylinderStatus + " Dispatch",
    status: "Dispatched"
  });
  if (customers.id.length === 0){
    var custID = customerRef.push().getKey();
    customerRef.child(custID).set({
      Name: custName.value,
      Address: address.value,
      Company: "N/A",
      Status: "Active"
    })
    .then(() => {
      checker[2] = true;
      validate2(checker);
    });
  } else {
    customerRef.child(customers.id).update({
      Name: custName.value,
      Address: address.value
    })
    .then(() => {
      checker[2] = true;
      validate2(checker);
    });
  }
  var itemsProcessed = [0,0];
  selectedCylinders.forEach((data) => {
    productsRef.child(data[0]).update({
      cylinderstatus: selectedCylinderStatus + " Dispatch"
    })
    .then(() => {
      itemsProcessed[0]++;
      if (itemsProcessed[0] === selectedCylinders.length){
        console.log("Success update on inventory");
        checker[0] = true;
        validate2(checker);
      }
    })
    dispatchRef.child(dispatchID).child("Cylinders").child(data[0]).update({
      type: data[1]
    })
    .then(() => {
      itemsProcessed[1]++;
      if (itemsProcessed[1] === selectedCylinders.length){
        console.log("Success update on dispatch");
        checker[1] = true;
        validate2(checker);
      }
    })
  })
}

function validate2(arrChecker) {
  if (arrChecker){
    window.location = "createDispatcher.html";
  }
}
