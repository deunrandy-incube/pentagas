//for PDF valid
var forViewing = "true";
var itemsList = [];
var dispnav = localStorage['dispatcherNav'] || 'defaultValue';
var specDispatcherInfo = [];
//input fields
var customername = document.getElementById("CustomerName");
var dispatchdate = document.getElementById("DispatchDate");
var cylindertype = document.getElementById("CylinderType");
var cylinderquan = document.getElementById("CylinderQty");
var dispcreationbtn = document.getElementById("dispatchercreationbtn");

//variables
//database references
var databaseRef = firebase.database().ref();
var productRef = databaseRef.child("Inventory");
var SORef = databaseRef.child("Sales Order");
var DispatchRef = databaseRef.child("Dispatch Slip");
var customerRef = databaseRef.child("Customer");

var customerInput = document.getElementById("customerInput");
var dispatchSearchInput = document.getElementById("dispatchSearchInput");
var mainDispArr = [];

var pageLoad = 20;

dispatchSearchInput.addEventListener('input', function (evt) {
  if (this.value == '') {
      stateTable(mainDispArr);
  }
});

var userrole = localStorage['userRole'] || 'defaultValue';
if (userrole === "cylcontrol") {
  dispcreationbtn.style.display = "none";
}

//get date today
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1;
var yyyy = today.getFullYear();
if(dd<10){
  dd='0'+dd;
}

if(mm<10){
  mm='0'+mm;
}
today = mm+'/'+dd+'/'+yyyy;

var table = '';
var table2 = '';
var table3 = '';
var table4 = '';

//display loading screen
document.querySelector(".loading").style.display = "block";

const getCustomers = new Promise((resolve, reject) => {
  var customers = [];
  customerRef.once("value", (data) => {
    data.forEach((data2) => {
      customers.push(data2.val().Name);
    });
    resolve(customers);
  });
  let initialDispArr = [];
  table2 = $('#dispatchList').DataTable( {
    data: initialDispArr,
    select: true,
    columns: [
      { title: "Dispatch No." },
      { title: "Customer Name" },
      { title: "Cylinder Type" },
      { title: "Status" }
    ],
    language: {
      "search": "Search Current Data in Table:",
      "emptyTable": "Select a Customer or Search a Specific ID to load Dispatcher Data"
  },
  } );
});

//data retrieval
function getDispatchList() {
  document.querySelector(".loading").style.display = "block";
  return new Promise((resolve, reject) => {
    var DispatchList = [];
    if(customerInput.value != 'All'){
      DispatchRef
      .orderByChild('custName')
      .equalTo(customerInput.value)
      .limitToLast(pageLoad)
      .once('value', snapshot => {
        if(snapshot.exists()){
          snapshot.forEach((data2) => {
            DispatchList.push([data2.key, data2.val().custName, data2.val().cylinderType, data2.val().status]);    
              resolve(DispatchList);
          }); 
        }else{
          document.querySelector(".loading").style.display = "none";
        }
      });
    }else{
      DispatchRef
      .limitToLast(pageLoad)
      .once('value', snapshot => {
        if(snapshot.exists()){
          snapshot.forEach((data2) => {
            DispatchList.push([data2.key, data2.val().custName, data2.val().cylinderType, data2.val().status]);    
            resolve(DispatchList);
          }); 
        }
      });
    }
  });
}

function retrieveSpecificDispatch() {
  if (dispatchSearchInput.value) {
    document.querySelector(".loading").style.display = "block";
    table2.clear();
    pageLoad = 20;
    DispatchRef.child(dispatchSearchInput.value).once('value', function (data2) {
      var exists = (data2.val() !== null);
      if (exists) {
        let specDispatchArr = [];
        specDispatchArr.push([data2.key, data2.val().custName, data2.val().cylinderType, data2.val().status]);    
        stateTable(specDispatchArr);
      } else {
        document.querySelector(".loading").style.display = "none";
        window.alert("Dispatcher Not Found.");
      }
    });
  } else {
    window.alert("Input is empty, please try again.");
  }
}

function stateTable(tablearr){
  var content = '';
  var paginate = false;
  paginate = tablearr.length > 1 ? true : false;

  if(table2){
    table2.destroy();
  }
  tablearr.forEach((value) => {
    content +='<tr id="' + "++" + value[0] + '">';
    content += '<td>' + value[0] + '</td>';
    content += '<td>' + value[1] + '</td>';
    content += '<td>' + value[2] + '</td>';
    content += '<td>' + value[3] + '</td>';
    content += '</tr>';
  });
  $('#dispatchList').append(content);

  table2 = $('#dispatchList').DataTable({
    "aaSorting": [],
    "bPaginate": paginate,
    drawCallback: function(){
      $('.paginate_button.next', this.api().table().container())          
         .on('click', async function(){
            document.querySelector(".loading").style.display = "block";
            pageLoad += 20;
            table2.clear();
            DispatchList = await getDispatchList();
            mainDispArr = DispatchList;
            stateTable(DispatchList);
      });       
   },
   "language": {
    "emptyTable": "Select a Customer or Search Specific Dispatch to load data."
  }
  });
  document.querySelector(".loading").style.display = "none";
}

//loading of data from DB
function getSOList() {
  return new Promise((resolve, reject) => {
    var SOList = [];
    SORef.on("child_added", function(data2) {
      if (data2.val().status === "Pending" || data2.val().status === "Partially Delivered" || data2.val().status === "Partially Dispatched" || data2.val().status === "Partially Invoiced"){
        SOList.push([data2.key, data2.val().customerName, data2.val().PONum, data2.val().status, data2.val().delDate]);
      }
      resolve(SOList);
    });
  });
} 

function getDefectiveList() {
  return new Promise((resolve, reject) => {
    var DefectiveList = [];
    productRef.on("child_added", function(data2) {
      if(data2.val().cylinderstatus === "Defective"){
        DefectiveList.push([data2.key, data2.val().cylindertype, data2.val().cylinderstatus])
      }
      resolve(DefectiveList);
    });
  });
}

function getEmptyList() {
  return new Promise((resolve, reject) => {
    var EmptyList = [];
    productRef.on("child_added", function(data2) {
      if(data2.val().cylinderstatus === "Empty"){
        EmptyList.push([data2.key, data2.val().cylindertype, data2.val().cylinderstatus])
      }
      resolve(EmptyList);
    });
  });
}

function deleteDispatcher(){
  if (confirm("You are about to delete this Dispatcher. Would you like to continue?")) {
    let checker = [false, false];
    let ctrchecker = 0;
    document.querySelector(".loading").style.display = "block";
    var cylcounter = 0;
    DispatchRef.child(DispatchID).remove().then(() => {
      SORef.child(SONumber).child("items").child(CylType).once('value', snapshot => {
        if(snapshot.exists()){
          cylcounter = snapshot.val().itemDispatched;
        }else{
          document.querySelector(".loading").style.display = "none";
        }
      }).then(() => {
        SORef.child(SONumber).child("items").child(CylType).update({
          itemDispatched: cylcounter - itemsList.length
        });
      }).then(() => {
        checker[0] = true;
        deleteChecker(checker);
      });
    })
  
    itemsList.forEach((data) => {
      productRef.child(data[0]).update({
        cylinderstatus: "Production"
      }).then(() => {
        ctrchecker++;
        if(ctrchecker == itemsList.length){
          checker[1] = true;
          deleteChecker(checker);
        }
      })
    })
  }
}

function deleteChecker(checker){
  if(checker[0] && checker[1]){
    document.querySelector(".loading").style.display = "none";
    window.alert("Dispatch Deleted.");
    window.location = "createDispatcher.html";
  }
}
 
//table assignment
async function retrieveSOListAW(){
  const CustomerList = await getCustomers;
  for(var x = 0; x< CustomerList.length; x++){
    customerInput.innerHTML += "<option value=\"" + CustomerList[x] + "\">" + CustomerList[x] + "</option>";
  }
  document.querySelector(".loading").style.display = "none";
}
retrieveSOListAW();

async function refreshDispatchTable(){
  pageLoad = 20;
  table2.clear();
  const DispatchList = await getDispatchList();
  mainDispArr = DispatchList;
  stateTable(DispatchList);
  document.querySelector(".loading").style.display = "none";
}

var DispatchID = '';
$('#dispatchList tbody').on('click', 'td', (e) => {
  var selectedRow = e.target.parentNode;
  DispatchID = selectedRow.children[0].innerText
  if (DispatchID != "No data available in table"){
    document.querySelector(".loading2").style.display = "block";
    
    getDispatchData = new Promise((resolve, reject) => {
      dispatchData = [];
      DispatchRef.child(DispatchID).once("value", (data2) => {
        dispatchData.push(data2.key, data2.val().custName, data2.val().cylinderType, data2.val().status,
        data2.val().address, data2.val().SONumber,data2.val().prepby,
        data2.val().custCode, data2.val().date);
        resolve(dispatchData);
      });
    });

    getItemList = new Promise((resolve, reject) => {
      itemsList = [];
      DispatchRef.child(DispatchID).child("Cylinders").orderByChild("sorterkey").once("value", (data) => {
        data.forEach((data2) => {
          let cylvaluehldr = '';
          if (dispnav == "cylindercontrol") {
            if((data2.key).indexOf("-") != -1){
              let cylsplit = (data2.key).split("-");
              cylinderval = cylsplit[1] + "-" + cylsplit[0];
            }else{
              cylinderval = data2.key;
            }
            
          } else {
            cylinderval = (data2.key);
          }
          itemsList.push([cylinderval, data2.val().type]);
        });
        resolve(itemsList);
      });
    });
    retrieveDispatchDataAW();
  }
});

function editDate(){
  document.querySelector(".loading2").style.display = "block";
  DispatchRef.child(DispatchID).update({
    date: document.getElementById("modalDateCr").value,
  }).then((e) => {
    window.location = "createDispatcher.html";
  });
}

var SONumber = "";
var CylType = "";

async function retrieveDispatchDataAW(){
  const dispatchData = await getDispatchData;
  specDispatcherInfo = dispatchData;
  document.getElementById("modalCustName").textContent = specDispatcherInfo[1];
  document.getElementById("modalCylType").textContent = specDispatcherInfo[2];
  document.getElementById("modalStatus").textContent = specDispatcherInfo[3];
  document.getElementById("modalAddress").textContent = specDispatcherInfo[4];
  document.getElementById("modalSONumber").textContent = specDispatcherInfo[5];
  document.getElementById("modalPrepBy").textContent = specDispatcherInfo[6];
  document.getElementById("modalCustCode").textContent = specDispatcherInfo[7];
  document.getElementById("modalDateCr").value = specDispatcherInfo[8];

  CylType = specDispatcherInfo[2];
  SONumber = specDispatcherInfo[5];

  document.querySelector(".modal-title").textContent = DispatchID;
  const itemsList = await getItemList;
  table5 = $('#itemsTable').DataTable( {
      "ordering": false,
      data: itemsList,
      select: true,
      columns: [
        { title: "Cylinder ID" },
        { title: "Quantity Type" }
      ]
  } );
  document.querySelector(".loading2").style.display = "none";
  $('#DispatcherModal').modal({
    backdrop: 'static',
    keyboard: false
  });
  $('#DispatcherModal').modal("show");
}

//for multiple row selection
$('#defectiveItemList tbody').on( 'click', 'tr', function () {
    $(this).toggleClass('selected');
} );
$('#emptyItemList tbody').on( 'click', 'tr', function () {
    $(this).toggleClass('selected');
} );

// //table click listener
$('#dispatchSOList tbody').on('click', 'td', (e) => {
  var selectedRow = e.target.parentNode;
  //set SO to local storage
  localStorage.setItem("selectedRow", selectedRow.children[0].innerText);
  if (selectedRow.children[0].innerText != "No data available in table"){
    if(confirm("Create dispatcher slip for " + selectedRow.children[0].innerText + "?")){
      window.location = "createDispatcher2.html";
    }
  }
});

async function openModal(){
  document.querySelector(".loading2").style.display = "block";
  const SOList = await getSOList();
  if(table){
    table.destroy();
  }
  table = $('#dispatchSOList').DataTable( {
    data: SOList,
    select: true,
    columns: [
      { title: "Sales Order No." },
      { title: "Customer Name" },
      { title: "Purchase Order No." },
      { title: "Status" },
      { title: "Delivery Date" }
    ]
  } );

  $('a[data-toggle="tab"]').on('shown.bs.tab', async function (e) {
    var target = $(e.target).attr("href") // activated tab
    
    if(target == '#defectiveDiv'){
      document.querySelector(".loading2").style.display = "block";
      const DefectiveItems = await getDefectiveList();
      if(table3){
        table3.destroy();
      }
      table3 = $('#defectiveItemList').DataTable( {
        data: DefectiveItems,
        select: true,
        columns: [
          { title: "Cylinder No." },
          { title: "Cylinder Type" },
          { title: "Status" }
        ]
      } );
      document.querySelector(".loading2").style.display = "none";
    }else if(target == '#emptyDiv'){
      document.querySelector(".loading2").style.display = "block";
      const EmptyItems = await getEmptyList();
      if(table4){
        table4.destroy();
      }
      table4 = $('#emptyItemList').DataTable( {
        data: EmptyItems,
        select: true,
        columns: [
          { title: "Cylinder No." },
          { title: "Cylinder Type" },
          { title: "Status" }
        ]
      } );
      document.querySelector(".loading2").style.display = "none";
    }
  });

  $('#myModal').modal({
    backdrop: 'static',
    keyboard: false
  });
  document.querySelector(".loading2").style.display = "none";
  $('#myModal').modal("show");
}

var selectedDefectCylinder = [];
var selectedEmptyCylinder = [];
var cylinderType = '';
function createDefectDispatch() {

  $('#defectiveItemList tbody')[0].childNodes.forEach((data) => {
    if(data.classList.contains("selected")){
      selectedDefectCylinder.push([data.childNodes[0].innerText, data.childNodes[1].innerText]);
      cylinderType = data.childNodes[1].innerText;
    }
  });
  if (selectedDefectCylinder.length == 0){
    alert("Please select cylinder(s)");
  } else {
    localStorage.setItem("selectedCylinders", JSON.stringify(selectedDefectCylinder));
    localStorage.setItem("selectedCylinderType", cylinderType);
    localStorage.setItem("dispatchItemStatus", "Defective");
    window.location = "createDefectDispatch.html";
  }
}
function createEmptyDispatch() {
  $('#emptyItemList tbody')[0].childNodes.forEach((data) => {
    if(data.classList.contains("selected")){
      selectedEmptyCylinder.push([data.childNodes[0].innerText, data.childNodes[1].innerText]);
      cylinderType = data.childNodes[1].innerText;
    }
  });
  if (selectedEmptyCylinder.length == 0){
    alert("Please select cylinder(s)");
  } else {
    localStorage.setItem("selectedCylinders", JSON.stringify(selectedEmptyCylinder));
    localStorage.setItem("selectedCylinderType", cylinderType);
    localStorage.setItem("dispatchItemStatus", "Empty");
    window.location = "createDefectDispatch.html";
  }
}

function clearTables() {
  table5.destroy();
}
