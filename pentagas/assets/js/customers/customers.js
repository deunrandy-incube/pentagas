var databaseRef = firebase.database().ref();
var customerRef = databaseRef.child("Customer");
var table = '';
document.querySelector(".loading").style.display = "block";
const getCustomers = new Promise((resolve, reject) => {
  var customers = [];
  customerRef.once("value", (data) => {
    data.forEach((data2) => {
      customers.push([data2.key, data2.val().Name, data2.val().Address, data2.val().Company, data2.val().Status]);
    });
    resolve(customers);
  });
});

async function retrieveCustAW(){
  const customers = await getCustomers;
  table = $('#customerTable').DataTable( {
      data: customers,
      select: true,
      columns: [
          { title: "Customer ID" },
          { title: "Customer Name" },
          { title: "Address" },
          { title: "Company" },
          { title: "Status" }
      ]
  } );
  document.querySelector(".loading").style.display = "none ";
}
retrieveCustAW();

$('#customerTable tbody').on('click', 'tr', () => {
  
});
