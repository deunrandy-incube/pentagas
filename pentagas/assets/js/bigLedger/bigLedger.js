//database references
var databaseRef = firebase.database().ref();
var bigledgerRef = databaseRef.child("Big Ledger");
var customerRef = databaseRef.child("Customer");

var cylindertypes = [];
var mainItemArr = [];
var mainItemPdfArr = [];
var mainItemActivePdfArr = [];
var forFilterArr = [];

var customerInput = document.getElementById("customerInput");
var cylinderFilter = document.getElementById("modalCylinderFilter");

//pdfvariables
var arraytest = [];
var activearraytest = [];

document.querySelector(".loading").style.display = "block";

var table = '';
var bigLedgerList = [];
//loading of data from DB

const getCustomers = new Promise((resolve, reject) => {
  var customers = [];
  customerRef.once("value", (data) => {
    data.forEach((data2) => {
      customers.push(data2.key+"--"+data2.val().Name);
    });
    resolve(customers);
  });
});

function getBigLedger(){
  document.querySelector(".loading").style.display = "block";
  return new Promise((resolve, reject) => {
    bigledgerRef.child(customerInput.value).once("value", (data2) => {
      if(data2.exists()){
        bigLedgerList=[];
        let dateSplitter = (data2.val().lastUpdate).split(" ");
        bigLedgerList.push([data2.key, data2.val().customerName, dateSplitter[0]]);
        resolve(bigLedgerList);
      }else{
        window.alert('No records found for this customer.');
        document.querySelector(".loading").style.display = "none";
      }
    });
  });
} 

async function getLedgerData(){
  const bigLedgerList = await getBigLedger();
  stateTable(bigLedgerList);
}

//table assignment
async function retrieveCustomerListAW(){
  const CustomerList = await getCustomers;
  for(var x = 0; x< CustomerList.length; x++){
    customerInput.innerHTML += "<option value=\"" + CustomerList[x].split("--")[0] + "\">" + CustomerList[x].split("--")[1] + "</option>";
  }
  document.querySelector(".loading").style.display = "none";
}
retrieveCustomerListAW();

function stateTable(tablearr){ 
  if(table){
    table.destroy();
  }
  table = $('#bigLedgerTable').DataTable( {
    data: tablearr,
    select: true,
    columns: [
        { title: "Customer ID" },
        { title: "Customer Name" },
        { title: "Last Update" }
    ]
} );
document.querySelector(".loading").style.display = "none";
}

var custID = '';
var custName = '';
var custCode = '';
var cylType = '';
$('#bigLedgerTable tbody').on('click', 'td', (e) => {
  var selectedRow = e.target.parentNode;
  custID = selectedRow.children[0].innerText;
  custName = selectedRow.children[1].innerText;
  cylType = selectedRow.children[2].innerText;
  cylinderFilter.innerHTML = "";
  cylinderFilter.innerHTML += "<option value=\"" + "All" + "\">" + "All" + "</option>";

  if (custID != "No data available in table"){
    document.querySelector(".loading2").style.display = "block";
    getLedgerItems = new Promise((resolve, reject) => {
      var items = [];
      var cylidsplitter = [];
      var datesplitter = [];
      var appendedcylid = '';
      var yearhldr = '';
      var newdate = '';
      var innewdate = '';
      var activenewdate = '';
      var datetimesplitter = '';

      arraytest = [];
      activearraytest = [];
      cylindertypes = [];
      mainItemArr = [];
      mainItemPdfArr = [];
      mainItemActivePdfArr = [];

      bigledgerRef.child(custID).child("items").orderByKey().once("value", (data) => {
        data.forEach((data2) => {
          data2.forEach((data3) =>{
            data3.forEach((data4) => {
              data4.forEach((data5) =>{
                  cylidsplitter = data2.key.split("-");   //Split Values
                  appendedcylid = cylidsplitter[1]+"-"+cylidsplitter[0];
                  console.log(data2.key, data5.val().out);
                  datetimesplitter = data5.val().out.split(" ");
                  if(datetimesplitter[0].indexOf("/") != -1){
                    datesplitter = datetimesplitter[0].split("/");
                  }else if(datetimesplitter[0].indexOf("-") != -1){
                    datesplitter = datetimesplitter[0].split("-");
                  }
                  
                  yearhldr = datesplitter[2].slice(-2);
                  newdate = datesplitter[0]+"-"+datesplitter[1]+"-"+yearhldr;
                  activenewdate = datesplitter[0]+"-"+datesplitter[1]+"-"+yearhldr;

                  if(data5.val().in != "ACTIVE"){
                    if(data5.val().in == "INCORRECT-ACTIVE"){
                      arraytest.push([appendedcylid, newdate, "ACTIVE", data5.val().cylinderType]);
                    }else{
                      datetimesplitter = data5.val().in.split(" ");
                      if(datetimesplitter[0].indexOf("/") != -1){
                        datesplitter = datetimesplitter[0].split("/");
                      }else if(datetimesplitter[0].indexOf("-") != -1){
                        datesplitter = datetimesplitter[0].split("-");
                      };
                      yearhldr = datesplitter[2].slice(-2);
                      innewdate = datesplitter[0]+"-"+datesplitter[1]+"-"+yearhldr;
                      arraytest.push([appendedcylid, newdate, innewdate, data5.val().cylinderType]);
                    }
                  }else{
                    activearraytest.push([appendedcylid, activenewdate, "ACTIVE", data5.val().cylinderType]);
                    arraytest.push([appendedcylid, newdate, "ACTIVE", data5.val().cylinderType]);
                  }
                  items.push([data5.val().out, appendedcylid, data5.val().in, data5.val().cylinderType]);
                  if(!cylindertypes.includes(data5.val().cylinderType)){
                    cylindertypes.push(data5.val().cylinderType);
                    cylinderFilter.innerHTML += "<option value=\"" + data5.val().cylinderType + "\">" + data5.val().cylinderType + "</option>";
                  }
              })
             
            })
          })
        });
        resolve(items);
      });
    });
    getCustCode = new Promise((resolve, reject) => {
      var code = '';
      customerRef.child(custID).once("value", (data) => {
        code = data.val().CustCode;
        resolve(code);
      });
    });
    retrieveLedgerDataAW();
  }
});

function filterLedgerTable() { 
  if(cylinderFilter.value == "All"){
    forFilterArr = mainItemArr;
    arraytest = mainItemPdfArr;
    activearraytest = mainItemActivePdfArr;
  }else{
    arraytest = [];
    activearraytest = [];
    
    for(var x =0; x < mainItemArr.length; x++){
      if(mainItemArr[x][3] == cylinderFilter.value){
        forFilterArr.push([mainItemArr[x][0], mainItemArr[x][1], mainItemArr[x][2], mainItemArr[x][3]]);
      }
    }
    for(var y=0; y < forFilterArr.length; y++){
      cylidsplitter = forFilterArr[y][1].split("-");   //Split Values
      appendedcylid = cylidsplitter[1]+"-"+cylidsplitter[0];
      datetimesplitter = forFilterArr[y][0].split(" ");
      if(datetimesplitter[0].indexOf("/") != -1){
        datesplitter = datetimesplitter[0].split("/");
      }else if(datetimesplitter[0].indexOf("-") != -1){
        datesplitter = datetimesplitter[0].split("-");
      }
                  
      yearhldr = datesplitter[2].slice(-2);
      newdate = datesplitter[0]+"-"+datesplitter[1]+"-"+yearhldr;
      activenewdate = datesplitter[0]+"-"+datesplitter[1]+"-"+yearhldr;

      if(forFilterArr[y][2] != "ACTIVE"){
        if(forFilterArr[y][2]== "INCORRECT-ACTIVE"){
          arraytest.push([appendedcylid, newdate, "ACTIVE", forFilterArr[y][3]]);
        }else{
          datetimesplitter = forFilterArr[y][2].split(" ");
          if(datetimesplitter[0].indexOf("/") != -1){
            datesplitter = datetimesplitter[0].split("/");
          }else if(datetimesplitter[0].indexOf("-") != -1){
            datesplitter = datetimesplitter[0].split("-");
          };
          yearhldr = datesplitter[2].slice(-2);
          innewdate = datesplitter[0]+"-"+datesplitter[1]+"-"+yearhldr;
          arraytest.push([appendedcylid, newdate, innewdate, forFilterArr[y][3]]);
        }
      }else{
        activearraytest.push([appendedcylid, activenewdate, "ACTIVE", forFilterArr[y][3]]);
        arraytest.push([appendedcylid, newdate, "ACTIVE", forFilterArr[y][3]]);
      }
    }

    table2.destroy();
    table2 = $('#ledgerItemsTable').DataTable( {
      data: forFilterArr,
      select: true,
      columns: [
        { title: "Out" },
        { title: "Cylinder ID" },
        { title: "In" }
      ]
  } );
  }
}

async function retrieveLedgerDataAW() {
  const items = await getLedgerItems;
  const code = await getCustCode;
  mainItemArr = items;
  mainItemPdfArr = arraytest;
  mainItemActivePdfArr = activearraytest;
  
  document.querySelector(".modal-title").textContent = code;

  table2 = $('#ledgerItemsTable').DataTable( {
      data: items,
      select: true,
      columns: [
        { title: "Out" },
        { title: "Cylinder ID" },
        { title: "In" }
      ]
  } );

  document.querySelector(".loading2").style.display = "none";
  $('#myModal').modal({
    backdrop: 'static',
    keyboard: false
  });
  $('#myModal').modal("show");
}

function clearTables() {
  table2.destroy();
}
