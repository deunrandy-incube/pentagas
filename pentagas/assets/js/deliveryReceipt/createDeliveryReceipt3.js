//for PDF valids
var forViewing = "falseowners";
var PDFOwnersarr = [];

//input fields
var customername = document.getElementById("CustomerName");
var dispatchdate = document.getElementById("DispatchDate");
var cylindertype = document.getElementById("CylinderType");
var cylinderquan = document.getElementById("CylinderQty");

//get date today
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1;
var yyyy = today.getFullYear();
var hr = today.getHours();
var min = today.getMinutes();
var sec = today.getSeconds();

if(dd<10)
{
  dd='0'+dd;
}

if(mm<10)
{
  mm='0'+mm;
}
var datetoday = mm+'-'+dd+'-'+yyyy+" "+hr+":"+min+":"+sec;

//variables
var reqs_id = 0; //cylinderid index
var currcylinders = []; //currentcylinders in the db

var table = '';
var dispatchList = [];
var prevDelVal = '';
var unitPrice = '';
var unitCode = '';
var SOData2 = {};
var DRQuantity = '';

//database references
var databaseRef = firebase.database().ref();
var dispatchRef = databaseRef.child("Dispatch Slip");
var ICRRef = databaseRef.child("Incoming Cylinder Receipt");
var customerRef = databaseRef.child("Customer");
var SORef = databaseRef.child("Sales Order");
var DRRef = databaseRef.child("Delivery Receipt");
var bigledgerRef = databaseRef.child("Big Ledger");
var smallledgerRef = databaseRef.child("Small Ledger");
var productRef = databaseRef.child("Inventory");

//display loading screen
document.querySelector(".loading").style.display = "block";

//the dispatch selected from prev page
var ICRNum = localStorage.getItem("ICRNum");
//loading of data from DB

// const getUnitPrice = (custID, cylType) => {
//   return new Promise((resolve, reject) => {
//     var unitPrice2 = '';
//     console.log(custID);
//     console.log(cylType)
//     customerRef.child(custID).child("Item Prices").child(cylType).child("Price").once("value", (data) => {
//       unitPrice2 = data.val();
//       console.log(unitPrice2);
//       resolve(unitPrice2);
//     })
//
//   })
// }
//
// const getUnitCode = (custID, cylType) => {
//   return new Promise((resolve, reject) => {
//     var unitCode2 = '';
//     console.log(custID);
//     console.log(cylType)
//     customerRef.child(custID).child("Item Prices").child(cylType).once("value", (data) => {
//       data.forEach((data2) => {
//         if (data2.key === "code"){
//           unitCode2 = data2.val();
//           console.log(unitCode2);
//         }
//       })
//       resolve(unitCode2);
//     })
//
//   })
// }

// const getDispatchList = new Promise((resolve, reject) => {
//   var dispatchList = [];
//   console.log(selectedRow);
//   dispatchRef.child(selectedRow).child("Cylinders").once("value", (data) => {
//     data.forEach((data2) => {
//         dispatchList.push([data2.key, data2.val().type]);
//     });
//     resolve(dispatchList);
//   });
// });

// const getSO = new Promise((resolve, reject) => {
//   var SOData = {};
//   SORef.child(selectedSO).once("value", (data) => {
//     SOData.custName = data.val().customerName;
//     SOData.address = data.val().custAddress;
//     SOData.delAddress = data.val().delAddress;
//     SOData.SOPOnum = data.val().PONum;
//     SOData.custID = data.val().customerID;
//     SOData.custCode = data.val().custCode;
//     SOData.PRNum = data.val().PRNum;
//     SOData.Terms = data.val().Terms;
//     SOData.Date = data.val().createDate;
//     SOData.TinNum = data.val().custTINNum;
//     // console.log(SOData.custName)
//     resolve(SOData);
//   })
// })
//
// const getPrevDelVal = cylinderType => {
//   return new Promise((resolve, reject) => {
//     var delVal = '';
//     SORef.child(selectedSO).child("items").child(cylinderType).once("value", (data) => {
//       delVal = data.val().itemDelivered;
//       resolve(delVal);
//     })
//   })
// }

const getICR = new Promise((resolve, reject) => {
  var ICRInfo = [];
  ICRRef.once("value", (data) => {
    data.forEach((data2) => {
      if (data2.key == ICRNum){
        ICRInfo.push(data2.val().custCode, data2.val().custAddress, data2.val().custName);
      }
    });
    resolve(ICRInfo);
  })
})

const getICRItems = new Promise((resolve, reject) => {
  var items = [];
  ICRRef.child(ICRNum).child("Cylinders").child("Others").once("value", data => {
    data.forEach(data2 => {
      items.push([data2.key, data2.val().itemPrice]);
      PDFOwnersarr.push([data2.key, data2.val().itemPrice]);
    })
    resolve(items);
  })
})

var ICRItems = [];
//table assignment
async function retrieveSOListAW(){
  // dispatchList = await getDispatchList;
  var ICRInfo = await getICR;
  ICRItems = await getICRItems;
  // unitPrice = await getUnitPrice(SOData2.custID, dispatchList[0][1]);
  // unitCode = await getUnitCode(SOData2.custID, dispatchList[0][1]);
  // prevDelVal = await getPrevDelVal(dispatchList[0][1]);


  console.log(ICRInfo);
  console.log(ICRItems);
  document.getElementById("CustomerName").value = ICRInfo[2];
  document.getElementById("Address").value = ICRInfo[1];
  document.getElementById("DeliveredTo").value = ICRInfo[1];
  document.getElementById("Date").value = datetoday;
  $("#DRnum").focus();
  table = $('#productsTable').DataTable( {
    data: ICRItems,
    select: true,
    columns: [
      { title: "Cylinder ID" },
      { title: "Price" }
    ]
  } );
  DRQuantity = ICRItems.length;
  document.querySelector(".loading").style.display = "none";
}
retrieveSOListAW();

//create button
document.getElementById("createDeliveryRecieptBtn").addEventListener("click", (event) => {
  event.preventDefault();

  if(DRList.indexOf(inputs[0].value) == -1){
    if (confirm("You are about to create a Delivery Receipt. Would you like to continue?")){
      document.querySelector(".loading2").style.display = "block";
      validate2();
      // window.location = "createDeliveryReceipt.html";
    } else {
      // do something.
    }
  }else{
    window.alert(inputs[0].value+" is existing, cannot create a Delivery Receipt with the same ID.");
  }
  
});

//create button
document.getElementById("cancelDeliveryRecieptBtn").addEventListener("click", (event) => {
  event.preventDefault();
  if (confirm("You are about to discard this session. Would you like to continue?")){
    window.location = "createDeliveryReceipt.html";
  } else {
    // do something.
  }
});

var itemDesc = document.getElementById("ItemDesc");
var truckPlate = document.getElementById("TruckPlate");
var inputs = document.getElementsByTagName("input");
console.log(inputs);
//create function
async function createDR(){
  // console.log("success")
  var DRSet = false;
  var dispatchSet = false;
  var productSet = false;
  var smallLedgerSet = false;
  var bigLedgerSet = false;
  var itemsprocessed = [0,0,0,0];
  var cylinderRecordID = '';
  validate();

  // var hldr = await setDR;

  // dispatchRef.child(selectedRow).update({
  //   status: "Delivered"
  // })
  // .then(() => {
  //   dispatchSet = true;
  // });

  DRRef.child(inputs[0].value).set({
    custName: inputs[1].value,
    address: inputs[2].value,
    DeliveredTo: inputs[3].value,
    Date: inputs[4].value,
    PONum: inputs[5].value,
    RCNum: inputs[6].value,
    Terms: inputs[7].value,
    PRNum: inputs[8].value,
    IssuedAt: inputs[9].value,
    Salesman: inputs[10].value,
    RefDRNum: inputs[11].value,
    On: inputs[13].value,
    PreparedBy: inputs[14].value,
    ReceivedBy: inputs[15].value,
    DeliveredBy: inputs[16].value,
    DriversLicense: inputs[17].value,
    TruckPlate: truckPlate.value,
    itemDesc: itemDesc.options[itemDesc.selectedIndex].value,
    ICRNum: ICRNum,
    SONum: "--",
    // unitPrice: unitPrice,
    itemQty: DRQuantity + " cyls",
    status: "Delivered"
  })
  .then((e) => {
    ICRItems.forEach((data) => {
      DRRef.child(inputs[0].value).child("items").child(data[0]).set({
        cylinderType: data[1]
      })
      .then((e) => {
        itemsprocessed[0]++;
        if (itemsprocessed[0] === ICRItems.length){
          console.log("success prod");
          DRSet = true;
          confirmSet(DRSet);
        }
        console.log("child added");
      });
    });
  })
  .catch((e) => {
    console.log(e);
  });
  // dispatchList.forEach((data) => {
  //   productRef.child(data[0]).update({
  //     cylindercust: inputs[1].value,
  //     cylinderstatus : "Active"
  //   })
  //   .then((e) => {
  //     cylinderRecordID = productRef.push().getKey()
  //     productRef.child(data[0]).child("CylinderRecord").child(datetoday).set({
  //       recorddate: inputs[4].value,
  //       recordcust: inputs[1].value,
  //       referenceno: inputs[0].value,
  //       custCode: SOData2.custCode,
  //       recordstatus: "Out"
  //     })
  //     .then((e) => {
  //       itemsprocessed[1]++;
  //       if (itemsprocessed[1] === dispatchList.length){
  //         console.log("success prod");
  //         productSet = true;
  //         confirmSet(DRSet, productSet, dispatchSet, bigLedgerSet, smallLedgerSet);
  //       }
  //     })
  //   })
  //   .catch((e) => {
  //     console.log(e);
  //   });
  //   bigledgerRef.child(SOData2.custID).update({
  //     customerName: SOData2.custName,
  //     lastUpdate: datetoday
  //   })
  //   bigledgerRef.child(SOData2.custID).child("items").child(data[0]).update({
  //     cylinderType: data[1],
  //     out: datetoday,
  //     in: "ACTIVE"
  //   })
  //   .then((e) => {
  //     itemsprocessed[2]++;
  //     if (itemsprocessed[2] === dispatchList.length){
  //       console.log("success big");
  //       bigLedgerSet = true;
  //       confirmSet(DRSet, productSet, dispatchSet, bigLedgerSet, smallLedgerSet);
  //     }
  //
  //   })
  //   .catch((e) => {
  //     alert(e);
  //   });
  //   smallledgerRef.child(data[0]).update({
  //     customer: SOData2.custName,
  //     lastUpdate: datetoday,
  //     lastOut: datetoday,
  //     cylinderType: data[1],
  //     status: "Active"
  //   })
  //   .then((e) => {
  //     smallledgerRef.child(data[0]).child("CylinderRecord").child(datetoday).set({
  //       out: datetoday,
  //       customer: SOData2.custName,
  //       customerID: SOData2.custID,
  //       custCode: SOData2.custCode,
  //       in: "ACTIVE"
  //     })
  //     .then((e) => {
  //       itemsprocessed[3]++;
  //       if (itemsprocessed[3] === dispatchList.length){
  //         console.log("success small");
  //         smallLedgerSet = true;
  //         confirmSet(DRSet, productSet, dispatchSet, bigLedgerSet, smallLedgerSet);
  //       }
  //
  //     })
  //   })
  //   .catch((e) => {
  //     alert(e);
  //   })
  // });
  // SORef.child(selectedSO).child("items").child(dispatchList[0][1]).update({
  //   itemDelivered: Number(prevDelVal) + dispatchList.length,
  //   latestDeliveryDate: inputs[4].value
  // })
  // console.log("before set");
  // const hldr = await setDR;
  // console.log("after set " + hldr);

}

function confirmSet(DRSet){
  if (DRSet){
    console.log("successful set");
    // validate2();
    document.querySelector(".loading2").style.display = "none";
    window.location = "createDeliveryReceipt.html";
  }
  // window.location = "createDispatcher.html";
}

//validation
function validate2(){
  // console.log("valid")
  var drnum = document.getElementById("DRnum");
  if(drnum.value.length < 1){
    alert("DR No cannot be empty");
    document.querySelector(".loading2").style.display = "none";
  } else {
    createDR();
  }
  console.log(drnum.value.length);
}

function validate(){
  // console.log("valid")
  var inputs = document.getElementsByTagName("input");
  for (var i = 0; i < 19; i++){
    console.log(i + " " + inputs[i].id);
    if (i === 12){
      continue;
    } else {
      if (inputs[i].value == ""){
        // console.log(i + " " + inputs[i].id + ": empty")
        inputs[i].value = "--";
      }
    }

  }
}
