//for PDF valids
var forViewing = "false";

//input fields
var customername = document.getElementById("CustomerName");
var dispatchdate = document.getElementById("DispatchDate");
var cylindertype = document.getElementById("CylinderType");
var cylinderquan = document.getElementById("CylinderQty");

//get date today
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1;
var yyyy = today.getFullYear();
var hr = today.getHours();
var min = today.getMinutes();
var sec = today.getSeconds();


if(dd<10)
{
  dd='0'+dd;
}

if(mm<10)
{
  mm='0'+mm;
}
var datetoday = mm + '/' + dd + '/' + yyyy + " " + hr + ":" + min + ":" + sec + " AM";

//variables
var reqs_id = 0; //cylinderid index
var currcylinders = []; //currentcylinders in the db

var table = '';
var dispatchList = [];
var prevDelVal = '';
var unitPrice = '';
var unitCode = '';
var SOData2 = {};
var DRQuantity = '';

//database references
var databaseRef = firebase.database().ref();
var dispatchRef = databaseRef.child("Dispatch Slip");
var customerRef = databaseRef.child("Customer");
var SORef = databaseRef.child("Sales Order");
var DRRef = databaseRef.child("Delivery Receipt");
var bigledgerRef = databaseRef.child("Big Ledger");
var smallledgerRef = databaseRef.child("Small Ledger");
var productRef = databaseRef.child("Inventory");
var custCylindersRef = databaseRef.child("Customer Cylinder");

//display loading screen
document.querySelector(".loading").style.display = "block";

//the dispatch selected from prev page
var selectedRow = localStorage.getItem("selectedRow");
var selectedSO = localStorage.getItem("selectedSO");

//loading of data from DB
const getUnitPrice = (custID, cylType) => {
  return new Promise((resolve, reject) => {
    var unitPrice2 = '';
    
    customerRef.child(custID).child("Item Prices").child(cylType).child("Price").once("value", (data) => {
      unitPrice2 = data.val();
      resolve(unitPrice2);
    })

  })
}

const getUnitCode = (custID, cylType) => {
  return new Promise((resolve, reject) => {
    var unitCode2 = '';
    
    customerRef.child(custID).child("Item Prices").child(cylType).once("value", (data) => {
      data.forEach((data2) => {
        if (data2.key === "code"){
          unitCode2 = data2.val();
        }
      })
      resolve(unitCode2);
    })

  })
}

const getDispatchList = new Promise((resolve, reject) => {
  var dispatchList = [];
  
  dispatchRef.child(selectedRow).child("Cylinders").orderByChild("sorterkey").once("value", (data) => {
    data.forEach((data2) => {
        dispatchList.push([data2.key, data2.val().type]);
    });
    resolve(dispatchList);
  });
});

const getSO = new Promise((resolve, reject) => {
  var SOData = {};
  SORef.child(selectedSO).once("value", (data) => {
    data.forEach((data2) => {
      data2.forEach((data3) => {
        data3.forEach((data4) => {
          SOData.custName = data.val().customerName;
          SOData.address = data.val().custAddress;
          SOData.delAddress = data.val().delAddress;
          SOData.SOPOnum = data.val().PONum;
          SOData.custID = data.val().customerID;
          SOData.custCode = data.val().custCode;
          SOData.PRNum = data.val().PRNum;
          SOData.Terms = data.val().Terms;
          SOData.Date = data.val().createDate;
          SOData.TinNum = data.val().custTINNum;
          if(data3.key == "Others"){
            dispatchList.push([data4.key, "OWNERS CYLINDER NO"]);
          }
        });
      });
    });
    resolve(SOData);
  });
});

const getPrevDelVal = cylinderType => {
  return new Promise((resolve, reject) => {
    var delVal = '';
    SORef.child(selectedSO).child("items").child(cylinderType).once("value", (data) => {
      delVal = data.val().itemDelivered;
      resolve(delVal);
    })
  })
}

//table assignment
async function retrieveSOListAW(){
  dispatchList = await getDispatchList;
  SOData2 = await getSO;
  
  if (SOData2.custID){
    unitPrice = await getUnitPrice(SOData2.custID, dispatchList[0][1]);
    unitCode = await getUnitCode(SOData2.custID, dispatchList[0][1]);
    prevDelVal = await getPrevDelVal(dispatchList[0][1]);
  }

  document.getElementById("CustomerName").value = SOData2.custName ? SOData2.custName : "-";
  document.getElementById("Address").value = SOData2.address ? SOData2.address : "-";
  document.getElementById("DeliveredTo").value = SOData2.delAddress ? SOData2.delAddress : "-";
  document.getElementById("PONum").value = SOData2.SOPOnum ? SOData2.SOPOnum : "-";
  document.getElementById("Terms").value = SOData2.Terms ? SOData2.Terms : "-";
  document.getElementById("PRNum").value = SOData2.PRNum ? SOData2.PRNum : "-";
  document.getElementById("Date").value = datetoday ? datetoday : "-";
  
  if (unitCode < 4){
    unitCode = 0;
  } else {
    unitCode -= 3;
  }
  document.getElementById('ItemDesc').value = unitCode;
  $("#DRnum").focus();
  table = $('#productsTable').DataTable( {
    data: dispatchList,
    ordering: false,
    select: true,
    columns: [
      { title: "Cylinder ID" },
      { title: "Cylinder Type" }
    ]
  } );
  DRQuantity = dispatchList.length;
  document.querySelector(".loading").style.display = "none";
}
retrieveSOListAW();

//create button
document.getElementById("createDeliveryRecieptBtn").addEventListener("click", (event) => {
  event.preventDefault();

  DRRef.child(inputs[0].value).once('value', function(snapshot) {
    var exists = (snapshot.val() !== null);
    if(!exists){
      if (confirm("You are about to create a Delivery Receipt. Would you like to continue?")){
        document.querySelector(".loading2").style.display = "block";
        validate2();
      } else {
        // do something.
      }
    }else{
      window.alert(inputs[0].value+" is existing, cannot create a Delivery Receipt with the same ID.");
    }
  });
});

//create button
document.getElementById("cancelDeliveryRecieptBtn").addEventListener("click", (event) => {
  event.preventDefault();
  if (confirm("You are about to discard this session. Would you like to continue?")){
    window.location = "createDeliveryReceipt.html";
  } else {
    // do something.
  }
});

var itemDesc = document.getElementById("ItemDesc");
var truckPlate = document.getElementById("TruckPlate");
var inputs = document.getElementsByTagName("input");

//create function
async function createDR(){
  
  var DRSet = false;
  var dispatchSet = false;
  var productSet = false;
  var smallLedgerSet = false;
  var bigLedgerSet = false;
  var customerCylSet = false;
  var itemsprocessed = [0,0,0,0,0];
  var cylinderRecordID = '';
  validate();

  dispatchRef.child(selectedRow).update({
    status: "Delivered"
  })
  .then(() => {
    dispatchSet = true;
  });

  DRRef.child(inputs[0].value).set({
    custName: inputs[1].value,
    custID: SOData2.custID,
    custCode: SOData2.custCode,
    custTINNum: SOData2.TinNum,
    address: inputs[2].value,
    DeliveredTo: inputs[3].value,
    Date: inputs[4].value,
    PONum: inputs[5].value,
    RCNum: inputs[6].value,
    Terms: inputs[7].value,
    PRNum: inputs[8].value,
    IssuedAt: inputs[9].value,
    Salesman: inputs[10].value,
    RefDRNum: inputs[11].value,
    On: inputs[13].value,
    PreparedBy: inputs[14].value,
    ReceivedBy: inputs[15].value,
    DeliveredBy: inputs[16].value,
    DriversLicense: inputs[17].value,
    TruckPlate: truckPlate.value,
    itemDesc: itemDesc.options[itemDesc.selectedIndex].value,
    SONum: selectedSO,
    SODate: SOData2.Date,
    cylinderType: dispatchList[0][1],
    unitPrice: unitPrice,
    itemQty: DRQuantity + " cyls",
    status: "Delivered"
  })
  .then((e) => {
    dispatchList.forEach((data) => {
      DRRef.child(inputs[0].value).child("items").child(data[0]).set({
        cylinderType: data[1],
        sorterkey: databaseRef.push().getKey()
      })
      .then((e) => {
        itemsprocessed[0]++;
        if (itemsprocessed[0] === dispatchList.length){
          DRSet = true;
          confirmSet(DRSet, productSet, dispatchSet, bigLedgerSet, smallLedgerSet, customerCylSet);
        }
        
      });
    });
  })
  .catch((e) => {
    console.log(e);
  });
  dispatchList.forEach((data) => {
    productRef.child(data[0]).update({
      cylindercust: inputs[1].value,
      cylinderstatus : "Active"
    })
    .then((e) => {
      itemsprocessed[1]++;
      if (itemsprocessed[1] === dispatchList.length){
        productSet = true;
        confirmSet(DRSet, productSet, dispatchSet, bigLedgerSet, smallLedgerSet, customerCylSet);
      }
    })
    .catch((e) => {
      console.log(e);
    });

    let smdatesplitter = (document.getElementById("Date").value).split(" ");
    let newdatesplit;
    
    if(smdatesplitter[0].indexOf("/") != -1){
      newdatesplit = smdatesplitter[0].split("/");
    }else if(smdatesplitter[0].indexOf("-") != -1){
      newdatesplit = smdatesplitter[0].split("-");
    }

    let newdate = newdatesplit[0]+"-"+newdatesplit[1]+"-"+newdatesplit[2];

    bigledgerRef.child(SOData2.custID).update({
      customerName: SOData2.custName,
      lastUpdate: datetoday
    })
    bigledgerRef.child(SOData2.custID).child("items").child(data[0]).child("Cylinder Record").child(inputs[0].value).child(newdate+" "+smdatesplitter[1]).set({
      drNum: inputs[0].value,
      invNum: '',
      directInvoice: false,
      cylinderType: data[1],
      out: document.getElementById("Date").value,
      in: "ACTIVE"
    })
    .then((e) => {
      itemsprocessed[2]++;
      if (itemsprocessed[2] === dispatchList.length){
        bigLedgerSet = true;
        confirmSet(DRSet, productSet, dispatchSet, bigLedgerSet, smallLedgerSet, customerCylSet);
      }

    })
    .catch((e) => {
      alert(e);
    });

    smallledgerRef.child(data[0]).update({
      customer: SOData2.custName,
      lastUpdate: datetoday,
      lastOut: document.getElementById("Date").value,
      cylinderType: data[1],
      status: "Active",
      directInvoice: false
    })
    .then((e) => {
      smallledgerRef.child(data[0]).child("CylinderRecord").child(newdate+" "+smdatesplitter[1]).set({
        out: document.getElementById("Date").value,
        customer: SOData2.custName,
        customerID: SOData2.custID,
        custCode: SOData2.custCode,
        in: "ACTIVE",
        drNumber: inputs[0].value
      })
      .then((e) => {
        itemsprocessed[3]++;
        if (itemsprocessed[3] === dispatchList.length){
          smallLedgerSet = true;
          confirmSet(DRSet, productSet, dispatchSet, bigLedgerSet, smallLedgerSet, customerCylSet);
        }
      })
    })
    .catch((e) => {
      alert(e);
    })

    custCylindersRef.child(SOData2.custID).child(data[0]).set({
      cylinderType: data[1],
      lastOut: document.getElementById("Date").value
    }).then((e) => {
      itemsprocessed[4]++;
      if (itemsprocessed[4] === dispatchList.length){
        customerCylSet = true;
        confirmSet(DRSet, productSet, dispatchSet, bigLedgerSet, smallLedgerSet, customerCylSet);
      }
    })
  });
  SORef.child(selectedSO).child("items").child(dispatchList[0][1]).update({
    itemDelivered: Number(prevDelVal) + dispatchList.length,
    latestDeliveryDate: inputs[4].value
  })
}

function confirmSet(DRSet, productSet, dispatchSet, bigLedgerSet, smallLedgerSet, customerCylSet){
  if (DRSet && productSet && dispatchSet && bigLedgerSet && smallLedgerSet && customerCylSet){
    document.querySelector(".loading2").style.display = "none";
    window.location = "createDeliveryReceipt.html";
  }
}

//validation
function validate2(){
  var drnum = document.getElementById("DRnum");
  if(drnum.value.length < 1){
    alert("DR No cannot be empty");
    document.querySelector(".loading2").style.display = "none";
  } else {
    createDR();
  }
}

function validate(){
  var inputs = document.getElementsByTagName("input");
  for (var i = 0; i < 19; i++){
    if (i === 12){
      continue;
    } else {
      if (inputs[i].value == ""){
        inputs[i].value = "--";
      }
    }

  }
}
