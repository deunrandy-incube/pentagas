//for PDF valids
var DRList = [];
var forViewing = "true";
var drDataPDF = [];
var itemsLedger = [];

var mainDRArr = [];

//input fields
var customername = document.getElementById("CustomerName");
var dispatchdate = document.getElementById("DispatchDate");
var cylindertype = document.getElementById("CylinderType");
var cylinderquan = document.getElementById("CylinderQty");

var drSearchInput = document.getElementById("drSearchInput");

var pageLoad = 20;

drSearchInput.addEventListener('input', function (evt) {
  if (this.value == '') {
      stateTable(mainDRArr);
  }
});

//get date today
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth() + 1;
var yyyy = today.getFullYear();
if (dd < 10) {
  dd = '0' + dd;
}

if (mm < 10) {
  mm = '0' + mm;
}
today = mm + '/' + dd + '/' + yyyy;

//variables
var reqs_id = 0; //cylinderid index
var currcylinders = []; //currentcylinders in the db

//database references
var databaseRef = firebase.database().ref();
var dispatchRef = databaseRef.child("Dispatch Slip");
var DRRef = databaseRef.child("Delivery Receipt");
var SmallLedgerRef = databaseRef.child("Small Ledger");
var BigLedgerRef = databaseRef.child("Big Ledger");
var productRef = databaseRef.child("Inventory");


var SmallLedgerList = [];
var BigLedgerList = [];

//get date today
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth() + 1;
var yyyy = today.getFullYear();
var hr = today.getHours();
var min = today.getMinutes();
var sec = today.getSeconds();
var SOInfo;

if (dd < 10) {
  dd = '0' + dd;
}

if (mm < 10) {
  mm = '0' + mm;
}
var datetoday = mm + '/' + dd + '/' + yyyy + " " + hr + ":" + min + ":" + sec + " AM";

//loading of data from DB
function getDispatchList() {
  return new Promise((resolve, reject) => {
    var dispatchList = [];
    dispatchRef.orderByChild("status").equalTo("Dispatched").once("value", (data) => {
      data.forEach((data2) => {
        let dateSplitter = data2.val().date.split(" ");
        dispatchList.push([data2.key, data2.val().SONumber, data2.val().cylinderType, dateSplitter[0]]);
      });
      resolve(dispatchList);
    });
  });
}

function getDRList() {
  return new Promise((resolve, reject) => {
    DRList = [];
    DRRef.limitToLast(pageLoad).once("value", (data) => {
      data.forEach((data2) => {
        let drDateSplitter = data2.val().Date.split(" ");
        DRList.push([data2.key, data2.val().custName, data2.val().SONum, drDateSplitter[0]]);
      });
      resolve(DRList);
    });
  });
}

var table = '';
var table2 = '';
var table5 = '';
//table assignment
async function retrieveAllDRData() {
  document.querySelector(".loading").style.display = "block";
  table2.clear();
  const DRList = await getDRList();
  mainDRArr = DRList;
  stateTable(DRList);
}

function retrieveSpecificDR() {
  if (drSearchInput.value) {
    document.querySelector(".loading").style.display = "block";
    // table2.clear();
    pageLoad = 20;
    DRRef.child(drSearchInput.value).once('value', function (data2) {
      var exists = (data2.val() !== null);
      if (exists) {
        let specDRArr = [];
        let drDateSplitter = data2.val().Date.split(" ");
        specDRArr.push([data2.key, data2.val().custName, data2.val().SONum, drDateSplitter[0]]);
        stateTable(specDRArr);
      } else {
        document.querySelector(".loading").style.display = "none";
        window.alert("Delivery Receipt Not Found.");
      }
    });
  } else {
    window.alert("Input is empty, please try again.");
  }
}

function stateTable(tablearr){
  var content = '';
  var paginate = false;
  paginate = tablearr.length > 1 ? true : false;

  if(table2){
    table2.destroy();
  }
  tablearr.forEach((value) => {
    content +='<tr id="' + "++" + value[0] + '">';
    content += '<td>' + value[0] + '</td>';
    content += '<td>' + value[1] + '</td>';
    content += '<td>' + value[2] + '</td>';
    content += '<td>' + value[3] + '</td>';
    content += '</tr>';
  });
  $('#deliveryReceiptTable').append(content);

  table2 = $('#deliveryReceiptTable').DataTable({
    "aaSorting": [],
    "bPaginate": paginate,
    drawCallback: function(){
      $('.paginate_button.next', this.api().table().container())          
         .on('click', async function(){
            document.querySelector(".loading").style.display = "block";
            pageLoad += 20;
            table2.clear();
            DispatchList = await getDRList();
            mainDRArr = getDRList;
            stateTable(mainDRArr);
      });       
   }
  });
  document.querySelector(".loading").style.display = "none";
}

var DeliveryID = '';
$('#deliveryReceiptTable tbody').on('click', 'td', (e) => {
  var selectedRow = e.target.parentNode;
  var selectedRow = e.target.parentNode;
  DeliveryID = selectedRow.children[0].innerText
  if (DeliveryID != "No data available in table") {
    document.querySelector(".loading2").style.display = "block";

    getDRData = new Promise((resolve, reject) => {
      drData = [];
      DRRef.child(DeliveryID).once("value", (data) => {
        let drDateSplitter = data.val().Date.split(" ");
        drData.push(data.key, data.val().custName, data.val().SONum, drDateSplitter[0],
          data.val().cylinderType, data.val().address, data.val().DeliveredBy,
          data.val().DeliveredTo, data.val().DriversLicense, data.val().IssuedAt,
          data.val().On, data.val().PONum, data.val().PRNum, data.val().PreparedBy,
          data.val().RCNum, data.val().ReceivedBy, data.val().RefDRNum, data.val().Salesman,
          data.val().Terms, data.val().TruckPlate, data.val().status, data.val().Date, 
          data.val().custID, data.val().dispatchID);
        resolve(drData);
      });
    });

    getItemList = new Promise((resolve, reject) => {
      itemsList = [];
      DRRef.child(DeliveryID).child("items").orderByChild("sorterkey").once("value", (data) => {
        data.forEach((data2) => {
          itemsList.push([data2.key, data2.val().cylinderType]);
        });
        resolve(itemsList);
      });
    });
    retrieveDRInformation();
  }
});

var custName = '';
var oldDate = '';
var custId = '';
var dispatchId = '';
var salesOrderId = '';

function getSmallLedger() {
  return new Promise((resolve, reject) => {
    for (var x = 0; x < itemsLedger.length; x++) {
      let cylinderLedgerId = itemsLedger[x][0];
      SmallLedgerRef.child(cylinderLedgerId).once("value", (data) => {
        data.forEach((data2) => {
          data2.forEach((data3) => {
            if (data3.val().drNumber == DeliveryID) {
              SmallLedgerList.push([cylinderLedgerId, DeliveryID, data3.val(), data2.val()]);
            }
          });
        });
      });
    }
    resolve(SmallLedgerList);
  });
}

function getBigLedger() {
  var itemsProcessedLedger = 0;
  return new Promise((resolve, reject) => {
    for (var x = 0; x < itemsLedger.length; x++) {
      let cylinderLedgerId = itemsLedger[x][0];
      BigLedgerRef.child(custId).child('items').child(cylinderLedgerId).child('Cylinder Record').child(DeliveryID).once("value", (data) => {
        data.forEach((data2) => {
          BigLedgerList.push([cylinderLedgerId, DeliveryID, data2.key, data2.val()]);
          itemsProcessedLedger++;
          if (itemsProcessedLedger === itemsList.length) {
            resolve(BigLedgerList);
          }
        });
      });
    }
  });
}

async function deleteDR(){
  if (confirm("You are about to delete this Delivery Receipt. Would you like to continue?")) {
    var deletechecker = [false, false];
    var deleteitemsProcessed = [0, 0];
  
    const SmallLedgerList = await getSmallLedger();
    const BigLedgerList = await getBigLedger();
    
    document.querySelector(".loading2").style.display = "block";
    if (document.getElementById("modalDelDate").value == oldDate) {
      let oldsmdatesplitter = (oldDate).split(" ");
      let olddatesplit;
      if (oldsmdatesplitter[0].indexOf("/") != -1) {
        olddatesplit = oldsmdatesplitter[0].split("/");
      } else if (oldsmdatesplitter[0].indexOf("-") != -1) {
        olddatesplit = oldsmdatesplitter[0].split("-");
      }
      let olddateupdated = olddatesplit[0] + "-" + olddatesplit[1] + "-" + olddatesplit[2];
      
      for (var w = 0; w < itemsList.length; w++) {
        for (var x = 0; x < SmallLedgerList.length; x++) {
          if (itemsList[w][0] == SmallLedgerList[x][0]) {
            
            let outsmdatesplitter = (SmallLedgerList[x][2].out).split(" ");
            let outdatesplit;
            if (outsmdatesplitter[0].indexOf("/") != -1) {
              outdatesplit = outsmdatesplitter[0].split("/");
            } else if (outsmdatesplitter[0].indexOf("-") != -1) {
              outdatesplit = outsmdatesplitter[0].split("-");
            }
            let outdateupdated = outdatesplit[0] + "-" + outdatesplit[1] + "-" + outdatesplit[2];
  
            if (olddateupdated + " " + oldsmdatesplitter[1] == outdateupdated + " " + outsmdatesplitter[1]) {
              if(Object.keys(SmallLedgerList[x][3]).length==1){
                SmallLedgerRef.child(SmallLedgerList[x][0]).remove().then(() =>{
                  deleteitemsProcessed[0]++;
                  if (deleteitemsProcessed[0] === itemsList.length) {
                    deletechecker[0] = true;
                    checkerFunc(deletechecker, "delete");
                  }
                })
              }else{
                SmallLedgerRef.child(SmallLedgerList[x][0]).update({
                  lastOut: "",
                  status: "Production"
                })
                SmallLedgerRef.child(SmallLedgerList[x][0]).child('CylinderRecord').child(olddateupdated + " " + oldsmdatesplitter[1]).remove()
                  .then(() => {
                    deleteitemsProcessed[0]++;
                    if (deleteitemsProcessed[0] === itemsList.length) {
                      deletechecker[0] = true;
                      checkerFunc(deletechecker, "delete");
                    }
                  })
              }
            }
          }
        }
  
        for (var y = 0; y < BigLedgerList.length; y++) {
          if (itemsList[w][0] == BigLedgerList[y][0]) {
            let outsmdatesplitter = (BigLedgerList[y][3].out).split(" ");
            let outdatesplit;
            if (outsmdatesplitter[0].indexOf("/") != -1) {
              outdatesplit = outsmdatesplitter[0].split("/");
            } else if (outsmdatesplitter[0].indexOf("-") != -1) {
              outdatesplit = outsmdatesplitter[0].split("-");
            }
            let outdateupdated = outdatesplit[0] + "-" + outdatesplit[1] + "-" + outdatesplit[2];
  
            if (olddateupdated + " " + oldsmdatesplitter[1] == outdateupdated + " " + outsmdatesplitter[1]) {
              BigLedgerRef.child(custId).child('items').child(BigLedgerList[y][0]).child('Cylinder Record').child(DeliveryID).remove()
                 .then(() => {
                  deleteitemsProcessed[1]++;
                  if (deleteitemsProcessed[1] === itemsList.length) {
                    deletechecker[1] = true;
                    checkerFunc(deletechecker, "delete");
                  }
                })
            }
          }
        }
      }
    }else{
      window.alert("You cannot delete this DR if you adjusted the Date. Please re-open DR window and use default date instead.")
    }
  }
}

async function editDate() {
  var checker = [false, false];
  var itemsProcessed = [0, 0];

  const SmallLedgerList = await getSmallLedger();
  const BigLedgerList = await getBigLedger();

  document.querySelector(".loading2").style.display = "block";
  DRRef.child(DeliveryID).update({
    Date: document.getElementById("modalDelDate").value,
  }).then((e) => {
    if (document.getElementById("modalDelDate").value != oldDate) {
      let smdatesplitter = (document.getElementById("modalDelDate").value).split(" ");
      let newdatesplit;
      if (smdatesplitter[0].indexOf("/") != -1) {
        newdatesplit = smdatesplitter[0].split("/");
      } else if (smdatesplitter[0].indexOf("-") != -1) {
        newdatesplit = smdatesplitter[0].split("-");
      }
      let newdate = newdatesplit[0] + "-" + newdatesplit[1] + "-" + newdatesplit[2];

      let oldsmdatesplitter = (oldDate).split(" ");
      let olddatesplit;
      if (oldsmdatesplitter[0].indexOf("/") != -1) {
        olddatesplit = oldsmdatesplitter[0].split("/");
      } else if (oldsmdatesplitter[0].indexOf("-") != -1) {
        olddatesplit = oldsmdatesplitter[0].split("-");
      }
      let olddateupdated = olddatesplit[0] + "-" + olddatesplit[1] + "-" + olddatesplit[2];

      for (var w = 0; w < itemsList.length; w++) {
        for (var x = 0; x < SmallLedgerList.length; x++) {
          if (itemsList[w][0] == SmallLedgerList[x][0]) {
            let outsmdatesplitter = (SmallLedgerList[x][2].out).split(" ");
            let outdatesplit;
            if (outsmdatesplitter[0].indexOf("/") != -1) {
              outdatesplit = outsmdatesplitter[0].split("/");
            } else if (outsmdatesplitter[0].indexOf("-") != -1) {
              outdatesplit = outsmdatesplitter[0].split("-");
            }
            let outdateupdated = outdatesplit[0] + "-" + outdatesplit[1] + "-" + outdatesplit[2];

            if (olddateupdated + " " + oldsmdatesplitter[1] == outdateupdated + " " + outsmdatesplitter[1]) {
              SmallLedgerRef.child(SmallLedgerList[x][0]).update({
                lastUpdate: datetoday,
                lastOut: document.getElementById("modalDelDate").value,
              })
              SmallLedgerRef.child(SmallLedgerList[x][0]).child("CylinderRecord").child(newdate + " " + smdatesplitter[1]).set({
                out: document.getElementById("modalDelDate").value,
                customer: SmallLedgerList[x][2].customer,
                customerID: SmallLedgerList[x][2].customerID,
                custCode: SmallLedgerList[x][2].custCode,
                in: SmallLedgerList[x][2].in,
                invoiceNum: SmallLedgerList[x][2].invoiceNum == undefined ? "" : SmallLedgerList[x][2].invoiceNum,
                drNumber: SmallLedgerList[x][2].drNumber
              })
              SmallLedgerRef.child(SmallLedgerList[x][0]).child('CylinderRecord').child(olddateupdated + " " + oldsmdatesplitter[1]).remove()
                .then(() => {
                  itemsProcessed[0]++;
                  if (itemsProcessed[0] === itemsList.length) {
                    checker[0] = true;
                    checkerFunc(checker, "edit");
                  }
                })
            }
          }
        }

        for (var y = 0; y < BigLedgerList.length; y++) {
          if (itemsList[w][0] == BigLedgerList[y][0]) {

            let outsmdatesplitter = (BigLedgerList[y][3].out).split(" ");
            let outdatesplit;
            if (outsmdatesplitter[0].indexOf("/") != -1) {
              outdatesplit = outsmdatesplitter[0].split("/");
            } else if (outsmdatesplitter[0].indexOf("-") != -1) {
              outdatesplit = outsmdatesplitter[0].split("-");
            }
            let outdateupdated = outdatesplit[0] + "-" + outdatesplit[1] + "-" + outdatesplit[2];

            if (olddateupdated + " " + oldsmdatesplitter[1] == outdateupdated + " " + outsmdatesplitter[1]) {
              BigLedgerRef.child(custId).update({
                customerName: custName,
                lastUpdate: datetoday
              })
              BigLedgerRef.child(custId).child("items").child(BigLedgerList[y][0]).child("Cylinder Record").child(DeliveryID).child(newdate + " " + smdatesplitter[1]).set({
                drNum: BigLedgerList[y][3].drNum,
                invNum: BigLedgerList[y][3].invNum == undefined ? "" : BigLedgerList[y][3].invNum,
                directInvoice: BigLedgerList[y][3].directInvoice,
                cylinderType: BigLedgerList[y][3].cylinderType,
                out: document.getElementById("modalDelDate").value,
                in: BigLedgerList[y][3].in
              })
              BigLedgerRef.child(custId).child('items').child(BigLedgerList[y][0]).child('Cylinder Record').child(DeliveryID).child(olddateupdated + " " + oldsmdatesplitter[1]).remove()
                .then(() => {
                  itemsProcessed[1]++;
                  if (itemsProcessed[1] === itemsList.length) {
                    checker[1] = true;
                    checkerFunc(checker, "edit");
                  }
                })
            }
          }
        }
      }
    }
  });
}

function checkerFunc(checker,from) {
  if (checker[0] && checker[1]) {
    if(from==="edit"){
      window.location = "createDeliveryReceipt.html";
    }else if(from==="delete"){
      removeDRfromDB();
    }
  }
}

function removeDRfromDB(){
  var inventoryprocess = 0;
  var inventorychecker = false;
  DRRef.child(DeliveryID).remove().then(() => {
    itemsList.forEach((data2) => {
      productRef.child(data2[0]).update({
        cylinderstatus : "Production"
      })
      .then((e) => {
        inventoryprocess++;
        if (inventoryprocess === itemsList.length){
          inventorychecker = true;
          inventoryCheckerFunc(inventorychecker);
        }
      });
    });
  });
}

function inventoryCheckerFunc(checker){
  if(checker){
    BigLedgerRef.child(custId).child('items').once("value").then(function(snapshot){
      if(snapshot.exists()){
        window.alert("DR deleted. Please notify Web Admin of the SO ID ("+salesOrderId+"), DR cylinder count ("+itemsList.length+"), the DR ID ("+ DeliveryID +") and Dispatch ID to update status of Dispatcher Slip back to 'Dispatched' and to update delivery count in the Sales Order")
        window.location = "createDeliveryReceipt.html"
      }else{
        BigLedgerRef.child(custId).remove().then(() => {
          window.alert("DR deleted. Please notify Web Admin of the SO ID ("+salesOrderId+"), DR cylinder count ("+itemsList.length+"), the DR ID ("+ DeliveryID +") and Dispatch ID to update status of Dispatcher Slip back to 'Dispatched' and to update delivery count in the Sales Order")
          window.location = "createDeliveryReceipt.html"
        })
      }
    });
  }
}


async function retrieveDRInformation() {
  const deliveryData = await getDRData;
  drDataPDF = deliveryData;
  custName = deliveryData[1];
  oldDate = deliveryData[21];
  custId = deliveryData[22];
  dispatchId = deliveryData[23];
  salesOrderId = deliveryData[2];

  document.getElementById("modalCustName").textContent = deliveryData[1];
  document.getElementById("modalDelDate").value = deliveryData[21];
  document.getElementById("modalCustAddress").textContent = deliveryData[5];
  document.getElementById("modalDeliveredBy").textContent = deliveryData[6];
  document.getElementById("modalDelTo").textContent = deliveryData[7];
  document.getElementById("modalDriversLicense").textContent = deliveryData[8];
  document.getElementById("modalIssuedAt").textContent = deliveryData[9];
  document.getElementById("modalON").textContent = deliveryData[10];
  document.getElementById("modalPONumber").textContent = deliveryData[11];
  document.getElementById("modalPRNumber").textContent = deliveryData[12];
  document.getElementById("modalPreparedBy").textContent = deliveryData[13];
  document.getElementById("modalRCNumber").textContent = deliveryData[14];
  document.getElementById("modalRecievedBy").textContent = deliveryData[15];
  document.getElementById("modalRefDRNumber").textContent = deliveryData[16];
  document.getElementById("modalSalesman").textContent = deliveryData[17];
  document.getElementById("modalTerms").textContent = deliveryData[18];
  document.getElementById("modalTruckPlate").textContent = deliveryData[19];
  document.getElementById("modalDispatchID").textContent = deliveryData[23];

  document.querySelector(".modal-title").textContent = DeliveryID;
  const itemsList = await getItemList;
  itemsLedger = itemsList;
  if(table5){
    table5.destroy();
  }
  table5 = $('#itemsTable').DataTable({
    data: itemsList,
    select: true,
    columns: [{
        title: "Cylinder ID"
      },
      {
        title: "Quantity Type"
      }
    ]
  });

  document.querySelector(".loading2").style.display = "none";
  $('#DispModal').modal({
    backdrop: 'static',
    keyboard: false
  });
  $('#DispModal').modal("show");
}

//table click listener
$('#dispatchList tbody').on('click', 'td', (e) => {
  var selectedRow = e.target.parentNode;
  localStorage.setItem("selectedRow", selectedRow.children[0].innerText);
  localStorage.setItem("selectedSO", selectedRow.children[1].innerText);

  if (selectedRow.children[0].innerText == "No data available in table") {

  } else if (selectedRow.children[0].innerText == "Empty" || selectedRow.children[0].innerText == "Defective") {
    if (confirm("Create Delivery Receipt for " + selectedRow.children[0].innerText + "?")) {
      window.location = "createDefectDR.html";
    }
  } else {
    if (confirm("Create Delivery Receipt for " + selectedRow.children[0].innerText + "?")) {
      window.location = "createDeliveryReceipt2.html";
    }
  }
});

async function openModal() {
  document.querySelector(".loading").style.display = "block";
  const dispatchList = await getDispatchList();
  if (table) {
    table.destroy();
  }
  table = $('#dispatchList').DataTable({
    data: dispatchList,
    select: true,
    columns: [{
        title: "Dispatch ID"
      },
      {
        title: "Sales Order No."
      },
      {
        title: "Cylinder Type"
      },
      {
        title: "Dispatch Date"
      }
    ]
  });

  document.querySelector(".loading").style.display = "none";

  $('#myModal').modal({
    backdrop: 'static',
    keyboard: false
  });
  $('#myModal').modal("show");
}

function clearTables() {
  table5.destroy();
}