var databaseRef = firebase.database().ref();
var customerRef = databaseRef.child("Customer");
var productRef = databaseRef.child("Inventory");
var SORef = databaseRef.child("Sales Order");
var PORef = databaseRef.child("Purchase Order");
var table = '';
var table2 = '';
var forViewing = "false";

//input fields
var SONumberInput = document.getElementById("SONumber");
var PONumberInput = document.getElementById("PONumber");
var addressInput = document.getElementById("Address");
var custNameInput = document.getElementById("CustName");
var orderedByInput = document.getElementById("Order");
var productsSelected = [];
var productList = [];
var productsQty = [];
var productsPrice = [];
var otherInput = document.getElementById("OtherProduct");
var CustTypeInput = document.getElementById("CustType");
var CustTINNum = document.getElementById("CustTINNum");
var CustCodeInput = document.getElementById("CustCode");
var PRNumInput = document.getElementById("PRNum");
var dateInput = document.getElementById("DatePicker");
var delAddressInput = document.getElementById("DelAddress");
var SIDRNumInput = document.getElementById("SIDRNum");
var TermsInput = document.getElementById("Terms");
var remarksInput = document.getElementById("Remarks");
var SOTypeInput = document.getElementById("SOType");
var preparedbyInput = document.getElementById("PrepBy");
// var recievedbyInput = document.getElementById("RecBy");

//value hldr
var SIDRHldr = '';
var remarksHldr = '';
var otherItemCtr = 0;
var isAnnex = "false";

//checker
var itemChckr = false;
var itemChckr2 = false;
var otherItemChckr = false;
var soChckr = false;
var soChckr2 = false;
var custChckr = false;
var custChckr2 = false;
var lastidindb = 0;
var itemIDs = [];
var itemTypes = [];

var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1;
var yyyy = today.getFullYear();
var hr = today.getHours();
var min = today.getMinutes();
var sec = today.getSeconds();

if(dd<10)
{
  dd='0'+dd;
}

if(mm<10)
{
  mm='0'+mm;
}
var datetoday = mm+'-'+dd+'-'+yyyy+" "+hr+":"+min+":"+sec;
document.getElementById("DatePicker").value = datetoday;

productList = document.getElementsByClassName("form-check-input");
productsQty = document.getElementsByClassName("Qty");
productsPrice = document.getElementsByClassName("Price");

$('#PONumber').focus();

//RETRIEVE IDs from Database
const getItemIDs = new Promise((resolve, reject) => {
  SORef.once("value", (data) => {
    data.forEach((data2) => {
      var keyhldr = data2.key.split("-");
      itemIDs.push(parseInt(keyhldr[1]));
    });
    resolve(itemIDs);
  });
});

const getCustomers = new Promise((resolve, reject) => {
  var customers = [];
  customerRef.once("value", (data) => {
    data.forEach((data2) => {
      customers.push(data2.val().Name);
    });
    resolve(customers);
  });
});

function getProducts(){
  document.querySelector(".loading").style.display = "block";
  return new Promise((resolve, reject) => {
    var products = [];
    productRef.once("value", (data) => {
      data.forEach((data2) => {
        products.push([data2.key, data2.val().cylindertype, data2.val().cylinderstatus]);
      });
      resolve(products);
    });
  
  })
} 

//display loading screen
document.querySelector(".loading").style.display = "block";


async function fillInventoryData(){
  const products = await getProducts();
  var productArr = [];
  var productQty = [0,0,0,0,0,0,0,0,0,0,0];
  products.forEach((data) => {
    if (data[2] === "Production"){
      productArr.push([data[0],data[1]]);
      switch(data[1]) {
        case "MED 02":
            productQty[0] += 1;
            break;
        case "MED 02 FLASK":
            productQty[1] += 1;
            break;
        case "MED 02 BANTAM":
            productQty[2] += 1;
            break;
        case "MED 02 MEDIUM":
            productQty[3] += 1;
            break;
        case "TECH 02":
            productQty[4] += 1;
            break;
        case "ARGON":
            productQty[5] += 1;
            break;
        case "COMP AIR":
            productQty[6] += 1;
            break;
        case "ACETYLENE":
            productQty[7] += 1;
            break;
        case "NITROGEN":
            productQty[8] += 1;
            break;
        case "NITROUS DIOXIDE":
            productQty[9] += 1;
            break;
        case "CARBON DIOXIDE":
            productQty[10] += 1;
            break;
      }
    }
  });
  for(var i = 0; i<productQty.length; i++){
    document.getElementById("value-" + i).textContent = productQty[i];
  }
  document.querySelector(".loading").style.display = "none";
}

//function that waits for data before execution
async function retrieveCustAW(){
  //for sales order id
  itemIDs = await getItemIDs;
  //IF THERE ARE NO IDS FOUND IN DATABASE. INITIALIZE
  if(itemIDs.length == 0){
    SONumberInput.value = "SO-00001";
  //INCREMENT PUSHED ID BY 1. USE INCREMENTED ID FOR PUSHING IN DB LATER ON
  }else{
    lastidindb = Math.max.apply(Math,itemIDs);
    SONumberInput.value = "SO-0000"+(lastidindb+1);
  }

  //for customer name
  const customers = await getCustomers;
  var options = '';
  for(var i = 0; i < customers.length; i++)
    options += '<option value="'+customers[i]+'" />';
  document.getElementById('Customers').innerHTML = options;
  //for products table
  //to hide loading screen
  document.querySelector(".loading").style.display = "none ";
}
//async function call
retrieveCustAW();

var customer = {
  id: ""
};

//input event for customer name
$('#CustName').bind("input", () => {
  var val = document.getElementById("CustName").value;
  var options = document.getElementById("Customers").childNodes;
  for (var i = 0; i < options.length; i++){
    if (options[i].value == val){
      fillData(val);
    } else {
      document.getElementById("Address").value = "";
      document.getElementById("DelAddress").value = "";
      document.getElementById("Order").value = "";
      customers.id = "";
      customers.Name = "";
      customers.Address = "";
      customers.OrderedBy = "";
      customers.DelAddress = "";
    }
  }
});


var getCustData = '';
var customers = {};
//function for autofill
function fillData(custName){
  document.querySelector(".loading2").style.display = "block";
  getCustData = new Promise((resolve, reject) => {
    customerRef.once("value", (data) => {
      data.forEach((data2) => {
        if (data2.val().Name === custName){
          customers.id = data2.key;
          customers.Name = data2.val().Name;
          customers.Address = data2.val().Address;
          customers.OrderedBy = data2.val().OrderedBy;
          customers.DelAddress = data2.val().DelAddress;
          customers.CustCode = data2.val().CustCode;
          customers.CustType = data2.val().CustType;
          customers.TinNum = data2.val().custTINNum;
        }
      });
      resolve(customers);
    });
  });
  retrieveCustDataAW();
}

const getItemPrices = customerID => {
  return new Promise((resolve, reject) => {
    var itemPrices = [];
    customerRef.child(customerID).child("Item Prices").once("value", (data) => {
      data.forEach((data2) => {
        itemPrices.push([data2.key, data2.val().code, data2.val().Price]);
      });
      resolve(itemPrices);
    })
  })
}

var itemPrices = [];

async function retrieveCustDataAW(){
  customer = await getCustData;
  itemPrices = await getItemPrices(customer.id);
  itemPrices.forEach((data) => {
    productsPrice[data[1]].value = data[2];
  })
  document.getElementById("Address").value = customer.Address;
  document.getElementById("DelAddress").value = customer.DelAddress;
  document.getElementById("Order").value = customer.OrderedBy;
  CustCodeInput.value = customer.CustCode;
  CustTypeInput.value = customer.CustType;
  CustTINNum.value = customers.TinNum
  document.querySelector(".loading2").style.display = "none";
}

//save button
document.getElementById("createSOBtn").addEventListener("click", (event) => {
  event.preventDefault();
  if (confirm("You are about to create a Sales Order. Would you like to continue?")){
    createSO();
  } else {
    // do something.
  }
});

//cancel button
document.getElementById("cancelSOBtn").addEventListener("click", (event) => {
  event.preventDefault();
  if (confirm("You are about to discard this session. Would you like to continue?")){
    window.location = "../index.html";
  } else {
    // do something.
  }
});

//click event for sales order annex
$('#annexCheck').bind("click", () => {
  if (productList[11].checked){
    isAnnex = "true";
    dateInput.value = "Staggered Delivery";
    dateInput.setAttribute("disabled", "disabled");
  } else {
    isAnnex = "false";
    dateInput.value = datetoday;
    dateInput.removeAttribute("disabled");
  }
});


//SALES ORDER
function createSO(){
  document.querySelector(".loading2").style.display = "block";

  for (var i = 0; i < productList.length - 1 ; i++){
    if (productList[i].checked){
      productsSelected.push([productList[i].value, productsQty[i].value, productsPrice[i].value, i]);
    }
  }

  if (customer.id.length == 0){
    customer.id = customerRef.push().getKey();
    customerRef.child(customer.id).set({
      Address: addressInput.value,
      Company: "N/A",
      Name: custNameInput.value,
      OrderedBy: orderedByInput.value,
      DelAddress: delAddressInput.value,
      CustType: CustTypeInput.value,
      CustCode: CustCodeInput.value,
      custTINNum: CustTINNum.value,
      Status: "Active"
    })
    .then(() => {
      customerRef.child(customer.id).child("PurchaseOrders").child(PONumberInput.value).set({
        ID: PONumberInput.value,
        SO: SONumberInput.value
      })
      .then(() => {
        custChckr = true;
        checker(itemChckr, itemChckr2, otherItemChckr, custChckr, soChckr);
      });
    });
  } else {
    customerRef.child(customer.id).update({
      Address: addressInput.value,
      Name: custNameInput.value,
      OrderedBy: orderedByInput.value,
      DelAddress: delAddressInput.value,
      CustType: CustTypeInput.value,
      CustCode: CustCodeInput.value,
      custTINNum: CustTINNum.value
    })
    .then(() => {
      customerRef.child(customer.id).child("PurchaseOrders").child(PONumberInput.value).set({
        ID: PONumberInput.value,
        SO: SONumberInput.value
      })
      .then(() => {
        custChckr = true;
        checker(itemChckr, itemChckr2, otherItemChckr, custChckr, soChckr);
      });
    });
  }

  SORef.child(SONumberInput.value).set({
    createDate: datetoday,
    PONum: PONumberInput.value,
    customerName: custNameInput.value,
    customerID: customer.id,
    custAddress: addressInput.value,
    custCode: CustCodeInput.value,
    PRNum: PRNumInput.value,
    delDate: dateInput.value,
    delAddress: delAddressInput.value,
    SIDRNum: SIDRNumInput.value,
    custTINNum: CustTINNum.value,
    Terms: TermsInput.value,
    remarks: remarksInput.value,
    SOType: SOTypeInput.value,
    prepBy: preparedbyInput.value,
    // recBy: recievedbyInput.value,
    SOAnnexStatus: isAnnex,
    status: "Pending"
  })
  .then(() => {
    if (productList[8].checked){
      SORef.child(SONumberInput.value).update({
        SOAnnextStatus: "Y"
      })
      .then(() => {
        soChckr = true;
        checker(itemChckr, itemChckr2, otherItemChckr, custChckr, soChckr);
      });
    } else {
      SORef.child(SONumberInput.value).update({
        SOAnnextStatus: "N"
      })
      .then(() => {
        soChckr = true;
        checker(itemChckr, itemChckr2, otherItemChckr, custChckr, soChckr);
      });
    }

  });


  var itemsProcessed = [0, 0];
  productsSelected.forEach((data2) => {
    SORef.child(SONumberInput.value).child("items").child(data2[0]).set({
      itemQty: data2[1],
      itemPrice: data2[2],
      itemDelivered: "0",
      itemDispatched: "0",
      latestDeliveryDate: "--",
      isFullyDispatched: false,
      isFullyInvoiced: false
    })
    .then(() => {
      itemsProcessed[0]++;
      if (itemsProcessed[0] = productsSelected.length){
        itemChckr = true;
        checker(itemChckr, itemChckr2, otherItemChckr, custChckr, soChckr);
      }
    });
    customerRef.child(customer.id).child("DeliveryRecord").child(datetoday).child(SONumberInput.value).child(data2[0]).set({
      itemQty: data2[1],
      itemPrice: data2[2]
    })
    .then(() => {
      customerRef.child(customer.id).child("Item Prices").child(data2[0]).update({
        code: data2[3],
        Price: data2[2]
      })
      .then(() => {
        itemsProcessed[1]++;
        if (itemsProcessed[1] = productsSelected.length){
          itemChckr2 = true;
          checker(itemChckr, itemChckr2, otherItemChckr, custChckr, soChckr);
        }
      })
    });
  });
  if (otherItemCtr === 0) {
    otherItemChckr = true;
    checker(itemChckr, itemChckr2, otherItemChckr, custChckr, soChckr);
  } else {
    for (var i = 0; i < otherItemCtr; i++){
      SORef.child(SONumberInput.value).child("items").child("Others").child(document.getElementById("itemName-" + i).value).set({
        itemQty: document.getElementById("itemQty-" + i).value,
        itemPrice: document.getElementById("itemPrice-" + i).value,
        itemDelivered: "0",
        itemDispatched: "0"
      })
      .then(() => {
        if ((i+1) == otherItemCtr){
          otherItemChckr = true;
          checker(itemChckr, itemChckr2, otherItemChckr, custChckr, soChckr);
        }
      });
    }
  }
}

function addItem(){
  var currentChild = document.getElementById("otherItemDiv");
  var openingDiv = "<div id='item-"+ otherItemCtr + "' class='form-group' style='max-height:300px;'>";
  var itemNameInput = "<input id='itemName-" + otherItemCtr + "' type='text' placeholder='Item Name' style='width:100px'>";
  var itemQtyInput = "<input id='itemQty-" + otherItemCtr + "' type='number' style='width:50px' placeholder='Qty'>";
  var itemPriceInput = "<input id='itemPrice-" + otherItemCtr + "' type='number' style='width:70px' placeholder='Price'>";
  var itemRemoveBtn = "<button id='close-"+otherItemCtr+"' onclick='removeItem(this, event)' class='btn btn-warning btn-fab btn-round btn-sm'><i class='material-icons'>close</i></button>";
  var closingDiv = "</div>";
  currentChild.innerHTML+= openingDiv+itemNameInput+itemQtyInput+itemPriceInput+itemRemoveBtn+closingDiv;
  otherItemCtr++;
}

function removeItem(object, e){
  e.preventDefault();
  var id = object.id.split("-");
  $("#item-" + id[1]).remove();
  otherItemCtr--;
}

function checker(itemChckr, itemChckr2, otherItemChckr, custChckr, soChckr) {
  if (itemChckr && itemChckr2 && otherItemChckr && custChckr && soChckr){
    window.location = "../index.html";
    document.querySelector(".loading2").style.display = "none";
  }
}
