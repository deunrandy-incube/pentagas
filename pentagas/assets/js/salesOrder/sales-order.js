//firebase variables
var databaseRef = firebase.database().ref();
var salesOrderRef = databaseRef.child("Sales Order");
var dispatchRef = databaseRef.child("Dispatch Slip");
var productRef = databaseRef.child("Inventory");
var DRRef = databaseRef.child("Delivery Receipt");
var SIRef = databaseRef.child("Sales Invoice");
var customerRef = databaseRef.child("Customer");

//table variables
var table = '';
var table2 = '';
var table3 = '';
var table4 = '';
var table5 = '';

//pdf variables
var sonumpdf;
var ponumpdf;
var custnamepdf;
var custaddpdf;
var deldatepdf;
var deladdresspdf;
var sidrpdf;
var remarkspdf;
var createdatepdf;
var prepbypdf;
// var recbypdf;
var cylindertypes = [];
var cylinderprices = [];
var cylinderqtys = 0;

var annexitems = [];
var annextotaldr = 0;
var annextotalsi = 0;
var totalqtyannex = 0;

var forViewing = "true";
var deliveryStatus = '';

var customerInput = document.getElementById("customerInput");

//display loading screen
document.querySelector(".loading").style.display = "block";

const getCustomers = new Promise((resolve, reject) => {
  var customers = [];
  customerRef.once("value", (data) => {
    data.forEach((data2) => {
      customers.push(data2.val().Name);
    });
    resolve(customers);
  });
  let initialDispArr = [];
  
  table = $('#SOTable').DataTable( {
    data: initialDispArr,
    select: true,
    columns: [
      { title: "Sales Order No." },
      { title: "Customer Name" },
      { title: "Purchase Order No." },
      { title: "Status" },
      { title: "Delivery Date" }
    ],
    language: {
      "search": "Search Current Data in Table:",
      "emptyTable": "Select a Customer to load SO Data"
  },
} );
});

function getSalesOrderList() {
  document.querySelector(".loading").style.display = "block";
  return new Promise((resolve, reject) =>{
    var salesOrder = [];
    if(customerInput.value != 'All'){
      salesOrderRef
      .orderByChild('customerName')
      .equalTo(customerInput.value)
      .once('value', data => {
        if(data.exists()){
          data.forEach((data2) => {
            let createDateSplitter = data2.val().createDate.split(" ");
            let deliveryDateSplitter = '';
            let deliveryDateHldr = '';
      
            if(data2.val().delDate != "Staggered Delivery"){
              deliveryDateSplitter = data2.val().delDate.split(" ");
              deliveryDateHldr = deliveryDateSplitter[0];
            }else{
              deliveryDateHldr = "Staggered Delivery";
            }
      
            salesOrder.push([data2.key, data2.val().customerName, data2.val().PONum,
                             data2.val().status, deliveryDateHldr]);
          });
          resolve(salesOrder);
        }else{
          document.querySelector(".loading").style.display = "none";
        }
      });
    }else{
      salesOrderRef
      .once('value', data => {
        if(data.exists()){
          data.forEach((data2) => {
            let createDateSplitter = data2.val().createDate.split(" ");
            let deliveryDateSplitter = '';
            let deliveryDateHldr = '';
      
            if(data2.val().delDate != "Staggered Delivery"){
              deliveryDateSplitter = data2.val().delDate.split(" ");
              deliveryDateHldr = deliveryDateSplitter[0];
            }else{
              deliveryDateHldr = "Staggered Delivery";
            }
      
            salesOrder.push([data2.key, data2.val().customerName, data2.val().PONum,
                             data2.val().status, deliveryDateHldr]);
          });
          resolve(salesOrder);
        }else{
          document.querySelector(".loading").style.display = "none";
        }
      });
    }
  });
} 


function getDispatchList(){
    return new Promise((resolve, reject) => {
    var dispatches = [];
    dispatchRef.orderByChild("SONumber").equalTo(SOID).limitToFirst(10).once("value", (data) => {
      data.forEach((data2) => {
        dispatches.push([data2.key, data2.val().date]);
      })
      resolve(dispatches);
    })
  });
}

function getItemList() {
  return new Promise((resolve, reject) => {
    var items = [];
    var hldr = {};
    DRRef.orderByChild("SONum").equalTo(SOID).once("value", (data) => {
      data.forEach((data2) => {
        hldr = data2.val().items;
        for (var key in hldr){
          if (hldr.hasOwnProperty(key)){
            items.push([key, data2.val().cylinderType]);
          }
        }
      })
    })
    .then(() => {
      SIRef.orderByChild("SONum").equalTo(SOID).once("value", (data) => {
        data.forEach((data2) => {
          for (let key in data2.val().items){
            if (data2.val().items[key].CylinderID){
              items.push([key, data2.val().items[key].CylinderType]);
            }
          }
        })
        resolve(items);
      })
    })
  });
  
} 
//display loading screen
document.querySelector(".loading").style.display = "block";

var salesOrders = '';
//function that waits for data before execution
async function retrieveSOAW(){
  const CustomerList = await getCustomers;
  for(var x = 0; x< CustomerList.length; x++){
    customerInput.innerHTML += "<option value=\"" + CustomerList[x] + "\">" + CustomerList[x] + "</option>";
  }
  document.querySelector(".loading").style.display = "none";
}

async function refreshSOTable(){
  const salesOrder = await getSalesOrderList();
  mainDispArr = salesOrder;
  stateTable(salesOrder);
  document.querySelector(".loading").style.display = "none";
}

function stateTable(tablearr){
  table.destroy();
  table = $('#SOTable').DataTable( {
    data: tablearr,
    select: true,
    columns: [
      { title: "Sales Order No." },
      { title: "Customer Name" },
      { title: "Purchase Order No." },
      { title: "Status" },
      { title: "Delivery Date" }
    ],
    language: {
      "emptyTable": "Select a Customer to load data."
    }
  } );
  document.querySelector(".loading").style.display = "none";
}

//async function call
retrieveSOAW();
var SOID = '';
$('#SOTable tbody').on('click', 'td', (e) => {
  var selectedRow = e.target.parentNode;
  SOID = selectedRow.children[0].innerText
  if (SOID != "No data available in table"){
    document.querySelector(".loading2").style.display = "block";
    
    getSpecSOData = new Promise((resolve, reject) => {
      var soData = [];
      salesOrderRef.child(SOID).once("value", (data2) => {
        let createDateSplitter = data2.val().createDate.split(" ");
        let deliveryDateSplitter = '';
        let deliveryDateHldr = '';
  
        if(data2.val().delDate != "Staggered Delivery"){
          deliveryDateSplitter = data2.val().delDate.split(" ");
          deliveryDateHldr = deliveryDateSplitter[0];
        }else{
          deliveryDateHldr = "Staggered Delivery";
        }

        let annexdata = data2.val().items;
        for (const key in annexdata) {
          for (const key2 in annexdata[key]){
            if(key2 == "itemQty"){
              totalqtyannex += parseInt(annexdata[key][key2]);
            }
          }
      }
      
        soData.push(data2.key, data2.val().customerName, data2.val().PONum,
          data2.val().status, deliveryDateHldr, data2.val().customerID,
          data2.val().custAddress, data2.val().delAddress, data2.val().SIDRNum,
          data2.val().remarks, createDateSplitter[0], data2.val().items,
          data2.val().prepBy, data2.val().SOAnnexStatus);
        resolve(soData);
      });
    });
       
    getDRList =  new Promise((resolve, reject) => {
      var items = [];
      annexitems = [];
      DRRef.orderByChild("SONum").equalTo(SOID).once("value", (data) => {
        data.forEach((data2) => {
          items.push([data2.key, data2.val().Date]);
            if(data2.val().status == "Delivered" || data2.val().status == "Invoiced"){
                var cylsplitter  = data2.val().itemQty.split(" ");    //Split Cylinder
                annextotaldr = data2.val().itemQty.split(" ")[0] * data2.val().unitPrice;
                
                cylinderqtys += parseInt(cylsplitter[0]);
  
                if(data2.val().Format != "Direct Invoice"){
                  annexitems.push([data2.key, data2.val().SONum, data2.val().Date, cylsplitter[0], data2.val().SODate, annextotaldr]);
                }
                
            }
        })
        resolve(items);
      })
    });

    getSIList = new Promise((resolve, reject) => {
      var items = [];
      annexitems = [];
      SIRef.orderByChild("SONum").equalTo(SOID).once("value", (data) => {
        data.forEach((data2) => {
          items.push([data2.key, data2.val().Date]);
          let itemarray = [];
          let itemcount;
          annextotalsi = 0;
          itemarray = Object.values(data2.val().items);
          itemcount = Object.keys(data2.val().items).length;

          cylinderqtys += parseInt(itemarray.length);

          if(data2.val().Format == "Direct Invoice"){
            annexitems.push([data2.key, data2.val().SONum, data2.val().Date, itemcount.toString(), data2.val().SODate, annextotalsi]);
          }

          
          for (const key of itemarray) {
            annextotalsi += parseFloat(key.CylinderPrice);
          }
          
        })
        resolve(items);
      })
    });   

    retrieveSODataAW();
  }
});

var selectedSOStatus = '';
async function retrieveSODataAW(){
  const specSOData = await getSpecSOData;
  const SIList = await getSIList;
  const DRList = await getDRList;

  document.querySelector(".modal-title").textContent = SOID;
  document.getElementById("modalCustName").textContent = specSOData[1];
  document.getElementById("modalCustID").textContent = specSOData[5];
  document.getElementById("modalPONumber").textContent = specSOData[2];
  document.getElementById("modalDelDate").textContent = specSOData[4];
  document.getElementById("modalStatus").textContent = specSOData[3];
  selectedSOStatus = specSOData[3];
  //variables for pdf formation
  sonumpdf = specSOData[0];
  custnamepdf = specSOData[1];
  ponumpdf = specSOData[2];
  deldatepdf = specSOData[4];
  custaddpdf  = specSOData[6];
  deladdresspdf = specSOData[7];
  sidrpdf = specSOData[8];
  remarkspdf = specSOData[9];
  createdatepdf = specSOData[10];
  prepbypdf = specSOData[12];
  isAnnex = specSOData[13];

  if(table3){
    table3.destroy();
  }
  table3 = $('#SITable').DataTable( {
    data: SIList,
    select: true,
    columns: [
      { title: "SI ID" },
      { title: "Date" }
    ]
  });

  if(table4){
    table4.destroy();
  }
  table4 = $('#DRTable').DataTable( {
    data: DRList,
    select: true,
    columns: [
      { title: "DR ID" },
      { title: "Date" }
    ]
} );

  $('a[data-toggle="tab"]').on('shown.bs.tab', async function (e) {
    var target = $(e.target).attr("href") // activated tab
    
    if(target == '#dispatchDiv'){
      document.querySelector(".loading2").style.display = "block";
      const dispatchList = await getDispatchList();
      if(table2){
        table2.destroy();
      }
      table2 = $('#dispatchTable').DataTable( {
          data: dispatchList,
          select: true,
          columns: [
            { title: "Dispatch ID" },
            { title: "Date" }
          ]
      } );
      document.querySelector(".loading2").style.display = "none";
    }else if(target == '#itemsDiv'){
      document.querySelector(".loading2").style.display = "block";
      const itemsList = await getItemList();
      if(table5){
        table5.destroy();
      }
      table5 = $('#itemsTable').DataTable( {
        data: itemsList,
        select: true,
        columns: [
          { title: "Cylinder ID" },
          { title: "Quantity Type" }
        ]
      });
      document.querySelector(".loading2").style.display = "none";
    }
  });

  document.querySelector(".loading2").style.display = "none";
  $('#myModal').modal({
    backdrop: 'static',
    keyboard: false
  });
  $('#myModal').modal("show");
}

document.getElementById("editSOBtn").addEventListener("click", (event) => {
  if (selectedSOStatus === "Pending"){
    localStorage.setItem("SOID", SOID)
    window.location = "./salesOrder/editSO.html";
  }
});
