//cylinder balance variables
var items = [];
var vardatelist = [];
var customernames = [];
var cylindervalues = [];
var tempcylinderhldr = [];
var splittedvalues = [];
var datestemphldr = [];
var valueshldr = [];
var finalcustvalues = [];
var finalcylvalues = [];
var nameloopctr = 0;
var cylinderctr = 0;

//PDF variables
var pdfitems = [];

//database references
var databaseRef = firebase.database().ref();
var inventoryRef = databaseRef.child("Inventory");

//pdfvariables

document.querySelector(".loading").style.display = "block";

var table = '';
//loading of data from DB
const getDates = new Promise((resolve, reject) => {
  inventoryRef.once("value", (data) => {
    data.forEach((data2) => {
      data2.forEach(function(data3){
        data3.forEach(function(data4){
            if(data3.key == "CylinderRecord"){
              if(datestemphldr.includes(data4.val().recorddate) == false){ //Check if data already exists in array
                datestemphldr.push(data4.val().recorddate);
                vardatelist.push([data4.val().recorddate]);
              }
          }
        });
      });
    });
    resolve(vardatelist);
  });
  // reject();
});

//table assignment
async function retrieveSOListAW(){
  const vardatelist = await getDates;
  document.querySelector(".loading").style.display = "none";
  table = $('#cylreportondelandpickuptable').DataTable( {
      data: vardatelist,
      select: true,
      columns: [
          { title: "Date" }
      ]
  } );
}
retrieveSOListAW();

var date = '';
$('#cylreportondelandpickuptable tbody').on('click', 'td', (e) => {
  document.querySelector(".loading2").style.display = "block";
  var selectedRow = e.target.parentNode;
  date = selectedRow.children[0].innerText;
  //Reinitialize Arrays
  customernames = [];
  cylindervalues = [];
  tempcylinderhldr = [];
  splittedvalues = [];
  valueshldr = [];
  finalcustvalues = [];
  finalcylvalues = [];
  //Initialize Table Variables
  var items = [];
  var cylinderhldr = [];
  var splittedvalshldr = [];
  //Delivered Quan Ctr
  var delmed02ctr = 0;
  var delcairctr = 0;
  var deln20ctr = 0;
  var deltech02ctr = 0;
  var delc2h2ctr = 0;
  var delc02ctr = 0;
  var deln2ctr = 0;
  var delargonctr = 0;
  //Picked Up Quan Ctr
  var pickmed02ctr = 0;
  var pickcairctr = 0;
  var pickn20ctr = 0;
  var picktech02ctr = 0;
  var pickc2h2ctr = 0;
  var pickc02ctr = 0;
  var pickn2ctr = 0;
  var pickargonctr = 0;
  getLedgerItems = new Promise((resolve, reject) => {
    inventoryRef.once("value", (data) => {
      data.forEach((data2) => {
        data2.forEach(function(data3){
          data3.forEach(function(data4){
              if(date == data4.val().recorddate){
                if(customernames.indexOf(data4.val().recordcust) == -1){  //Check if Data already exists in array
                  customernames.push(data4.val().recordcust);
                }
                if(tempcylinderhldr.indexOf(data2.key) == -1){
                  tempcylinderhldr.push(data2.key);
                  cylindervalues.push(data2.key+","+data2.val().cylindertype+","+data4.val().recordcust+","+data4.val().recordstatus+","+data4.val().custCode);
                }
              }
            });
        });
      });

      for(var namectr=0;namectr<customernames.length;namectr++){
          for(var cylctr=0;cylctr<cylindervalues.length;cylctr++){
            splittedvalues = cylindervalues[cylctr].split(",");    //Split Values
            if(splittedvalues[2] == customernames[namectr]){       //IF name exists. Set values to temp array
              valueshldr.push(cylindervalues[cylctr]);
            }
        }
        finalcustvalues.push(customernames[namectr]);
        finalcylvalues.push(valueshldr);
        valueshldr = [];
      }

      for(var cylctr=0;cylctr<finalcustvalues.length;cylctr++){
        cylinderhldr = finalcylvalues[cylctr];
        for(balctr=0;balctr<cylinderhldr.length;balctr++){
          splittedvalues = cylinderhldr[balctr].split(",");    //Split Values
          if(splittedvalues[1] == "MED 02"){
            if(splittedvalues[3] == "Out"){
              delmed02ctr++;
            }else if(splittedvalues[3] == "In"){
              pickmed02ctr++;
            }
          }
          else if(splittedvalues[1] == "COMP AIR"){
            if(splittedvalues[3] == "Out"){
              delcairctrctr++;
            }else if(splittedvalues[3] == "In"){
              pickcairctr++;
            }
          }
          else if(splittedvalues[1] == "NITROUS DIOXIDE"){
            if(splittedvalues[3] == "Out"){
              deln20ctr++;
            }else if(splittedvalues[3] == "In"){
              pickn20ctr++;
            }
          }
          else if(splittedvalues[1] == "TECH 02"){
            if(splittedvalues[3] == "Out"){
              deltech02ctr++;
            }else if(splittedvalues[3] == "In"){
              picktech02ctr++;
            }
          }
          else if(splittedvalues[1] == "ACETYLENE"){
            if(splittedvalues[3] == "Out"){
              delc2h2ctr++;
            }else if(splittedvalues[3] == "In"){
              pickc2h2ctr++;
            }
          }
          else if(splittedvalues[1] == "CARBON DIOXIDE"){
            if(splittedvalues[3] == "Out"){
              delc02ctr++;
            }else if(splittedvalues[3] == "In"){
              pickc02ctr++;
            }
          }
          else if(splittedvalues[1] == "NITROGEN"){
            if(splittedvalues[3] == "Out"){
              deln2ctr++;
            }else if(splittedvalues[3] == "In"){
              pickn2ctr++;
            }
          }
          else if(splittedvalues[1] == "ARGON"){
            if(splittedvalues[3] == "Out"){
              delargonctr++;
            }else if(splittedvalues[3] == "In"){
              pickargonctr++;
            }
          }
        }
        items.push([finalcustvalues[cylctr], delmed02ctr, delcairctr, deln20ctr, deltech02ctr, delc2h2ctr, delc02ctr, deln2ctr, delargonctr
                                           , pickmed02ctr, pickcairctr, pickn20ctr, picktech02ctr, pickc2h2ctr, pickc02ctr, pickn2ctr, pickargonctr]);
        pdfitems.push(splittedvalues[4]+","+delmed02ctr+","+delcairctr+","+deln20ctr+","+deltech02ctr+","+delc2h2ctr+","+delc02ctr+","+deln2ctr+","+delargonctr
                                             +","+pickmed02ctr+","+pickcairctr+","+pickn20ctr+","+picktech02ctr+","+pickc2h2ctr+","+pickc02ctr+","+pickn2ctr+","+pickargonctr);
        //reset delivery quantity ctr
        delmed02ctr = 0;
        delcairctr = 0;
        deln20ctr = 0;
        deltech02ctr = 0;
        delc2h2ctr = 0;
        delc02ctr = 0;
        deln2ctr = 0;
        delargonctr = 0;
        //reset pick ip quantity ctr
        pickmed02ctr = 0;
        pickcairctr = 0;
        pickn20ctr = 0;
        picktech02ctr = 0;
        pickc2h2ctr = 0;
        pickc02ctr = 0;
        pickn2ctr = 0;
        pickargonctr = 0;
      }
      resolve(items);
    });
  });
  retrieveLedgerDataAW();
});

async function retrieveLedgerDataAW() {
  const items = await getLedgerItems;
  document.querySelector(".modal-title").textContent = date;
  table2 = $('#ledgerItemsTable').DataTable( {
      data: items,
      select: true,
      columns: [
        { title: "Customer Name" },
        { title: "Del Med 02" },
        { title: "Del C-Air" },
        { title: "Del N20" },
        { title: "Del TECH 02" },
        { title: "Del C2H2" },
        { title: "Del C02" },
        { title: "Del N2" },
        { title: "Del Argon" },
        { title: "Pick Med 02" },
        { title: "Pick C-Air" },
        { title: "Pick N20" },
        { title: "Pick TECH 02" },
        { title: "Pick C2H2" },
        { title: "Pick C02" },
        { title: "Pick N2" },
        { title: "Pick Argon" }
      ]
  } );
  document.querySelector(".loading2").style.display = "none";
  $('#myModal').modal({
    backdrop: 'static',
    keyboard: false
  });
  $('#myModal').modal("show");
}

function clearTables() {
  table2.destroy();
  // table3.destroy();
  // table4.destroy();
  // table5.destroy();
}
