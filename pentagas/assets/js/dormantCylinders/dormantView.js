var databaseRef = firebase.database().ref();
var customerRef = databaseRef.child("Customer");
var bigledgerRef = databaseRef.child("Big Ledger");
var smallLedgerRef = databaseRef.child("Small Ledger").orderByChild("status").startAt('Active').endAt('Active');

var addressedTo = document.getElementById("addressedTo");
var addressedToPos = document.getElementById("addressedToPos");
var ccsHeadName = document.getElementById("ccsHeadName");

//display loading screen
document.querySelector(".loading").style.display = "block";

var customername = '';
var customeraddress = '';
var items = [];

var bigLedgerList = [];
var customerDormant = [];
var customerDormantItems = {};
const getCylinders = new Promise((resolve, reject) => {
    smallLedgerRef.once("value", (data) => {
        for (let key in data.val()){
            if (!moment().subtract(6, 'months').isBefore(moment(data.val()[key].lastOut))){
                let reference = '';
                if (data.val()[key].directInvoice){
                    reference = data.val()[key].CylinderRecord[`${data.val()[key].lastOut.split(' ')[0].replace(/\/+/g, '-')} ${data.val()[key].lastOut.split(' ')[1]}`].invoiceNum;
                } else {
                    reference = data.val()[key].CylinderRecord[`${data.val()[key].lastOut.split(' ')[0].replace(/\/+/g, '-')} ${data.val()[key].lastOut.split(' ')[1]}`].drNumber + " / " + data.val()[key].CylinderRecord[`${data.val()[key].lastOut.split(' ')[0]} ${data.val()[key].lastOut.split(' ')[1]}`].invoiceNum;
                }
                if (!customerDormantItems[`${data.val()[key].customer}`]){
                    customerDormant.push([data.val()[key].customer])
                    customerDormantItems[`${data.val()[key].customer}`] = {
                        items: []
                    }
                    customerDormantItems[`${data.val()[key].customer}`].items.push({"serial": key, "dateIssued": data.val()[key].lastOut, "cylinderType": data.val()[key].cylinderType, reference});
                } else {
                    customerDormantItems[`${data.val()[key].customer}`].items.push({"serial": key, "dateIssued": data.val()[key].lastOut, "cylinderType": data.val()[key].cylinderType, reference});
                }
            }
        }
        resolve(customerDormantItems);
    })
})

const getCustomers = new Promise((resolve, reject) => {
    var customersarray = [];
    customerRef.once("value", (data) => {
      data.forEach((data2) => {
        customersarray.push([data2.val().Name, data2.val().Address]);
      });
      resolve(customersarray);
    });
  });

async function retrieveLedgerListAW(){
    bigLedgerList = await getCylinders;
    customerList = await getCustomers;
    document.querySelector(".loading").style.display = "none";
    table = $('#cylindertable').DataTable( {
        data: customerDormant,
        select: true,
        columns: [
            { title: "Customer Name" },
            { title: "Action" , "defaultContent": "<a class='btn btn-info btn-round btn-sm' style='color:white;' onClick='printDormant(this)'>PRINT</a>"}
        ]
    } );
    document.querySelector(".loading").style.display = "none";
}
retrieveLedgerListAW();

let dormantTable;
function printDormant(event) {
    customername = event.parentNode.parentNode.childNodes[0].innerHTML;
    customeraddress = '';
    items = bigLedgerList[`${customername}`].items

    for(var x = 0; x < customerList.length; x++){
        if(customerList[x][0] == customername){
            customeraddress = customerList[x][1];
        }
    }

    dormantTable = $('#dormantTable').DataTable( {
        select: true,
        paging: false,
        columns: [
          { title: "No." },
          { title: "Cylinder ID" },
          { title: "Cylinder Type" },
          { title: "Date" },
          { title: "Reference No." },
          { title: "Location" }
        ]
    } );
    items.forEach((item, index) => {
        dormantTable.row.add([
            index + 1,
            item.serial,
            item.cylinderType,
            item.dateIssued,
            item.reference,
            "<input type='text' class='form-control location-text'>"
        ]).draw(false);
    })
    
    $('#modal-title').html(customername);
    $('#myModal').modal("show");
}

$('#myModal').on('hidden.bs.modal', function () {
    dormantTable.clear().draw();
    dormantTable.destroy();
})