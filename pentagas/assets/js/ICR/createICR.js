//variables
var table = '';
var table2;
var ICRList = [];
var forViewing = "true";
var icrPDFData = [];
var mainICRArr = [];
//database references
var databaseRef = firebase.database().ref();
var ICRRef = databaseRef.child("Incoming Cylinder Report")

var icrSearchInput = document.getElementById("icrSearchInput");
// var modalORideCylCtr = document.getElementById("modalORideCylCtr");
var pageLoad = 20;
var cylLoad = 10;

icrSearchInput.addEventListener('input', function (evt) {
  if (this.value == '') {
      stateTable(mainICRArr);
  }
});

//data retrieval
function getICRTable() {
  return new Promise((resolve, reject) => {
    var ICRList = [];
    ICRRef.orderByKey().limitToLast(pageLoad).once("value", (data) => {
      data.forEach((data2) => {
        ICRList.push([data2.key, data2.val().date, data2.val().custName, data2.val().cylType]);
      })
      resolve(ICRList);
    });
  });
}

function loadMoreICR(){
  return new Promise((resolve, reject) => {
    var ICRList = [];
    ICRRef.limitToLast(pageLoad).once("value", (data) => {
      data.forEach((data2) => {
        ICRList.push([data2.key, data2.val().date, data2.val().custName, data2.val().cylType]);
      })
      resolve(ICRList);
    });
  });
}

async function retrieveAllICRData(){
  document.querySelector(".loading").style.display = "block";
  ICRList = await getICRTable();
  mainICRArr = ICRList;
  stateTable(ICRList);
}

function retrieveSpecificICR() {
  if (icrSearchInput.value) {
    document.querySelector(".loading").style.display = "block";
    ICRRef.child(icrSearchInput.value).once('value', function (data2) {
      var exists = (data2.val() !== null);
      if (exists) {
        let specICRArr = [];
        specICRArr.push([data2.key, data2.val().date, data2.val().custName, data2.val().cylType]);
        stateTable(specICRArr);
      } else {
        document.querySelector(".loading").style.display = "none";
        window.alert("ICR Not Found.");
      }
    });
  } else {
    window.alert("Input is empty, please try again.");
  }
}

function stateTable(tablearr){
  var content = '';
  var paginate = false;
  paginate = tablearr.length > 1 ? true : false;

  if(table){
    table.destroy();
  }
  tablearr.forEach((value) => {
    content +='<tr id="' + "++" + value[0] + '">';
    content += '<td>' + value[0] + '</td>';
    content += '<td>' + value[1] + '</td>';
    content += '<td>' + value[2] + '</td>';
    content += '<td>' + value[3] + '</td>';
    content += '</tr>';
  });
  $('#ICRList').append(content);

  table = $('#ICRList').DataTable({
    "aaSorting": [],
    "bPaginate": paginate,
    drawCallback: function(){
      $('.paginate_button.next', this.api().table().container())          
         .on('click', async function(){
            document.querySelector(".loading").style.display = "block";
            pageLoad += 20;
            table.clear();
            ICRList = await loadMoreICR();
            mainICRArr = ICRList;
            stateTable(ICRList);
      });       
   }
  });
  document.querySelector(".loading").style.display = "none";
}

var ICRNo = '';
$('#ICRList tbody').on('click', 'td', (e) => {
  cylLoad = 10;
  var selectedRow = e.target.parentNode;
  ICRNo = selectedRow.children[0].innerText;
  if (ICRNo != "No data available in table"){
    document.querySelector(".loading2").style.display = "block";
    icrdata = [];
    getICRData = new Promise((resolve, reject) => {
      ICRRef.child(ICRNo).once("value", (data2) => {
        icrdata.push(data2.key, data2.val().date, data2.val().custName, data2.val().cylType, data2.val().custAddress, data2.val().oRideCylCtr);
      });
      resolve(icrdata);
    });

    cylsreturned = [];
    getCylsReturned = new Promise((resolve, reject) => {
      ICRRef.child(ICRNo).child("Cylinders").orderByKey().once("value", (data) => {
        data.forEach((data2) => {
          cylsreturned.push([data2.key, data2.val().type]);
        });
        resolve(cylsreturned);
      });
    });
    retrieveICRDataAW();
  }
});

//async function
async function retrieveICRDataAW(){
  const selCylList = await getCylsReturned;
  const icrData = await getICRData;
  icrPDFData = icrData;

  loadCylindersTable();

  document.querySelector(".modal-title").textContent = ICRNo;
  document.getElementById("modalCustName").textContent = icrData[2];
  document.getElementById("modalCustAddress").textContent = icrData[4];
  document.getElementById("modalCylType").textContent = icrData[3];
  document.getElementById("modalDelDate").textContent = icrData[1];

  // modalORideCylCtr.value = icrData[5] == 0 ? selCylList.length : icrData[5];

  document.querySelector(".loading2").style.display = "none";
  $('#DispModal').modal({
    backdrop: 'static',
    keyboard: false
  });
  $('#DispModal').modal("show");
}

function loadCylindersTable(){
  var cylcontent = '';
 
  if(table2){
    table2.destroy();
  }

  cylsreturned.forEach((value) => {
    cylcontent +='<tr id="' + "++" + value[0] + '">';
    cylcontent += '<td>' + value[0] + '</td>';
    cylcontent += '<td>' + value[1] + '</td>';
    cylcontent += '</tr>';
  });
  $('#itemsTable').append(cylcontent);

  table2 = $('#itemsTable').DataTable({
    "aaSorting": [],
    "select": true,
  //   drawCallback: function(){
  //     $('.paginate_button.next', this.api().table().container())          
  //        .on('click', async function(){
  //           document.querySelector(".loading").style.display = "block";
  //           cylLoad += 10;
  //           table2.clear();
  //           selCylList = await loadMoreCylinders();
  //           loadCylindersTable();
  //     });       
  //  }
  });
  document.querySelector(".loading").style.display = "none";
}

function loadMoreCylinders(){
    return new Promise((resolve, reject) => {
      cylsreturned = [];
      ICRRef.child(ICRNo).child("Cylinders").orderByKey().limitToFirst(cylLoad).once("value", (data) => {
        data.forEach((data2) => {
          cylsreturned.push([data2.key, data2.val().type]);
        });
        resolve(cylsreturned);
      });
    });
}

function overrideCylinders(){
  ICRRef.child(ICRNo).update({
    oRideCylCtr: modalORideCylCtr.value
  }).then(() => {
    window.alert("Cylinder Count Override successful!");
  })
}

function clearTables() {
  table2.destroy();
}
