var databaseRef = firebase.database().ref();
var customerRef = databaseRef.child("Customer");
var productRef = databaseRef.child("Inventory");
var SORef = databaseRef.child("Sales Order");
var PORef = databaseRef.child("Purchase Order");
var ICRRef = databaseRef.child("Incoming Cylinder Receipt")
var table = '';
var table2 = '';
var forViewing = "false";

//input fields
var ICRNumberInput = document.getElementById("ICRNumber");
var PONumberInput = document.getElementById("PONumber");
var addressInput = document.getElementById("Address");
var custNameInput = document.getElementById("CustName");
var orderedByInput = document.getElementById("Order");
var productsSelected = [];
var productList = [];
var productsQty = [];
var productsPrice = [];
var otherInput = document.getElementById("OtherProduct");
var CustTypeInput = document.getElementById("CustType");
var CustTINNum = document.getElementById("CustTINNum");
var CustCodeInput = document.getElementById("CustCode");
var PRNumInput = document.getElementById("PRNum");
var dateInput = document.getElementById("DatePicker");
var delAddressInput = document.getElementById("DelAddress");
var SIDRNumInput = document.getElementById("SIDRNum");
var TermsInput = document.getElementById("Terms");
var remarksInput = document.getElementById("Remarks");
var SOTypeInput = document.getElementById("SOType");
var preparedbyInput = document.getElementById("PrepBy");
// var recievedbyInput = document.getElementById("RecBy");

//value hldr
var SIDRHldr = '';
var remarksHldr = '';
var otherItemCtr = 0;
var isAnnex = "false";
var createDR = false;

//checker
var itemChckr = false;
var itemChckr2 = false;
var otherItemChckr = false;
var soChckr = false;
var soChckr2 = false;
var custChckr = false;
var custChckr2 = false;
var lastidindb = 0;
var itemIDs = [];
var itemTypes = [];

var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1;
var yyyy = today.getFullYear();
var hr = today.getHours();
var min = today.getMinutes();
var sec = today.getSeconds();
// console.log(timeNow);
console.log(today)
if(dd<10)
{
  dd='0'+dd;
}

if(mm<10)
{
  mm='0'+mm;
}
var datetoday = mm+'-'+dd+'-'+yyyy+" "+hr+":"+min+":"+sec;


productList = document.getElementsByClassName("form-check-input");
productsQty = document.getElementsByClassName("Qty");
productsPrice = document.getElementsByClassName("Price");

$('#PONumber').focus();

const getItemIDs = new Promise((resolve, reject) => {
  SORef.once("value", (data) => {
    data.forEach((data2) => {
      var keyhldr = data2.key.split("-");
      itemIDs.push(parseInt(keyhldr[1]));
    });
    resolve(itemIDs);
  });
});

const getCustomers = new Promise((resolve, reject) => {
  var customers = [];
  customerRef.once("value", (data) => {
    data.forEach((data2) => {
      customers.push(data2.val().Name);
    });
    resolve(customers);
  });
});

const getProducts = new Promise((resolve, reject) => {
  var products = [];
  productRef.once("value", (data) => {
    data.forEach((data2) => {
      products.push([data2.key, data2.val().cylindertype, data2.val().cylinderstatus]);
    });
    resolve(products);
  });

})

//display loading screen
document.querySelector(".loading").style.display = "block";


//function that waits for data before execution
async function retrieveCustAW(){
  //for sales order id
  // itemIDs = await getItemIDs;
  // if(itemIDs.length == 0){
  //   SONumberInput.value = "SO-00001";
  // }else{
  //   lastidindb = Math.max.apply(Math,itemIDs);
  //   SONumberInput.value = "SO-0000"+(lastidindb+1);
  // }

  //for customer name
  const customers = await getCustomers;
  var options = '';
  for(var i = 0; i < customers.length; i++)
    options += '<option value="'+customers[i]+'" />';
  document.getElementById('Customers').innerHTML = options;
  //for products table
  const products = await getProducts;
  // console.log(products)
  var productArr = [];
  var productQty = [0,0,0,0,0,0,0,0,0,0,0];
  products.forEach((data) => {
    if (data[2] === "Production"){
      productArr.push([data[0],data[1]]);
      // console.log(data[1] + " " + data[2]);
      switch(data[1]) {
        case "MED 02":
            productQty[0] += 1;
            break;
        case "MED 02 FLASK":
            productQty[1] += 1;
            break;
        case "MED 02 BANTAM":
            productQty[2] += 1;
            break;
        case "MED 02 MEDIUM":
            productQty[3] += 1;
            break;
        case "TECH 02":
            productQty[4] += 1;
            break;
        case "ARGON":
            productQty[5] += 1;
            break;
        case "COMP AIR":
            productQty[6] += 1;
            break;
        case "ACETYLENE":
            productQty[7] += 1;
            break;
        case "NITROGEN":
            productQty[8] += 1;
            break;
        case "NITROUS DIOXIDE":
            productQty[9] += 1;
            break;
        case "CARBON DIOXIDE":
            productQty[10] += 1;
            break;
      }
    }
  });
  for(var i = 0; i<productQty.length; i++){
    document.getElementById("value-" + i).textContent = productQty[i];
  }
  // console.log(productQty);
  table = $('#productsTable').DataTable( {
      data: productArr,
      select: true,
      columns: [
          { title: "Cylinder ID" },
          { title: "Cylinder" },
          { title: "Action" , "defaultContent": "<a class='btn btn-info btn-round btn-sm' style='color:white;'>ADD</a>"}
      ]
  } );
  //for order table
  table2 = $('#orderTable').DataTable( {
      select: true,
      columns: [
          { title: "Cylinder ID" },
          { title: "Cylinder" },
          { title: "Action" }
      ]
  } );

  // for (var i = 0; i < productList.length - 1 ; i++){
  //   console.log(productList[i].value);
  // }
  //to hide loading screen
  document.querySelector(".loading").style.display = "none ";
}
//async function call
retrieveCustAW();

//click event for product table
// $('#productsTable tbody').on( 'select', function ( e, dt, type, indexes ) {
//     console.log(type)
//     if ( type === 'row' || type === 'cell' || type === 'column') {
//         var data = table.rows( indexes ).data();
//         console.log(data);
//         // do something with the ID of the selected items
//     }
// } );
$('#productsTable tbody').on('click', 'a', (e) => {
  var selectedRow = e.target.parentNode.parentNode;
  // console.log(selectedRow.children[0].innerText);
  var cylinderID = selectedRow.children[0].innerText;
  var cylinder = selectedRow.children[1].innerText
  e.target.parentNode.parentNode.classList.toggle("selected");
  // console.log(e.target.parentNode.parentNode);
  table.row(".selected").remove().draw(false);
  table2.row.add([
    cylinderID,
    cylinder,
    "<a class='btn btn-info btn-round btn-sm' style='color:white;'>Remove</a>"
  ]).draw(false);
});

$('#orderTable tbody').on('click', 'a', (e) => {
  var selectedRow = e.target.parentNode.parentNode;
  // console.log(selectedRow.children[0].innerText);
  var cylinderID = selectedRow.children[0].innerText;
  var cylinder = selectedRow.children[1].innerText
  e.target.parentNode.parentNode.classList.toggle("selected");
  // console.log(e.target.parentNode.parentNode);
  table2.row(".selected").remove().draw(false);
  table.row.add([
    cylinderID,
    cylinder,
    "<a class='btn btn-info btn-round btn-sm' style='color:white;'>Add</a>"
  ]).draw(false);
});

var customer = {
  id: ""
};

//input event for customer name
$('#CustName').bind("input", () => {
  var val = document.getElementById("CustName").value;
  var options = document.getElementById("Customers").childNodes;
  // var newCust = false;
  for (var i = 0; i < options.length; i++){
    if (options[i].value == val){
      // newCust = false;
      fillData(val);
    } else {
      document.getElementById("Address").value = "";
      document.getElementById("DelAddress").value = "";
      document.getElementById("Order").value = "";
      customers.id = "";
      customers.Name = "";
      customers.Address = "";
      customers.OrderedBy = "";
      customers.DelAddress = "";
    }
  }
});


var getCustData = '';
var customers = {};
//function for autofill
function fillData(custName){
  console.log(custName);
  document.querySelector(".loading2").style.display = "block";
  getCustData = new Promise((resolve, reject) => {
    customerRef.once("value", (data) => {
      data.forEach((data2) => {
        if (data2.val().Name === custName){
          console.log("enter DB");
          customers.id = data2.key;
          customers.Name = data2.val().Name;
          customers.Address = data2.val().Address;
          customers.OrderedBy = data2.val().OrderedBy;
          customers.DelAddress = data2.val().DelAddress;
          customers.CustCode = data2.val().CustCode;
          customers.CustType = data2.val().CustType;
          customers.TinNum = data2.val().custTINNum;
        }
      });
      resolve(customers);
    });
  });
  retrieveCustDataAW();
}

const getItemPrices = customerID => {
  return new Promise((resolve, reject) => {
    var itemPrices = [];
    customerRef.child(customerID).child("Item Prices").once("value", (data) => {
      data.forEach((data2) => {
        itemPrices.push([data2.key, data2.val().code, data2.val().Price]);
      });
      resolve(itemPrices);
    })
  })
}

var itemPrices = [];

async function retrieveCustDataAW(){
  customer = await getCustData;
  itemPrices = await getItemPrices(customer.id);
  console.log(itemPrices);
  itemPrices.forEach((data) => {
    console.log(productsPrice[0].value + data[1]);
    productsPrice[data[1]].value = data[2];
  })
  console.log(customer);
  document.getElementById("Address").value = customer.Address;
  document.getElementById("DelAddress").value = customer.DelAddress;
  document.getElementById("Order").value = customer.OrderedBy;
  CustCodeInput.value = customer.CustCode;
  CustTypeInput.value = customer.CustType;
  // CustTINNum.value = customers.TinNum
  document.querySelector(".loading2").style.display = "none";
}



//save button
document.getElementById("createDRBtn").addEventListener("click", (event) => {
  event.preventDefault();
  if (confirm("You are about to create a Delivery Receipt. Would you like to continue?")){
    validate();

  } else {
    // do something.
  }
});

//save button
document.getElementById("createSIBtn").addEventListener("click", (event) => {
  event.preventDefault();
  if (confirm("You are about to create a Sales Invoice. Would you like to continue?")){
    validate();

  } else {
    // do something.
  }
});

//cancel button
document.getElementById("cancelSOBtn").addEventListener("click", (event) => {
  event.preventDefault();
  if (confirm("You are about to discard this session. Would you like to continue?")){
    window.location = "../index.html";
  } else {
    // do something.
  }
});

//click event for sales order annex
$('#annexCheck').bind("click", () => {
  if (productList[11].checked){
    // console.log("annex");
    isAnnex = "true";
    dateInput.value = "Staggered Delivery";
    dateInput.setAttribute("disabled", "disabled");
  } else {
    // console.log("sales order");
    isAnnex = "false";
    dateInput.value = "";
    dateInput.removeAttribute("disabled");
  }
});


//PURCHASE ORDER
function createPO(){
  var today = new Date();
  var dd = today.getDate();
  var mm = today.getMonth()+1;
  var yyyy = today.getFullYear();
  if(dd<10){
    dd='0'+dd;
  }

  if(mm<10){
    mm='0'+mm;
  }
  today = mm+'/'+dd+'/'+yyyy;

  var CUSTIDTEMP = "CUST-0001";
  var itemsselectedarr = [];
  var itemsquanarr = [];
  var itemscostarr = [];
  var initialbalance = 0;
  var currbalance = 0;
  var postatus = "Active";

  for (var socreatectr = 0; socreatectr < productList.length; socreatectr++){
    if (productList[socreatectr].checked){
      itemnames.push(productList[socreatectr].value);
      itemqtys.push(productsQty[socreatectr].value);
      itemprices.push(productsPrice[socreatectr].value);
    }
  }

  for(soqtyctr=0;i<itemqtys.length;soqtyctr++){
    initialbalance += itemsquanarr[soqtyctr];
  }

  currbalance = initialbalance;

  PORef.child(PONumberInput.value).set({
    createDate: today,
    PONum: PONumberInput.value,
    SONum: SONumberInput.value,
    customerID: customer.id,
    customerName: custNameInput.value,
    custAddress: addressInput.value,
    itemsSelected: itemnames,
    itemsQuantity: itemsqtys,
    itemsPrices: itemprices,
    initialBalance: initialbalance,
    currentBalance: currbalance,
    POStatus: postatus
  });
}

//SALES ORDER
function createOwnersICR(){
  // createPO();
  document.querySelector(".loading2").style.display = "block";

//   const validateSOID = new Promise((resolve, reject) => {
//   productRef.once("value", (data) => {
//     data.forEach((data2) => {
//       var keyhldr = data2.key.split("-");
//       itemIDs.push(parseInt(keyhldr[1]));
//     });
//     resolve(itemIDs);
//   });
// });
//
// validateIDAW();
//
// async function validateIDAW(){
//   itemIDs = await getItemIDs;
//   lastidindb = Math.max.apply(Math,itemIDs);
//   if(lastidindb)
//   itemnumber.value = "SO-0000"+(lastidindb+1);
//   retrieveTypesAW();
// }



  for (var i = 0; i < productList.length - 1 ; i++){
    if (productList[i].checked){
      productsSelected.push([productList[i].value, productsQty[i].value, productsPrice[i].value, i]);
    }
  }
  // console.log(SONumberInput.value + "/n" +
  //   addressInput.value + "/n" +
  //   custNameInput.value + "/n" +
  //   orderedByInput.value + "/n" +
  //   productsSelected + "/n" +
  //   otherInput.value + "/n" +
  //   dateInput.value + "/n" +
  //   delAddressInput.value + "/n" +
  //   SIDRNumInput.value + "/n" +
  //   remarksInput.value + "/n" +
  //   today
  // );

  //retrieve data from table
  // var data = table2.rows().data();
  //  data.each(function (value, index) {
  //      productsSelected.push([value[0], value[1]]);
  //  });
  ICRRef.child(ICRNumberInput.value).set({
    custName: CustName.value,
    date: datetoday,
    custAddress: addressInput.value,
    custCode: CustCodeInput.value,
    prepBy: preparedbyInput.value,
    remarks: remarksInput.value
  })
  .then(() => {
    soChckr = true;
    console.log("ICRRef");
    checker(itemChckr, itemChckr2, otherItemChckr, soChckr);
  })

  var itemsProcessed = [0, 0];
  if (productsSelected.length == 0){
    itemChckr = true;
    itemChckr2 = true;
    console.log("ProductRef");
    checker(itemChckr, itemChckr2, otherItemChckr, soChckr);
  } else {
    productsSelected.forEach((data2) => {
      // console.log(data2[0] + " " + data2[1]);
      ICRRef.child(ICRNumberInput.value).child("Cylinders").child(data2[0]).set({
        itemQty: data2[1],
        itemPrice: data2[2]
      })
      .then(() => {
        itemsProcessed[0]++;
        if (itemsProcessed[0] = productsSelected.length){
          itemChckr = true;
          console.log("ProductRef2");
          checker(itemChckr, itemChckr2, otherItemChckr, soChckr);
        }
      });
      customerRef.child(customer.id).child("Item Prices").child(data2[0]).update({
        code: data2[3],
        Price: data2[2]
      })
      .then(() => {
        itemsProcessed[1]++;
        if (itemsProcessed[1] = productsSelected.length){
          itemChckr2 = true;
          console.log("ProductRef3");
          checker(itemChckr, itemChckr2, otherItemChckr, soChckr);
        }
      })
    });
  }

  if (otherItemCtr === 0) {
    otherItemChckr = true;
    checker(itemChckr, itemChckr2, otherItemChckr, soChckr);
  } else {
    for (var i = 0; i < otherItemCtr; i++){
      ICRRef.child(ICRNumberInput.value).child("Cylinders").child("Others").child(document.getElementById("itemName-" + i).value).update({
        // itemQty: document.getElementById("itemQty-" + i).value,
        itemPrice: document.getElementById("itemPrice-" + i).value
        // itemDelivered: "0",
        // itemDispatched: "0"
      })
      .then(() => {
        console.log((i) + " " + otherItemCtr )
        if (i == otherItemCtr){
          otherItemChckr = true;
          console.log("OtherRef");
          checker(itemChckr, itemChckr2, otherItemChckr, soChckr);
        }
      });
     }
    }
}

function validate(){
  if (document.getElementById("ICRNumber").value.length == 0){
    document.querySelector(".loading2").style.display = "none";
    alert("ICR number cannot be empty");
  } else {
    localStorage.setItem("ICRNum", document.getElementById("ICRNumber").value);
    console.log(document.getElementById("ICRNumber").value);
    createOwnersICR();

  }
}

function addItem(){
  var currentChild = document.getElementById("otherItemDiv");
  var openingDiv = "<div id='item-"+ otherItemCtr + "' class='form-group' style='max-height:300px;'>";
  var itemNameInput = "<input id='itemName-" + otherItemCtr + "' type='number' placeholder='Cylinder ID' style='width:100px'>";
  // var itemQtyInput = "<input id='itemQty-" + otherItemCtr + "' type='number' style='width:50px' placeholder='Qty'>";
  var itemPriceInput = "<input id='itemPrice-" + otherItemCtr + "' type='number' style='width:70px' placeholder='Price'>";
  var itemRemoveBtn = "<button id='close-"+otherItemCtr+"' onclick='removeItem(this, event)' class='btn btn-warning btn-fab btn-round btn-sm'><i class='material-icons'>close</i></button>";
  var closingDiv = "</div>";
  currentChild.innerHTML+= openingDiv+itemNameInput+itemPriceInput+itemRemoveBtn+closingDiv;
  otherItemCtr++;
}

function removeItem(object, e){
  e.preventDefault();
  var id = object.id.split("-");
  // console.log("remove " + id[1]);
  $("#item-" + id[1]).remove();
  otherItemCtr--;
}


// console.log(productsQty[0]);
// for (var i = 0; i < productsSelected.length; i++){
//   productsSelected[i].addEventListener("change", () => {
//     console.log(i);
//     if($(this).is(':checked')){
//       productsQty[i].removeAttribute("disabled");
//     } else {
//       console.log(productsQty[i]);
//       // productsQty[i].removeAttribute("disabled");
//       // productsQty[i].setAttribute("disabled","disabled");
//     }
//   })
// }

function checker(itemChckr, itemChckr2, otherItemChckr, soChckr) {
  console.log(itemChckr + " " + itemChckr2 + " " + otherItemChckr + " " + soChckr)
  if (itemChckr && itemChckr2 && otherItemChckr && soChckr){
    if (createDR){
      window.location = "createDeliveryReceipt3.html";
    } else {
      window.location = "createSalesInvoice4.html";
    }

    document.querySelector(".loading2").style.display = "none";
  }
}
