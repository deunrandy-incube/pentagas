// variables
var table = '';
var table2 = '';
var forViewing = "false";

//display loading screen
document.querySelector(".loading").style.display = "block";

// input fields
var ICRNo = document.getElementById("ICRNo");
var CustName = document.getElementById("CustomerName");
var date = document.getElementById("ICRDate");
var cylType = document.getElementById("CylinderType");
var custAddress = document.getElementById("Address");
var cylStat = document.getElementById("CylinderStatus");

//get date today
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth() + 1;
var yyyy = today.getFullYear();
var hr = today.getHours();
var min = today.getMinutes();
var sec = today.getSeconds();
var checker = [false, false, false, false, false];

if (dd < 10) {
  dd = '0' + dd;
}

if (mm < 10) {
  mm = '0' + mm;
}
var datetoday = mm + '/' + dd + '/' + yyyy + " " + hr + ":" + min + ":" + sec + " AM";
document.getElementById("ICRDate").value = datetoday;

//references
var databaseRef = firebase.database().ref();
var ICRRef = databaseRef.child("Incoming Cylinder Report");
var inventoryRef = databaseRef.child("Inventory");
var customerRef = databaseRef.child("Customer")
var smallLedgerRef = databaseRef.child("Small Ledger");
var bigLedgerRef = databaseRef.child("Big Ledger");
var custCylindersRef = databaseRef.child("Customer Cylinder");

//table array
var cyltable = [];
var custcyls = [];

//getter
const getCustData = new Promise((resolve, reject) => {
  var customers = [];
  customerRef.once("value", (data) => {
    data.forEach((data2) => {
      customers.push(data2.val().Name);
    });
    resolve(customers);
  });
});

async function initialAW() {
  const customers = await getCustData;
  var options = '';
  for (var i = 0; i < customers.length; i++)
    options += '<option value="' + customers[i] + '" />';
  document.getElementById('Customers').innerHTML = options;
  var uniqueTypes = [
    "MED 02",
    "COMP AIR",
    "TECH 02",
    "ARGON",
    "ACETYLENE",
    "NITROGEN",
    "NITROUS DIOXIDE",
    "CARBON DIOXIDE"
  ];
  // $.each(smallLedgers, function (i, el) {
  //   if ($.inArray(el, uniqueTypes) === -1) uniqueTypes.push(el);
  // });
  var typeoptions = '';
  for (var i = 0; i < uniqueTypes.length; i++)
  typeoptions += '<option value="' + uniqueTypes[i] + '" />';
  document.getElementById('CylinderType').disabled=true;
  document.getElementById('Products').innerHTML = typeoptions;
  
  table = $('#activeProductsTable').DataTable({
    select: true,
    columns: [{
        title: "Cylinder ID"
      },
      {
        title: "Cylinder Type"
      },
      {
        title: "Action",
        "defaultContent": "<a class='btn btn-info btn-round btn-sm' style='color:white;'>ADD</a>"
      }
    ]
  });
  table2 = $('#ICRTable').DataTable({
    select: true,
    columns: [{
        title: "Cylinder ID"
      },
      {
        title: "Cylinder Type"
      },
      {
        title: "Status",
        "defaultContent": '<select id="ItemDesc" value="Empty" class="form-control">' +
          '<option value="Empty">Empty</option>' +
          '<option value="Defective">Defective</option>' +
          '<option value="Production">Production</option>' +
          '</select>'
      },
      {
        title: "Action",
        "defaultContent": "<a class='btn btn-info btn-round btn-sm' style='color:white;'>REMOVE</a>"
      }
    ]
  })
  document.querySelector(".loading").style.display = "none";
}
initialAW();

var customers = {};
//input event for customer name
$('#CustomerName').bind("input", () => {
  var val = document.getElementById("CustomerName").value;
  var options = document.getElementById("Customers").childNodes;
  // var newCust = false;
  for (var i = 0; i < options.length; i++) {
    if (options[i].value == val) {
      // newCust = false;
      fillData(val);
    } else {
      document.getElementById("Address").value = "";
      customers.id = "";
      customers.Name = "";
      customers.Address = "";
    }
  }
});

var getCustData2 = '';

//function for autofill
function fillData(custName) {
  document.getElementById('CylinderType').disabled= false;
  document.querySelector(".loading2").style.display = "block";
  getCustData2 = new Promise((resolve, reject) => {
    customerRef.once("value", (data) => {
      data.forEach((data2) => {
        if (data2.val().Name === custName) {
          customers.id = data2.key;
          customers.Name = data2.val().Name;
          customers.Address = data2.val().Address;
          customers.code = data2.val().CustCode;
        }
      });
      resolve(customers);
    });
  });
  retrieveCustDataAW();
}

var CylindersList = '';
var customer = {};
async function retrieveCustDataAW() {
  cyltable = [];
  customer = await getCustData2;
  document.getElementById("Address").value = customer.Address;
  document.querySelector(".loading2").style.display = "none";
}

function retrieveCustomerCyls(){
  return new Promise((resolve, reject) => {
    custcyls = [];
    custCylindersRef.child(customers.id).orderByChild("cylinderType").equalTo(cylType.value).once("value", (data) => {
      data.forEach((data2) => {
        cyltable.push([data2.key, data2.val().cylinderType]);
        custcyls.push([data2.key, data2.val().cylinderType, data2.val().lastOut]);
      });
      resolve(custcyls);
    });
  });
}

var customerCylinders = [];
async function fillTableData() {
  customerCylinders = await retrieveCustomerCyls();
  fillData2AW();
  document.querySelector(".loading2").style.display = "none";
}

$('#CylinderType').bind("input", () => {
  cyltable = [];
  document.querySelector(".loading2").style.display = "block";
  var val = document.getElementById("CylinderType").value;
  var options = document.getElementById("Products").childNodes;
  for (var i = 0; i < options.length; i++) {
    if (options[i].value === val) {
      fillTableData();
    }
  }
});

async function fillData2AW() {
  table.destroy();
  table2.destroy();
 

  table = $('#activeProductsTable').DataTable({
    data: cyltable,
    select: true,
    columns: [{
        title: "Cylinder ID"
      },
      {
        title: "Cylinder Type"
      },
      {
        title: "Action",
        "defaultContent": "<a class='btn btn-info btn-round btn-sm' style='color:white;'>ADD</a>"
      }
    ]
  });
  table2 = $('#ICRTable').DataTable({
    select: true,
    columns: [{
        title: "Cylinder ID"
      },
      {
        title: "Cylinder Type"
      },
      {
        title: "Status",
        "defaultContent": `<select id="ItemDesc" class="form-control"><option value="Empty" ${document.getElementById("CylinderStatus").value === 'Empty' ? 'Selected' : ''}>Empty</option><option value="Defective" ${document.getElementById("CylinderStatus").value === 'Defective' ? 'Selected' : ''}>Defective</option><option value="Production" ${document.getElementById("CylinderStatus").value === 'Production' ? 'Selected' : ''}>Production</option></select>`
      },
      {
        title: "Action",
        "defaultContent": "<a class='btn btn-info btn-round btn-sm' style='color:white;'>REMOVE</a>"
      }
    ]
  })
  cylinderquan = 0;
  var tableSearch = document.getElementsByClassName("form-control-sm");
  tableSearch[1].addEventListener("keypress", (e) => {
    if (e.key === "Enter") {
      row = table.row({
        search: 'applied'
      }).data();
      cylinderID = row[0];
      cylinder = row[1];
      table.row({
        search: 'applied'
      }).remove().draw(false);
      table2.row.add([
        cylinderID,
        cylinder,
        `<select id="ItemDesc" class="form-control"><option value="Empty" ${document.getElementById("CylinderStatus").value === 'Empty' ? 'Selected' : ''}>Empty</option><option value="Defective" ${document.getElementById("CylinderStatus").value === 'Defective' ? 'Selected' : ''}>Defective</option><option value="Production" ${document.getElementById("CylinderStatus").value === 'Production' ? 'Selected' : ''}>Production</option></select>`,
        "<a class='btn btn-info btn-round btn-sm' style='color:white;'>Remove</a>"
      ]).draw(false);
      cylinderquan += 1;
      tableSearch[1].value = "";
    }
  });
  tableSearch[3].addEventListener("keypress", (e) => {
    if (e.key === "Enter") {
      row = table2.row({
        search: 'applied'
      }).data();
      cylinderID = row[0];
      cylinder = row[1];
      table2.row({
        search: 'applied'
      }).remove().draw(false);
      table.row.add([
        cylinderID,
        cylinder,
        "<a class='btn btn-info btn-round btn-sm' style='color:white;'>Add</a>"
      ]).draw(false);
      cylinderquan -= 1;
      tableSearch[3].value = "";
    }
  });
}

document.getElementById("createICRBtn").addEventListener("click", (event) => {
  event.preventDefault();

  ICRRef.child(ICRNo.value).once('value', function (snapshot) {
    var exists = (snapshot.val() !== null);
    if (!exists) {
      if (confirm("You are about to create an ICR. Would you like to continue?")) {
        if (ICRNo.value) {
          if (CustName.value) {
            if (cylType.value) {
              if (table2.rows().data().length > 0) {
                createICR();
              } else {
                alert("Please select atleast 1 cylinder.")
              }
            } else {
              alert("Please select Cylinder Type")
            }
          } else {
            alert("Please select Customer");
          }
        } else {
          alert("Please input ICR No.")
        }
      } else {
        // do something.
      }
    } else {
      window.alert(ICRNo.value + " is existing, cannot create an ICR with the same ID.");
    }
  });
});

var itemProcessed = [0, 0, 0, 0, 0];
var productsSelected = [];
var productsSelected2 = [];
var selects = document.getElementsByTagName('select');

//cancel button
document.getElementById("cancelICRBtn").addEventListener("click", (event) => {
  event.preventDefault();
  if (confirm("You are about to discard this session. Would you like to continue?")) {
    window.location = "createICR.html";
  } else {
    // do something.
  }
});

function getBigLedger(itemsLedger) {
  return new Promise((resolve, reject) => { 
    BigLedgerList = [];
    for (var x = 0; x < itemsLedger.length; x++) {
      let cylinderLedgerId = itemsLedger[x][0];
      let custIdLedger = customers.id;
      bigLedgerRef.child(custIdLedger).child('items').child(cylinderLedgerId).once("value", (data) => {
        data.forEach((data2) => {
          data2.forEach((data3) => {
            data3.forEach((data4) => {
              if (data4.val().cylinderType === cylType.value) {
                let mainID;

                if (data4.val().directInvoice == true) {
                  mainID = data4.val().invNum;
                } else if (data4.val().directInvoice == false) {
                  mainID = data4.val().drNum;
                }
            
                let smdatesplitter = (data4.val().out).split(" ");
                let newdatesplit;
            
                if (smdatesplitter[0].indexOf("/") != -1) {
                  newdatesplit = smdatesplitter[0].split("/");
                } else if (smdatesplitter[0].indexOf("-") != -1) {
                  newdatesplit = smdatesplitter[0].split("-");
                }
            
                let newdate = newdatesplit[0] + "-" + newdatesplit[1] + "-" + newdatesplit[2];
            
                if (data4.val().in == "ACTIVE") {
                  bigLedgerRef.child(custIdLedger).child("items").child(cylinderLedgerId).child("Cylinder Record").child(mainID).child(newdate + " " + smdatesplitter[1]).update({
                      in: date.value
                    })
                    .then(() => {
                      itemProcessed[1]++;
                      if (itemProcessed[1] === productsSelected2.length) {
                        checker[1] = true;
                        funcComplete(checker);
                      }
                    });
                }else{
                  itemProcessed[1]++;
                  if (itemProcessed[1] === productsSelected2.length) {
                    checker[1] = true;
                    funcComplete(checker);
                  }
                }
                BigLedgerList.push([cylinderLedgerId, data4.key, data4.val().directInvoice, data4.val().drNum, data4.val().invNum, data4.val().in, data4.val().out, custIdLedger]);
              }
            });
          });
        });
      });
      resolve(BigLedgerList);
    }
  });
}

async function createICR() {
  document.querySelector(".loading2").style.display = "block";
  var data = table2.rows().data();
  data.each(function (value, index) {
    productsSelected.push([value[0], value[1], value[2]]);
    custcyls.forEach(function (product) {//palitan
      if (product[0] == value[0]) {
        productsSelected2.push([product[0], product[1], product[2]]);
      }
    })
  });

  productsSelected.forEach(product => {
    var dom = document.createElement('div');
    dom.innerHTML = product[2];
    dom = dom.firstChild;
    product[2] = dom.options[dom.selectedIndex].value;
  })

  ICRRef.child(ICRNo.value).set({
    custName: CustName.value,
    date: date.value,
    cylType: cylType.value,
    custAddress: custAddress.value
  });

  productsSelected2.forEach((data2, index) => {
    ICRRef.child(ICRNo.value).child("Cylinders").child(data2[0]).set({
        type: data2[1],
        sorterkey: databaseRef.push().getKey()
      })
      .then(() => {
        itemProcessed[0]++;
        if (itemProcessed[0] === productsSelected2.length) {
          checker[0] = true;
          funcComplete(checker);
        }
      });

    inventoryRef.child(data2[0]).update({
        cylinderstatus: productsSelected[index][2]
      })
      .then(() => {
        itemProcessed[2]++;
        if (itemProcessed[2] === productsSelected2.length) {
          checker[2] = true;
          funcComplete(checker);
        }
    });

    smallLedgerRef.child(data2[0]).update({
        status: productsSelected[index][2]
      })
      .then(() => {
        let smdatesplitter = (data2[2]).split(" ");
        let newdatesplit;
        if (smdatesplitter[0].indexOf("/") != -1) {
          newdatesplit = smdatesplitter[0].split("/");
        } else if (smdatesplitter[0].indexOf("-") != -1) {
          newdatesplit = smdatesplitter[0].split("-");
        }

        let newdate = newdatesplit[0] + "-" + newdatesplit[1] + "-" + newdatesplit[2];

        smallLedgerRef.child(data2[0]).child("CylinderRecord").child(newdate + " " + smdatesplitter[1]).update({
            in: date.value
          })
          .then(() => {
            itemProcessed[3]++;
            if (itemProcessed[3] === productsSelected2.length) {
              checker[3] = true;
              funcComplete(checker);
            }
          })
      });
      
      custCylindersRef.child(customers.id).child(data2[0]).remove()
      .then(() => {
        itemProcessed[4]++;
        if (itemProcessed[4] === productsSelected2.length) {
          checker[4] = true;
          funcComplete(checker);
        }
      })
  })
  const finishBigLedger = await getBigLedger(productsSelected);
}

function funcComplete(checker) {
  console.log(checker);
  if (checker[0] && checker[1] && checker[2] && checker[3] && checker[4]) {
    window.location = "createICR.html";
  }
}

$('#activeProductsTable tbody').on('click', 'a', (e) => {
  var selectedRow = e.target.parentNode.parentNode;
  var cylinderID = selectedRow.children[0].innerText;
  var cylinder = selectedRow.children[1].innerText
  e.target.parentNode.parentNode.classList.toggle("selected");
  table.row(".selected").remove().draw(false);
  table2.row.add([
    cylinderID,
    cylinder,
    `<select id="ItemDesc" class="form-control"><option value="Empty" ${document.getElementById("CylinderStatus").value === 'Empty' ? 'Selected' : ''}>Empty</option><option value="Defective" ${document.getElementById("CylinderStatus").value === 'Defective' ? 'Selected' : ''}>Defective</option><option value="Production" ${document.getElementById("CylinderStatus").value === 'Production' ? 'Selected' : ''}>Production</option></select>`,
    "<a class='btn btn-info btn-round btn-sm' style='color:white;'>Remove</a>"
  ]).draw(false);
  cylinderquan += 1;
});

$('#ICRTable tbody').on('click', 'a', (e) => {
  var selectedRow = e.target.parentNode.parentNode;
  var cylinderID = selectedRow.children[0].innerText;
  var cylinder = selectedRow.children[1].innerText
  e.target.parentNode.parentNode.classList.toggle("selected");
  table2.row(".selected").remove().draw(false);
  table.row.add([
    cylinderID,
    cylinder,
    "<a class='btn btn-info btn-round btn-sm' style='color:white;'>Add</a>"
  ]).draw(false);
  cylinderquan -= 1;
});