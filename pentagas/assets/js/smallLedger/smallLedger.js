//database references
var databaseRef = firebase.database().ref();
var smallledgerRef = databaseRef.child("Small Ledger");
var productsRef = databaseRef.child("Inventory");
var customerRef = databaseRef.child("Customer");
var bigLedgerRef = databaseRef.child("Big Ledger");

var activeRecordHldr = '';
var outRecord = '';
var updateddate = document.getElementById("modalInDate");

//pdfvariables
var custcode = [];
var custid = [];
var cylto = [];
var cylout = [];
var cylin = [];

//get date today
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1;
var yyyy = today.getFullYear();
if(dd<10){
  dd='0'+dd;
}

if(mm<10){
  mm='0'+mm;
}
var datetoday = mm+'-'+dd+'-'+yyyy;


// var table = '';
var table2;

function getProducts(){
  document.querySelector(".loading2").style.display = "block";
  return new Promise((resolve, reject) => {
    var products = [];
    productsRef.orderByChild("cylinderstatus").equalTo("Production").once("value", (data) => {
      data.forEach((data2) => {
        products.push([data2.key, data2.val().cylindertype, data2.val().cylinderstatus]);
      });
      resolve(products);
    });  
  })
} 

async function fillInventoryData(){
  // for products quantity table
  const products = await getProducts();
  document.querySelector(".loading2").style.display = "none";
  var productArr = [];
  var productQty = [0,0,0,0,0,0,0,0,0,0,0];
  products.forEach((data) => {
    productArr.push([data[0],data[1]]);
      switch(data[1]) {
        case "MED 02":
            productQty[0] += 1;
            break;
        case "MED 02 FLASK":
            productQty[1] += 1;
            break;
        case "MED 02 BANTAM":
            productQty[2] += 1;
            break;
        case "MED 02 MEDIUM":
            productQty[3] += 1;
            break;
        case "TECH 02":
            productQty[4] += 1;
            break;
        case "ARGON":
            productQty[5] += 1;
            break;
        case "COMP AIR":
            productQty[6] += 1;
            break;
        case "ACETYLENE":
            productQty[7] += 1;
            break;
        case "NITROGEN":
            productQty[8] += 1;
            break;
        case "NITROUS DIOXIDE":
            productQty[9] += 1;
            break;
        case "CARBON DIOXIDE":
            productQty[10] += 1;
            break;
      }
  });

  document.getElementById("itemQtyTableBody").innerHTML =
  `
  <tr>
  <td>
    ${productQty[0]}
  </td>
  <td>
    ${productQty[1]}
  </td>
  <td>
    ${productQty[2]}
  </td>
  <td>
    ${productQty[3]}
  </td>
  <td>
    ${productQty[4]}
  </td>
  <td>
    ${productQty[5]}
  </td>
  <td>
    ${productQty[6]}
  </td>
  <td>
    ${productQty[7]}
  </td>
  <td>
    ${productQty[8]}
  </td>
  <td>
    ${productQty[9]}
  </td>
  <td>
    ${productQty[10]}
  </td>
  </tr>
  `
}

function changeDate(e){
  if(e.parentNode.parentNode.children[2].innerHTML == 'ACTIVE'){
    $('#myModal').modal("hide");
    activeRecordHldr = e.parentNode.parentNode.children[2].innerHTML;
    outRecord = e.parentNode.parentNode.children[1].innerHTML;
    updateddate.value = outRecord;
  
    $('#myModal2').modal({
      backdrop: 'static',
      keyboard: false
    });
    $('#myModal2').modal("show");
  }else{
    alert("Status of this record cannot be changed due to it not being 'ACTIVE'");
  }

}

function changeStatus() {
  if (activeRecordHldr === "ACTIVE"){
    if (window.confirm("Are you sure you want to change the 'IN' date of this record?")){
      $('#myModal2').modal("hide");
      // document.querySelector(".loading2").style.display = "block";
      let selectedRecord = outRecord;
      let newInRecord = updateddate.value;
      let recordToChange;

      smallledgerRef.child(cylid).once('value', function(snapshot) {
       recordToChange = snapshot.val();

        let insmdatesplitter = (newInRecord).split(" ");
        let indatesplit;
        if(insmdatesplitter[0].indexOf("/") != -1){
          indatesplit = insmdatesplitter[0].split("/");
        }else if(insmdatesplitter[0].indexOf("-") != -1){
          indatesplit = insmdatesplitter[0].split("-");
        }

        let newindate = indatesplit[0]+"/"+indatesplit[1]+"/"+indatesplit[2];
      
        let customerID = recordToChange.CylinderRecord[`${selectedRecord}`].customerID;
        let isDirectInvoice = recordToChange.directInvoice; 
        let refNum = isDirectInvoice ? recordToChange.CylinderRecord[`${selectedRecord}`].invoiceNum : recordToChange.CylinderRecord[`${selectedRecord}`].drNumber;
        smallledgerRef.child(cylid).child("CylinderRecord").child(selectedRecord).update({
          in: newindate+" "+insmdatesplitter[1]+" AM"
        })
        .then(() => {
          bigLedgerRef.child(customerID).child("items").child(cylid).child("Cylinder Record").child(refNum).child(selectedRecord).update({
            in: newindate+" "+insmdatesplitter[1]+"AM"
          }).then(() => {
            document.querySelector(".loading2").style.display = "none";
            // window.location = "smallLedger.html";
            table2.destroy();
            $('#myModal2').modal("hide");
          })
        })

      });

    }
  } else {
    alert("Status of this record cannot be changed");
  }
}

var cylid = '';
var cyltype = '';
var lastupdate = '';
var custID = '';
$('#smallLedgerTable tbody').on('click', 'td', (e) => {
  var selectedRow = e.target.parentNode;
  if (selectedRow.children[1].innerText == "ProductionDefectiveMissingEmpty" || selectedRow.children[1].innerText == "Defective" || selectedRow.children[1].innerText == "Missing" || selectedRow.children[1].innerText == "Empty" || selectedRow.children[1].innerText == "Production"){
    
  }else {
    document.querySelector(".loading2").style.display = "block";
    cylid = selectedRow.children[0].innerText;
    cyltype = selectedRow.children[1].innerText;
    lastupdate = selectedRow.children[2].innerText;
    custID = selectedRow.children[4].innerText;

    getLedgerItems = new Promise((resolve, reject) => {
      var items = [];
      cylto = [];
      cylout = [];
      cylin = [];
      var cyloutsplitter = '';
      var appendedcylout = '';
      var cylinsplitter = '';
      var appendedcylin = '';
      smallledgerRef.child(cylid).child("CylinderRecord").once("value", (data) => {
        data.forEach((data2) => {
            if(data2.val().customer != undefined && data2.val().out != undefined, data2.val().in != undefined){
              cylto.push(data2.val().custCode); //CustCode

              let convertedDate = data2.val().out.split(' ')[0];
              let convertedTime = data2.val().out.split(' ')[1];
              let convertedOut = `${convertedDate.split('/')[0]}-${convertedDate.split('/')[1]}-${convertedDate.split('/')[2]} ${convertedTime}`;

              cyloutsplitter = (data2.val().out).split(" ");   //Split Values
              appendedcylout = cyloutsplitter[0];
              cylout.push(appendedcylout);     //CylOut

              if(data2.val().in != "ACTIVE"){
                cylinsplitter = (data2.val().in).split(" ");   //Split Values
                appendedcylin = cylinsplitter[0];
                cylin.push(appendedcylin);    //CylIn
              }else{
                cylin.push("ACTIVE");
              }

              items.push([data2.val().custCode, convertedOut, data2.val().in]);
            }
        });
        resolve(items);
      });
    });

    retrieveLedgerDataAW();
  }
});

async function retrieveLedgerDataAW() {
  const items = await getLedgerItems;
  document.querySelector(".modal-title").textContent = cylid;
  table2 = $('#ledgerItemsTable').DataTable( {
      data: items,
      select: true,
      columns: [
        { title: "To" },
        { title: "Out" },
        { title: "In" },
        { title: "Change Status", "defaultContent" : `<input id="incorrectRecordBtn" type="button" class="btn btn-warning btn-rounded btn-sm" value="Update IN Date" onClick="changeDate(this)">`}
      ]
  } );
  document.querySelector(".loading2").style.display = "none";
  $('#myModal').modal({
    backdrop: 'static',
    keyboard: false
  });
  $('#myModal').modal("show");
}

function clearTables() {
  table2.destroy();
}

function defectiveFunc(e){
  document.querySelector(".loading2").style.display = "block";
  if (confirm("You are about to change the status of " + e.parentNode.parentNode.parentNode.children[0].innerText + " to defective. Would you like to continue?")){
    productsRef.child(e.parentNode.parentNode.parentNode.children[0].innerText).update({
      cylinderstatus: "Defective"
    })
    .then(() => {
      smallledgerRef.child(e.parentNode.parentNode.parentNode.children[0].innerText).update({
        status: "Defective",
        lastUpdate: datetoday
      })
      .then(() => {
        window.location = "smallLedger.html";
      })
    });
  } else {
    // do something.
  }
}
function missingFunc(e){
  document.querySelector(".loading2").style.display = "block";
  if (confirm("You are about to change the status of " + e.parentNode.parentNode.parentNode.children[0].innerText + " to missing. Would you like to continue?")){
    productsRef.child(e.parentNode.parentNode.parentNode.children[0].innerText).update({
      cylinderstatus: "Missing"
    })
    .then(() => {
      smallledgerRef.child(e.parentNode.parentNode.parentNode.children[0].innerText).update({
        status: "Missing",
        lastUpdate: datetoday
      })
      .then(() => {
        window.location = "smallLedger.html";
      })
    });
  } else {
    // do something.
  }
}
function emptyFunc(e){
  document.querySelector(".loading2").style.display = "block";
  if (confirm("You are about to change the status of " + e.parentNode.parentNode.parentNode.children[0].innerText + " to Empty. Would you like to continue?")){
    productsRef.child(e.parentNode.parentNode.parentNode.children[0].innerText).update({
      cylinderstatus: "Empty"
    })
    .then(() => {
      smallledgerRef.child(e.parentNode.parentNode.parentNode.children[0].innerText).update({
        status: "Empty",
        lastUpdate: datetoday
      })
      .then(() => {
        window.location = "smallLedger.html";
      })
    });
  } else {
    // do something.
  }
}

function prodFunc(e){
  document.querySelector(".loading2").style.display = "block";
  if (confirm("You are about to change the status of " + e.parentNode.parentNode.parentNode.children[0].innerText + " to Production. Would you like to continue?")){
    productsRef.child(e.parentNode.parentNode.parentNode.children[0].innerText).update({
      cylinderstatus: "Production"
    })
    .then(() => {
      smallledgerRef.child(e.parentNode.parentNode.parentNode.children[0].innerText).update({
        status: "Production",
        lastUpdate: datetoday,
      })
      .then(() => {
        window.location = "smallLedger.html";
      })
    });
  } else {
    // do something.
  }
}
