var databaseRef = firebase.database().ref();
var smallledgerprocessref = databaseRef.child("Small Ledger");

var tablecolumns = [];
var tabledata = [];
var pageLoad = 200;
var table = '';
var mainItemArr = [];

var retrievestatus = '';

var cylinderInput = document.getElementById("cylinderSearchInput");
document.querySelector(".loading").style.display = "none";

cylinderInput.addEventListener('input', function (evt) {
    if (this.value == '') {
        table.destroy();
        stateTable(mainItemArr);
    }
});

stateTable(mainItemArr);

function getAllSmallLedgerData(toberetrieved) {
    retrievestatus = toberetrieved;
    var tabledataarr = [];
    return new Promise((resolve, reject) => {
        smallledgerprocessref.limitToFirst(pageLoad).once("value", (data) => {
            data.forEach((data2) => {
                if (data2.key !== undefined && data2.val().cylinderType != undefined && data2.val().lastUpdate != undefined) {
                    let dateSplitter = (data2.val().lastUpdate).split(" ");
                    if (toberetrieved == 'all') {
                        tabledataarr.push([data2.key, data2.val().cylinderType, dateSplitter[0], data2.val().status]);
                    } else if (toberetrieved == 'allactive') {
                        if (data2.val().status == 'Active') {
                            tabledataarr.push([data2.key, data2.val().cylinderType, dateSplitter[0], data2.val().status]);
                        }
                    }
                }
            });
            resolve(tabledataarr);
        });
    });
}

async function retrieveAllSLData(toberetrieved) {
    cylinderInput.value = '';
    document.querySelector(".loading").style.display = "block";
    const smallLedgerAllList = await getAllSmallLedgerData(toberetrieved);
    mainItemArr = smallLedgerAllList;
    stateTable(smallLedgerAllList);
}

function retriveCylinder() {
    if (cylinderInput.value) {
        document.querySelector(".loading").style.display = "block";
        table.clear();
        pageLoad = 200;
        smallledgerprocessref.child(cylinderInput.value).once('value', function (snapshot) {
            var exists = (snapshot.val() !== null);
            if (exists) {
                let cylinderArr = [];
                let dateSplitter = (snapshot.val().lastUpdate).split(" ");
                cylinderArr.push([snapshot.key, snapshot.val().cylinderType, dateSplitter[0], snapshot.val().status]);
                stateTable(cylinderArr);
            } else {
                document.querySelector(".loading").style.display = "block";
                window.alert("Cylinder Not Found.");
            }
        });
    } else {
        window.alert("Input is empty, please try again.");
    }

}

function stateTable(tabledata) {
    var paginate = false;
    paginate = tabledata.length > 1 ? true : false;

    if(table){
        table.destroy();
    }
    
    table = $('#smallLedgerTable').DataTable({
        select: true,
        data: tabledata,
        paging: true,
        language: {
            "search": "Search Current Data in Table:",
            "emptyTable": "Search or Load Cylinder Data/s"
        },
        aaSorting: [],
        bPaginate: paginate,
        columns: [{
                title: "Cylinder ID"
            },
            {
                title: "Cylinder Type"
            },
            {
                title: "Last Update"
            },
            {
                title: "Status"
            },
            {
                title: "Change Status",
                "defaultContent": '<input type="button" class="btn btn-success dropdown-toggle nav-link" data-toggle="dropdown" value="Action"><div class="dropdown-menu"><a onclick="prodFunc(this)" class="dropdown-item smallLedger-dropdown">Production</a><a onclick="defectiveFunc(this)" class="dropdown-item smallLedger-dropdown">Defective</a><a onclick="missingFunc(this)" class="dropdown-item smallLedger-dropdown">Missing</a><a onclick="emptyFunc(this)" class="dropdown-item smallLedger-dropdown">Empty</a></div>'
            }
        ],
        drawCallback: function(){
            $('.paginate_button.next', this.api().table().container())          
               .on('click', async function(){
                  document.querySelector(".loading").style.display = "block";
                  pageLoad += 200;
                  table.clear();
                  smallLedgerAllList = await getAllSmallLedgerData(retrievestatus);
                  mainItemArr = smallLedgerAllList;
                  stateTable(smallLedgerAllList);
            });       
         }
    });
    document.querySelector(".loading").style.display = "none";
}