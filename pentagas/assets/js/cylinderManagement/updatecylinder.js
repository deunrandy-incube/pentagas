var cylinderID = document.getElementById("cylID");
var cylinderType = document.getElementById("cylType");
var cylinderRemarks = document.getElementById("cylRemarks");
var databaseRef = firebase.database().ref();
var productsRef = databaseRef.child("Inventory");
var bigLedgerRef = databaseRef.child("Big Ledger");
var smallLedgerRef = databaseRef.child("Small Ledger");
var table = '';
let product;

document.querySelector(".loading").style.display = "block";

const getProduct = new Promise((resolve, reject) => {
    productsRef.once("value", (data) => {
        product = {
            cylinderId: '',
            cylinderType: '',
            cylinderStatus: '',
            remarks: ''
        }
        data.forEach((data2) => {
            if (data2.key === localStorage.getItem('CylinderUpdateID')){
                product.cylinderId = data2.key;
                product.cylinderType = data2.val().cylindertype;
                product.cylinderStatus = data2.val().cylinderstatus;
                product.remarks = data2.val().cylinderremarks ? data2.val().cylinderremarks : '';
            }
        });
        resolve(product);
    })
});

async function retrieveInventory() {
    const product = await getProduct;
    cylinderID.value = product.cylinderId;
    cylinderType.value = product.cylinderType;
    cylinderRemarks.value = product.remarks;

    cylinderID.disabled = true;

    if(product.cylinderStatus != "Production"){
        window.alert("Cylinder selected is currently ACTIVE thus you cannot change the cylinder type.");
        cylinderType.disabled = true;
    }else{
        cylinderType.disabled = false;
    }

    document.querySelector(".loading").style.display = "none";
}
retrieveInventory();

function updateCylinders() {
    productsRef.child(product.cylinderId).update({
        cylindertype: product.cylinderType,
        cylinderstatus: product.cylinderStatus,
        cylinderremarks: cylinderRemarks.value
    })
    .then(res => {
        window.alert(`Successfully updated ${product.cylinderId}`)
    })
}

function cancel() {
    window.location = "cylinderView.html";
}