//database references
var databaseRef = firebase.database().ref();
var productsRef = databaseRef.child("Inventory");

//get date today
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1;
var yyyy = today.getFullYear();
if(dd<10){
  dd='0'+dd;
}

if(mm<10){
  mm='0'+mm;
}
var datetoday = mm+'-'+dd+'-'+yyyy;

document.querySelector(".loading").style.display = "block";

var table = '';
const getProducts = new Promise((resolve, reject) => {
  var products = [];
  var productQty = [0,0,0,0,0,0,0,0,0,0,0];
  productsRef.once("value", (data) => {
    data.forEach((data2) => {
      products.push([data2.key, data2.val().cylindertype, data2.val().cylinderstatus]);
        switch(data2.val().cylindertype) {
          case "MED 02":
              productQty[0] += 1;
              break;
          case "MED 02 FLASK":
              productQty[1] += 1;
              break;
          case "MED 02 BANTAM":
              productQty[2] += 1;
              break;
          case "MED 02 MEDIUM":
              productQty[3] += 1;
              break;
          case "TECH 02":
              productQty[4] += 1;
              break;
          case "ARGON":
              productQty[5] += 1;
              break;
          case "COMP AIR":
              productQty[6] += 1;
              break;
          case "ACETYLENE":
              productQty[7] += 1;
              break;
          case "NITROGEN":
              productQty[8] += 1;
              break;
          case "NITROUS DIOXIDE":
              productQty[9] += 1;
              break;
          case "CARBON DIOXIDE":
              productQty[10] += 1;
              break;
        }
    });
    document.getElementById("itemQtyTableBody").innerHTML =
    `
    <tr>
    <td>
        ${productQty[0]}
    </td>
    <td>
        ${productQty[1]}
    </td>
    <td>
        ${productQty[2]}
    </td>
    <td>
        ${productQty[3]}
    </td>
    <td>
        ${productQty[4]}
    </td>
    <td>
        ${productQty[5]}
    </td>
    <td>
        ${productQty[6]}
    </td>
    <td>
        ${productQty[7]}
    </td>
    <td>
        ${productQty[8]}
    </td>
    <td>
        ${productQty[9]}
    </td>
    <td>
        ${productQty[10]}
    </td>
    </tr>
    `
    resolve(products);
  });
})

//table assignment
async function retrieveInventory(){
  const inventoryList = await getProducts;
  document.querySelector(".loading").style.display = "none";
  table = $('#cylindertable').DataTable( {
      data: inventoryList,
      select: true,
      columns: [
          { title: "Cylinder ID" },
          { title: "Cylinder Type" },
          { title: "Status" },
      ]
  } );
}
retrieveInventory();

$('#cylindertable tbody').on('click', 'td', (e) => {
    var selectedRow = e.target.parentNode;
    localStorage.setItem("CylinderUpdateID", selectedRow.children[0].innerText)
    window.location = "cylinderUpdate.html";
})
