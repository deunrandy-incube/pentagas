var cylinderID = document.getElementById("cylID");
var cylinderType = document.getElementById("cylType");
var cylinderRemarks = document.getElementById("cylRemarks");
var databaseRef = firebase.database().ref();
var productsRef = databaseRef.child("Inventory");
var table = '';
var products = [];

document.querySelector(".loading").style.display = "block";

const getProducts = new Promise((resolve, reject) => {
    productsRef.once("value", (data) => {
        data.forEach((data2) => {
            products.push(data2.key+"---"+data2.val().cylinderstatus);
            resolve(products);
        });
    })
});

async function retrieveInventory() {
    const inventoryList = await getProducts;
    document.querySelector(".loading").style.display = "none";
}
retrieveInventory();

function createCylinders() {
    var checkid = false;
    ctr = 0;
    var cylindershldr = (cylinderID.value).split(',');
    cylindershldr.forEach((data2) => {
        checkid = false;
        for (var x = 0; x < products.length; x++) {
            if (data2 == products[x]) {
                checkid = true;
            }
        }

        if (checkid == false) {
            productsRef.child(data2).set({
                    cylindertype: cylinderType.value,
                    cylinderstatus: "Production",
                    cylinderremarks: cylinderRemarks.value
                })
        }

        ctr++;
        if (ctr == cylindershldr.length) {
            window.alert("Cylinder/s added into Inventory.");
            cylinderID.value = '';
        }
    });
}

function cancel() {
    window.location = "cylinderView.html";
}


function makeAllCylindersProd(){
    for (var x = 0; x < products.length; x++) {
        let splitter = products[x].split("---");
        if(splitter[1] != "Production"){
            productsRef.child(splitter[0]).update({
                cylinderstatus: "Production",
            })
        }
    }
}
